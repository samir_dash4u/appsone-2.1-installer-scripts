#!/bin/bash
BASEDIR=$(pwd)
CONFFILE="${BASEDIR}/conf/generalConf.py"
CONFFILE2="${BASEDIR}/conf/extendedConf.py"
mkdir -p "$BASEDIR/logs"
LOG_FILE="$BASEDIR/logs/${0}.log"
VERBOSE=1 	# Default verbose output is set to false
VERSION="2.1"

function logit()
{
        datestamp=$(date +"%d-%m-%Y:%H:%M:%S");
        echo "$datestamp : $* " >> $LOG_FILE
	if [ $VERBOSE -eq 0 ]; then
		echo "$*"
	fi
        return 0;
}

function check_validip() {
	ip="$1"
	if [[ $ip =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
		return 0
	else
		return 1
	fi
}

function fetch_localnode() {
	nodes=($(grep ^NODES ${CONFFILE} | cut -d = -f2 | tr ',' '\n' | cut -d : -f2 | sed -e "s/'//g;s/}//g") )
	check_validip "${nodes[0]}"
	if [ $? -eq 0 ]; then
		logit "Proceeding with IP address based installation"
		which ip > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			echo "Not able to determine local node's ip"
			read -p "Please enter local node's ip" localip
			export LOCAL_NODE="$localip"
		else
			for i in "${nodes[@]}"; do
				ip addr | grep -w $i > /dev/null 2>&1
				if [ $? -eq 0 ]; then 
					grep -q "LOCAL_NODE = '$i'" ${CONFFILE2}
					if [ $? -ne 0 ]; then
						echo "LOCAL_NODE = '$i'" >> ${CONFFILE2}
					fi
					export LOCAL_NODE="$i"
			        fi
			done
		fi
		if [ -z "$LOCAL_NODE" ]; then
		        logit "None of the IP address provided in generalConf.py matches with local machine IP address. Exiting."
		        exit 1
		else
			logit "Local machine IP address is $LOCAL_NODE"
		fi
	else
		logit "Proceeding with host name based installation"
		which hostname > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			echo "Not able to determine local node's hostname"
			read -p "Please enter local node's hostname" localip
			export LOCAL_NODE="$localip"
		else
			for i in "${nodes[@]}"; do
				hostname | grep -w $i > /dev/null 2>&1
				if [ $? -eq 0 ]; then 
					grep -q "LOCAL_NODE = '$i'" ${CONFFILE2}
					if [ $? -ne 0 ]; then
						echo "LOCAL_NODE = '$i'" >> ${CONFFILE2}
					fi
					export LOCAL_NODE="$i"
			        fi
			done
		fi
		if [ -z "$LOCAL_NODE" ]; then
		        logit "None of the Host name provided in generalConf.py matches with local machine Host name. Exiting."
		        exit 1
		else
			logit "Local machine host name is $LOCAL_NODE"
		fi
	fi

	IP=`grep ^NODES ${CONFFILE} | cut -d = -f2 | tr ',' '\n' | cut -d : -f2 | sed -e "s/'//g;s/}//g" | grep -v "$LOCAL_NODE"`
	export IP="$IP"
}

function key_auth() {
	if [ -f ~/.ssh/id_rsa.pub ]; then
		echo "Key is already present."
		read -p "Do you wish to re-create key for password less authentication: [ y/N ]" choice
		if [ $choice == "y" -o $choice == "Y" ]; then
			logit "Overwriting existing rsa key pair"
			echo "If it asks to over write, hit y and Enter"
			echo "Hit Enter for each step, till the key is generated"
			ssh-keygen -t rsa
		else
			logit "Using existing rsa key pair for password less connection"
		fi
	else
		echo "Hit Enter for each step, till the key is generated"
		echo "Do not enter any passphrase."
		logit "Generating rsa key pair"
		ssh-keygen -t rsa
	fi

	for ip in `echo $IP`; do
		logit "Copying the public key to $ip"
		echo "Enter the password of target machines when asked for and hit Enter"
                ssh-copy-id -o ConnectTimeout=200 -i ~/.ssh/id_rsa.pub $ip 
                if [ $? -ne 0 ];then
                        logit "$ip is not reachable exiting."
                        exit 1
                fi
	done
	logit "Password less connection created"
}

function open_port() {
	TMPFILE="$BASEDIR/Ports.xml"
	logit "Opening ports for communication between $LOCAL_NODE and other nodes"
	logit "Opening ports on $LOCAL_NODE"
	echo '<?xml version="1.0" encoding="utf-8"?>' > $TMPFILE
        echo "<service>" >> $TMPFILE
	grep _PORT  conf/extendedConf.py | grep -v "RANGE\|^#" | cut -d = -f2 | sed -e "s/'//g;s/ //g" | while read line
	do
		grep -q $line $TMPFILE
		if [ $? -ne 0 ]; then
	        	echo "<port port=\"$line\" protocol=\"tcp\"/>" >> $TMPFILE
		fi
	done
        echo "<port port=\"4567\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "<port port=\"5701\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "<port port=\"5702\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "<port port=\"5703\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "<port port=\"8001\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "<port port=\"8300\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "<port port=\"8301\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "<port port=\"8500\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "<port port=\"8501\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "<port port=\"8995\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "<port port=\"9193\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "<port port=\"50020\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "<port port=\"50010\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "<port port=\"50075\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "<port port=\"56728\" protocol=\"tcp\"/>" >> $TMPFILE
        echo "</service>" >> $TMPFILE
	logit "Enabling ports and reloading firewall rules on local node"
        firewall-cmd --get-service | grep "A1_Service" > /dev/null 2>&1
        if [ $? -eq 0 ]; then
                firewall-cmd --delete-service=A1_Service --zone=public --permanent > /dev/null 2>&1
                firewall-cmd --reload > /dev/null 2>&1
        fi
        firewall-offline-cmd --new-service-from-file=$TMPFILE --name=A1_Service --zone=public > /dev/null 2>&1
        firewall-cmd --reload > /dev/null 2>&1
        firewall-cmd --add-service A1_Service --zone=public --permanent > /dev/null 2>&1
        firewall-cmd --reload > /dev/null 2>&1
	if [ $? -eq 0 ]; then
		logit "Firewall successfully reloaded on local node"
	else
		logit "Firewall rule updation failed on local node"
	fi

	for ip in `echo $IP`
	do
		logit "Opening ports on $ip"
		scp $TMPFILE $ip:/tmp/firewall.xml > /dev/null 2>&1
		logit "Enabling ports and reloading firewall rules on $ip"
		ssh -o ConnectTimeout=200 $ip "firewall-cmd --delete-service=A1_Service --zone=public --permanent > /dev/null 2>&1; firewall-cmd --reload > /dev/null 2>&1; firewall-offline-cmd --new-service-from-file=/tmp/firewall.xml --name=A1_Service --zone=public > /dev/null 2>&1; firewall-cmd --reload > /dev/null 2>&1; firewall-cmd --add-service=A1_Service --zone=public --permanent > /dev/null 2>&1; firewall-cmd --reload > /dev/null 2>&1"
		if [ $? -eq 0 ]; then
			logit "Firewall successfully reloaded on $ip"
		else
			logit "Firewall rule updation failed on $ip"
		fi
	done
	rm -f $TMPFILE
	logit "All the ports configured in config file has been enabled on the firewall. Please run the below command to verify"
	logit "iptable -vnL"
	logit "Port enabling completed.."
}

function disable_selinux() {
	logit "Disabling SELINUX, ICMP on local node"
	bash $BASEDIR/lib/firewall.sh
	if [ $? -ne 0 ]; then
		logit "Step failed to execute. Exiting"
		exit 1
	else
		logit "Step completed successfully"
	fi
	for ip in `echo $IP`; do
		logit "Disabling SELINUX, ICMP on $ip"
	        ssh -A -o ConnectTimeout=200 $ip 'bash ' < $BASEDIR/lib/firewall.sh
		if [ $? -ne 0 ]; then
			logit "Step failed to execute. Exiting"
			exit 1
		else
			logit "Step completed successfully"
		fi
	done
}

function create_nonrootuser() {
	user=`grep 'NOMAD_USER =' ${CONFFILE} | awk -F'=' '{print $2}' | tr -d "'" | tr -d ' '`
	logit "Creating user on local node $LOCAL_NODE."
	id -u $user > /dev/null 2>&1
	if [ $? -ne 0 ]; then
		useradd $user
		passwd $user
	else
		logit "User $user already exists on local node"
	fi
	
	grep -q "$user.*ALL" /etc/sudoers
	if [ $? -ne 0 ]; then
		sed -i "/root.*ALL=(ALL).*ALL/a $user    ALL=(ALL)       ALL" /etc/sudoers
	fi
	for ip in `echo $IP`; do
		echo "Creating user $user on $ip"
	        ssh -t -o ConnectTimeout=200 $ip  "export user=$user; id -u $user > /dev/null 2>&1; if [ \$? -ne 0 ]; then useradd $user; passwd $user; else echo "User $user already exists on $ip"; fi; grep -q "$user.*ALL" /etc/sudoers; if [ \$? -ne 0 ]; then  sed -i '/root.*ALL=(ALL).*ALL/a $user    ALL=(ALL)       ALL' /etc/sudoers; fi"
	done
	
	logit "The non root user $user have been created on all nodes."
}

function copy_certs() {
	logit "Copying certficates to be used for internal ssl comunication"
	nomad_port=$(grep ^HTTP_PORT  $BASEDIR/conf/extendedConf.py | grep -v "RANGE\|^#" | cut -d = -f2 | sed -e "s/'//g;s/ //g" )
	a1Home=$(grep ^SER_INSTALL_PATH $BASEDIR/conf/generalConf.py | grep -v "RANGE\|^#" | cut -d = -f2 | sed -e "s/'//g;s/ //g" )
	
	cd /etc/pki/ca-trust/source/anchors/
	cp $BASEDIR/conf/cert/nomad-ca.pem .
	update-ca-trust extract
	cd - > /dev/null 2>&1
	HOSTNAME=$(grep ^HOST_ENTRY $BASEDIR/conf/generalConf.py | cut -d = -f2 | sed -e "s/'//g;s/ //g")
	logit "Updating environment variables in /root/.bash_profile. Please reload the same after installation is completed using 'source /root/.bash_profile'"
	grep -q "NOMAD_CACERT" /root/.bash_profile
	if [ $? -ne 0 ]; then
		echo "export NOMAD_CACERT=\"/etc/nomad/nomad-ca.pem\"
export NOMAD_CLIENT_KEY=\"/etc/nomad/cli-key.pem\"
export NOMAD_CLIENT_CERT=\"/etc/nomad/cli.pem\"
export NOMAD_ADDR=https://localhost:$nomad_port
export A1_Home=$a1Home" >> /root/.bash_profile
	echo "$LOCAL_NODE       $HOSTNAME" >> /etc/hosts
	fi

	for ip in `echo $IP`; do
		scp $BASEDIR/conf/cert/nomad-ca.pem root@$ip:/etc/pki/ca-trust/source/anchors/ > /dev/null 2>&1
		ssh -t -o ConnectTimeout=200 $ip  "cd /etc/pki/ca-trust/source/anchors/; update-ca-trust extract; grep -q "NOMAD_CACERT" /root/.bash_profile; if [ \$? -ne 0 ]; then echo 'export NOMAD_CACERT="/etc/nomad/nomad-ca.pem"' >> /root/.bash_profile; echo 'export NOMAD_CLIENT_KEY="/etc/nomad/cli-key.pem"' >> /root/.bash_profile;  echo 'export NOMAD_CLIENT_CERT="/etc/nomad/cli.pem"' >> /root/.bash_profile; echo 'export NOMAD_ADDR=https://localhost:$nomad_port' >> /root/.bash_profile;  echo 'export A1_Home=$a1Home' >> /root/.bash_profile;echo '$LOCAL_NODE    $HOSTNAME' >> /etc/hosts; fi"
	done
	logit "Copying certificates completed"
}

function check_root
{
        ROOT_UID=0      #The root user has a UID of 0
        Chk_Root=$(id -u 2>/dev/null);
        if [ "$Chk_Root" -ne "$ROOT_UID" ]; then
                echo "The script requires root user. Please run as root"
                exit 1
        fi
}

function usage
{
	echo "Usage : $0 [-hvV]
	$0 -v Dispalys version
	$0 -h Displayes this help message
	$0 -V Runs the script with verbose output
	$0 Runs the script without verbose ouptut"
}

function exit_script
{
	logit "$1"
	exit $2
}

while getopts ":v :h :V" opt; do
        case $opt in
                v)
                        echo "Version = $VERSION"
                        exit_script "Version displayed" 0
                        ;;
                h)
                        usage
                        exit_script "Usage displayed" 0
                        ;;
                V)
                        VERBOSE=0
                        shift
                        ;;
                ?)
                        echo "Invalid option " >&2
                        usage
                        exit 1
                        ;;
        esac
done
check_root
fetch_localnode
key_auth
disable_selinux
open_port
create_nonrootuser
copy_certs

cp ${BASEDIR}/lib/jq /usr/bin/
logit "Pre-requisites steps completed successfully"
