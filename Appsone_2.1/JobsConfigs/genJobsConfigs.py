#!/usr/bin/env python

import mysql.connector
from template_nomad_job import *
from template_tpl import *
from template_docker import *
import socket, errno
import os
import sys
from uuid import *
from subprocess import Popen,PIPE
from time import * 

confdir=sys.argv[1]
ROOT_DB_USER=sys.argv[2]
ROOT_DB_PASSWORD=sys.argv[3]
consul_deps_path=sys.argv[4]
createDefaultMysql_path=sys.argv[5]
sys.path.insert(0,confdir)
sys.path.append(consul_deps_path)
import consul

try:
    from extendedConf import *
    from generalConf import *
    from version import *
except Exception as e:
    print ("No able to include configuration files due to " + str(e) )
    sys.exit(101)

if ( MIGRATION.upper() == "TRUE" ):
   from migrationConf import *

con = consul.Consul()

def get_ips():
    for serverName, doc_service in DOCKER_SERVICES.items():
	try :
		IP = NODES[serverName]
	except Exception as e :
		print ("Not able to fetch the IP of " + str(serverName) )
		continue;

	fileh = open("service_ip.py" , "a" )
	if "PERCONA" in doc_service:
		fileh.write("DB_IP = \"" + IP + "\"\n")
		
	if "CASSANDRA_NODE1" in doc_service:
		fileh.write("CASSANDRA_BROADCAST_ADDRESS_Node1 = \"" + IP + "\"\n" )
		
	if "CASSANDRA_NODE2" in doc_service:
		fileh.write("CASSANDRA_BROADCAST_ADDRESS_Node2 = \"" + IP + "\"\n" )
		
	if "CASSANDRA_NODE3" in doc_service:
		fileh.write("CASSANDRA_BROADCAST_ADDRESS_Node3 = \"" + IP + "\"\n" )
		
	if "COUCHBASE" in doc_service:		
		fileh.write("COUCHBASE_IP = \"" + IP + "\"\n" )
		
	if "TOMCAT" in doc_service:
		fileh.write("TOMCAT_IP = \"" + IP + "\"\n" )
		
	if "KEYCLOAK" in doc_service:
		fileh.write("KEYCLOAK_IP = \"" + IP + "\"\n" )
		
	if "HAPROXY" in doc_service:		
		fileh.write("HAPROXY_IP = \"" + IP + "\"\n" )
		
	if "RABBITMQ" in doc_service:		
		fileh.write("RABBITMQ_IP = \"" + IP + "\"\n" )
		
	if "HADOOP_NAMENODE" in doc_service:		
		fileh.write("HADOOP_NAMENODE_IP = \"" + IP + "\"\n" )
		
	if "HADOOP_DATANODE" in doc_service:		
		fileh.write("HADOOP_DATANODE_IP = \"" + IP + "\"\n" )
		
	if "ZOOKEEPER" in doc_service:
		fileh.write("ZOOKEEPER_IP = \"" + IP + "\"\n" )
		
	if "FLINK_NODE1" in doc_service:
		fileh.write("FLINK_IP_1 = \"" + IP + "\"\n" )
		
	if "FLINK_NODE2" in doc_service:
		fileh.write("FLINK_IP_2 = \"" + IP + "\"\n" )
		
	if "CAPACITY_PLANNING" in doc_service:
		fileh.write("CAPACITY_PLANNING_IP = \"" + IP + "\"\n" )
	fileh.close()


# To fetch the unused port for the given IP
# count in the argument is used to fetch multiple unused port
def unused_ports(IP, from_port, to_port, count):
    counter = 0
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(10)
    for port in xrange(from_port, to_port+1):
        try:
            s.connect((IP, port))
        except Exception as e:
                counter = counter + 1
                if counter == count:
                        filehPort.write(str(port) +"\n")
                        s.close()
                        return (port)


# To create .nomad job for the given service
def configurenomadjobs(tmpl, target):
    fileh = open(nomaddir + "/" + target + ".nomad", "w")
    fileh.write(tmpl.safe_substitute(JOB_NAME=target, NOMAD_JOB_PATH=NOMAD_JOB_PATH, IP=IP, USER=NOMAD_USER, DATA_CENTER=DATA_CENTER,REGION=REGION))
    fileh.close()

# To create .tpl file for the given service
def configuretpl(tmpl, target):
    fileh = open(target + ".config.tpl", "w")
    fileh.write(tmpl.safe_substitute(JOB_NAME=target))
    fileh.close()

def ServerDetails():
    global IP
    global server_name
    for server_name, appsone_service in SERVERS.items():
        try :
                IP = NODES[server_name]
        except Exception as e :
                print ("Not able to fetch the IP of " + str(server_name) )
                continue;
        servicedetails(IP, appsone_service)


def txncollector(uniq_service, port_count):
    port = unused_ports(IP, TXNCOLLECTOR_PORT_RANGE[0], TXNCOLLECTOR_PORT_RANGE[1], port_count)
    key_name = "service/" + uniq_service + "/port/http"
    con.kv.put(key_name, str(port))

    port_count = port_count + 1
    port = unused_ports(IP, TXNCOLLECTOR_PORT_RANGE[0], TXNCOLLECTOR_PORT_RANGE[1], port_count)
    key_name = "service/" + uniq_service + "/port/https"
    con.kv.put(key_name, str(port))
    con.kv.put("service/txncollector/cache_update_interval",str(cache_update_interval) )

    file_txn = open("txn_haproxy.txt", "a")
    file_txn.write("  server "+uniq_service+" "+IP+":"+str(port)+" check-ssl ssl verify none\n")
    file_txn.close()

    configurenomadjobs(TXNCOLLECTOR, uniq_service)
    configuretpl(TXNCOLLECTOR_1, uniq_service)

def alertingservice(uniq_service, port_count):
    port = unused_ports(IP, ALERTINGSERVICE_PORT_RANGE[0], ALERTINGSERVICE_PORT_RANGE[1], port_count)
    port_count = port_count + 1
    port1 = unused_ports(IP, ALERTINGSERVICE_PORT_RANGE[0], ALERTINGSERVICE_PORT_RANGE[1], port_count)

    con.kv.put("service/alertingservice/ip", IP)
    con.kv.put("service/alertingservice/protocol", "https")
    con.kv.put("service/alertingservice/http/port", str(port))
    con.kv.put("service/alertingservice/https/port", str(port1))
    con.kv.put("service/alertingservice/exitFlag", ALERTINGSERVICE_ExitFlag)
    con.kv.put("service/alertingservice/componentSeverityAlerts", ALERTINGSERVICE_ComponentSeverityAlerts)
    con.kv.put("service/alertingservice/transactionSeverityAlerts", ALERTINGSERVICE_TransactionSeverityAlerts)
    # Putting dummy values for alerting service 
    index = None
    index, data = con.kv.get("service/alertingservice/third_party_url", index=index)
    if data == None:
    	con.kv.put("service/alertingservice/third_party_url"," ")

    configurenomadjobs(ALERTINGSERVICE, uniq_service)

def componentagent(uniq_service):
    agent_name = uniq_service+"_AGENT"
    if ( MIGRATION.upper() == "TRUE" ):
	try:
	    uniq_id=UID[server_name]['COMPONENTAGENT']
	except(KeyError):
	    uniq_id = str(uuid4())
	    dbentry(agent_name,IP,uniq_id)
    else :
	uniq_id = str(uuid4())
	dbentry(agent_name,IP,uniq_id)
    con.kv.put("service/component-agent/uniqueid",uniq_id)
    con.kv.put("service/component-agent/ip",IP)
    configurenomadjobs(COMPONENTAGENT, uniq_service)

def etlltm(uniq_service, port_count):
    port = unused_ports(IP, ETLLTM_PORT_RANGE[0], ETLLTM_PORT_RANGE[1], port_count)
    key_name = "service/etl/" + uniq_service + "/http/port"
    con.kv.put(key_name, str(port))

    port_count = port_count + 1
    port = unused_ports(IP, ETLLTM_PORT_RANGE[0], ETLLTM_PORT_RANGE[1], port_count)
    key_name = "service/etl/" + uniq_service + "/https/port"
    con.kv.put(key_name, str(port))
    file_ltm_etl = open("etl_ltm_haproxy.txt", "a")
    file_ltm_etl.write("  server "+uniq_service+" "+IP+":"+str(port)+" check-ssl ssl verify none\n")
    file_ltm_etl.close()

    key_name = "service/etl/" + uniq_service + "/runmode"
    con.kv.put(key_name, ETL_RUNMODE)
    key_name = "service/etl/" + uniq_service + "/threads/min"
    con.kv.put(key_name, str(ETL_MINTHREAD))
    key_name = "service/etl/" + uniq_service + "/threads/max"
    con.kv.put(key_name, str(ETL_MAXTHREAD))
    key_name = "service/etl/" + uniq_service + "/fi/timediff"
    con.kv.put(key_name, str(ETL_FI_TIMEDIFF))
    key_name = "service/etl/" + uniq_service + "/fi/agentid"
    con.kv.put(key_name, str(ETL_FI_AGENTID))
    key_name = "service/etl/" + uniq_service + "/fi/timezone"
    con.kv.put(key_name, str(ETL_FI_TIMEZONE))
    key_name = "service/etl/" + uniq_service + "/ltm/agentid"
    con.kv.put(key_name, str(ETL_LTM_AGENTID))
    key_name = "service/etl/" + uniq_service + "/ltm/timezone"
    con.kv.put(key_name, str(ETL_LTM_TIMEZONE))
    key_name = "service/etl/" + uniq_service + "/ltm/eumfilter"
    con.kv.put(key_name, str(ETL_LTM_EUMFILTER))
    key_name = "service/etl/" + uniq_service + "/global/sinktype"
    con.kv.put(key_name, str(ETL_GLOBAL_SKINTYPE))
    key_name = "service/etl/" + uniq_service + "/maxactiveconn"
    con.kv.put(key_name, str(ETL_MAXACTIVECONN))
    key_name = "service/etl/" + uniq_service + "/ds/version"
    con.kv.put(key_name, str(ETL_DS_VERSION))
    key_name = "service/etl/" + uniq_service + "/fi/sinktype"
    con.kv.put(key_name, str(ETL_FI_SKINTYPE))
    key_name = "service/etl/" + uniq_service + "/feba/sinktype"
    con.kv.put(key_name, str(ETL_FEBA_SKINTYPE))
    key_name = "service/etl/" + uniq_service + "/dc/sinktype"
    con.kv.put(key_name, str(ETL_DC_SKINTYPE))
    key_name = "service/etl/" + uniq_service + "/eum/sinktype"
    con.kv.put(key_name, str(ETL_EUM_SKINTYPE))
    key_name = "service/etl/" + uniq_service + "/scheduler/inmemory"
    con.kv.put(key_name, str(ETL_SCHEDULER_INMEM))
    key_name = "service/etl/" + uniq_service + "/scheduler/cache"
    con.kv.put(key_name, str(ETL_SCHEDULER_CACHE))
    key_name = "service/etl/" + uniq_service + "/fi/dbtxnfields"
    con.kv.put(key_name, str(ETL_FI_DBTXNFIELD))
    key_name = "service/etl/" + uniq_service + "/feba/dbtxnfields"
    con.kv.put(key_name, str(ETL_FEBA_DBTXNFIELD))
    key_name = "service/etl/" + uniq_service + "/dc/dbtxnfields"
    con.kv.put(key_name, str(ETL_DC_DBTXNFIELD))
    key_name = "service/etl/" + uniq_service + "/eum/dbtxnfields"
    con.kv.put(key_name, str(ETL_EUM_DBTXNFIELD))
    key_name = "service/etl/" + uniq_service + "/meter/enabled"
    con.kv.put(key_name, str(ETL_METER_ENABLED))
    key_name = "service/etl/" + uniq_service + "/meter/starttime"
    con.kv.put(key_name, str(ETL_METER_STARTTIME))
    key_name = "service/etl/" + uniq_service + "/tcsockettimeout"
    con.kv.put(key_name, str(ETL_SOCKET_TIMEOUT))
    key_name = "service/etl/" + uniq_service + "/tcmaxconn"
    con.kv.put(key_name, str(ETL_MAX_CONN))
    key_name = "service/etl/" + uniq_service + "/iorthreads"
    con.kv.put(key_name, str(ETL_IOR_THREADS))
    key_name = "service/etl/" + uniq_service + "/tcconntimeout"
    con.kv.put(key_name, str(ETL_CONN_TIMEOUT))
    key_name = "service/etl/" + uniq_service + "/tcconnreqtimeout"
    con.kv.put(key_name, str(ETL_CONN_REQ_TIMEOUT))
    key_name = "service/etl/" + uniq_service + "/pendingreqvalfreq"
    con.kv.put(key_name, str(ETL_PENDING_REQ_VALIDATION))

    configurenomadjobs(ETLLTM, uniq_service)
    configuretpl(ETLLTM_1, uniq_service)

def etlstitching(uniq_service, port_count):
    port = unused_ports(IP, ETLSTICHING_PORT_RANGE[0], ETLSTICHING_PORT_RANGE[1], port_count)
    key_name = "service/etl/" + uniq_service + "/httpport"
    con.kv.put(key_name, str(port))

    port_count = port_count + 1
    port = unused_ports(IP, ETLSTICHING_PORT_RANGE[0], ETLSTICHING_PORT_RANGE[1], port_count)
    key_name = "service/etl/" + uniq_service + "/httpsport"
    con.kv.put(key_name, str(port))
    file_etl_stitching = open("etl_stitching_haproxy.txt", "a")
    file_etl_stitching.write("  server "+uniq_service+" "+IP+":"+str(port)+" check-ssl ssl verify none\n")
    file_etl_stitching.close()

    key_name = "service/etl/" + uniq_service + "/evtdelayedtime"
    con.kv.put(key_name,EVT_DELAYED_TIME)
    key_name = "service/etl/" + uniq_service + "/runmode"
    con.kv.put(key_name, RUNMODE)
    key_name = "service/etl/" + uniq_service + "/threads/min"
    con.kv.put(key_name, str(ETL_STITCHING_MINTHREAD))
    key_name = "service/etl/" + uniq_service + "/threads/max"
    con.kv.put(key_name, str(ETL_STITCHING_MAXTHREAD))
    key_name = "service/etl/" + uniq_service + "/npci/agentid"
    con.kv.put(key_name, str(ETL_NPCI_AGENTID))
    key_name = "service/etl/" + uniq_service + "/npci/timezone"
    con.kv.put(key_name, str(ETL_NPCI_TIMEZONE))
    key_name = "service/etl/" + uniq_service + "/global/sinktype"
    con.kv.put(key_name, str(ETL_STITCHING_GLOBAL_SKINTYPE))
    key_name = "service/etl/" + uniq_service + "/maxactiveconn"
    con.kv.put(key_name, str(ETL_STITCHING_MAXACTIVECONN))
    key_name = "service/etl/" + uniq_service + "/ds/version"
    con.kv.put(key_name, str(ETL_STITCHING_DS_SKINTYPE))
    key_name = "service/etl/" + uniq_service + "/c24/sinktype"
    con.kv.put(key_name, str(ETL_STITCHING_C24_SKINTYPE))
    key_name = "service/etl/" + uniq_service + "/bom/sinktype"
    con.kv.put(key_name, str(ETL_STITCHING_BOM_SKINTYPE))
    key_name = "service/etl/" + uniq_service + "/scheduler/inmemory"
    con.kv.put(key_name, str(ETL_STITCHING_SCHEDULER_INMEM))
    key_name = "service/etl/" + uniq_service + "/scheduler/cache"
    con.kv.put(key_name, str(ETL_STITCHING_SCHEDULER_CACHE))
    key_name = "service/etl/" + uniq_service + "/c24/dbtxnfields"
    con.kv.put(key_name, str(ETL_STITCHING_C24_DBTXNFIELD))
    key_name = "service/etl/" + uniq_service + "/npci/dbtxnfields"
    con.kv.put(key_name, str(ETL_STITCHING_NPCI_DBTXNFIELD))
    key_name = "service/etl/" + uniq_service + "/bom/dbtxnfields"
    con.kv.put(key_name, str(ETL_STITCHING_BOM_DBTXNFIELD))
    key_name = "service/etl/" + uniq_service + "/tcsockettimeout"
    con.kv.put(key_name, str(ETL_STITCHING_SOCKET_TIMEOUT))
    key_name = "service/etl/" + uniq_service + "/tcmaxconn"
    con.kv.put(key_name, str(ETL_STITCHING_MAX_CONN))
    key_name = "service/etl/" + uniq_service + "/iorthreads"
    con.kv.put(key_name, str(ETL_STITCHING_IOR_THREADS))
    key_name = "service/etl/" + uniq_service + "/tcconntimeout"
    con.kv.put(key_name, str(ETL_STITCHING_CONN_TIMEOUT))
    key_name = "service/etl/" + uniq_service + "/tcconnreqtimeout"
    con.kv.put(key_name, str(ETL_STITCHING_CONN_REQ_TIMEOUT))
    key_name = "service/etl/" + uniq_service + "/pendingreqvalfreq"
    con.kv.put(key_name, str(ETL_STITCHING_PENDING_REQ_VALIDATION))

    configurenomadjobs(ETLSTITCHING , uniq_service)
    configuretpl(ETLSTITCHING_1, uniq_service)

def incidentmgmtbroker(uniq_service, port_count):
    port = unused_ports(IP, INCIDENTMGMTBROKER_PORT_RANGE[0], INCIDENTMGMTBROKER_PORT_RANGE[1], port_count)
    con.kv.put("service/incident-management-broker/ip", IP)
    con.kv.put("service/incident-management-broker/broker_port", str(port))
    ## for alerting service
    val="https://" + IP + ":" + str(port) + "/appsone-alerts"
    con.kv.put("service/alertingservice/third_party_url",val)

    con.kv.put("service/incident-management-broker/cache_update_interval",str(INCIDENTMGMTBROKER_cache_update_interval))
    con.kv.put("service/incident-management-broker/alerts_data_duration",str(INCIDENTMGMTBROKER_alerts_data_duration))
    con.kv.put("service/incident-management-broker/suppression_duration",str(INCIDENTMGMTBROKER_suppression_duration))
    con.kv.put("service/incident-management-broker/alerts_per_incident",str(INCIDENTMGMTBROKER_alerts_per_incident))

    configurenomadjobs(INCIDENTMGMTBROKER, uniq_service)

def patternrt(uniq_service):
    configurenomadjobs(PATTERNRT, uniq_service)

def jmxcollector_jboss(uniq_service,count):
    uniq_id = str(uuid4())
    agent_name = uniq_service+"_AGENT"
    count=count*2 - 2
    if ( MIGRATION.upper() == "TRUE" ):
        try:
            uniq_id=UID[server_name]['JMXCOLLECTOR_JBOSS'][count]
	    count=count+1
	    uniq_service=UID[server_name]['JMXCOLLECTOR_JBOSS'][count]
        except (KeyError , IndexError):
            uniq_id = str(uuid4())
            dbentry(agent_name,IP,uniq_id)
    else :
        uniq_id = str(uuid4())
        dbentry(agent_name,IP,uniq_id)

    con.kv.put("service/jmx-collector/jboss/uniqueid", uniq_id)
    con.kv.put("service/jmx-collector/jboss/ip", IP)

    configurenomadjobs(JMXCOLLECTOR_JBOSS, uniq_service)

def jmxcollector(uniq_service, count):
    uniq_id = str(uuid4())
    agent_name = uniq_service+"_AGENT"
    count=count*2 - 2
    if ( MIGRATION.upper() == "TRUE" ):
        try:
            uniq_id=UID[server_name]['JMXCOLLECTOR'][count]
	    count=count+1
	    uniq_service=UID[server_name]['JMXCOLLECTOR'][count]
        except (KeyError , IndexError):
            uniq_id = str(uuid4())
            dbentry(agent_name,IP,uniq_id)
    else :
        uniq_id = str(uuid4())
        dbentry(agent_name,IP,uniq_id)

    con.kv.put("service/jmx-collector/uniqueid", uniq_id)
    con.kv.put("service/jmx-collector/ip", IP)
    configurenomadjobs(JMXCOLLECTOR, uniq_service)

def jmxcollector_was(uniq_service,count):
    if ( not os.path.isfile(IBM_JRE_PATH)):
	print ("IBM JRE FILE doesn't exists. Can not create .nomad file for jmxcollector was")
	return 0	
    uniq_id = str(uuid4())
    agent_name = uniq_service+"_AGENT"
    count=count*2 - 2
    if ( MIGRATION.upper() == "TRUE" ):
        try:
            uniq_id=UID[server_name]['JMXCOLLECTOR_WAS'][count]
	    count=count+1
	    uniq_service=UID[server_name]['JMXCOLLECTOR_WAS'][count]
        except (KeyError , IndexError):
            uniq_id = str(uuid4())
            dbentry(agent_name,IP,uniq_id)
    else :
        uniq_id = str(uuid4())
        dbentry(agent_name,IP,uniq_id)
    con.kv.put("service/jmx-collector-was/ibm-gre-path",IBM_JRE_PATH)
    con.kv.put("service/jmx-collector/was/uniqueid", uniq_id)
    con.kv.put("service/jmx-collector/was/ip", IP)
    configurenomadjobs(JMXCOLLECTOR_WAS, uniq_service)

def collatingservice(uniq_service, port_count):
    con.kv.put("service/collatingservice/ip",IP)
    configurenomadjobs(COLLATINGSERVICE, uniq_service)

def cxo(uniq_service, port_count):
    con.kv.put("service/cxo/ip" , IP )
    con.kv.put("service/sqlds/ip", IP)
    con.kv.put("service/sqlds/sumo_ip", "sqlds.appsone.com")
    con.kv.put("service/sumo-ds/numOfMins", SUMODS_numOfMins)
    con.kv.put("service/sumo-ds/severityLevels", SUMODS_severityLevels)
    con.kv.put("service/sumo-ds/alertTypes", SUMODS_alertTypes)
    con.kv.put("service/sumo-ds/responseType", SUMODS_responseType)
    con.kv.put("service/sumo-ds/decimialPrecision", SUMODS_decimialPrecision)
    con.kv.put("service/sumo-ds/lagInMins", SUMODS_lagInMins)
    con.kv.put("service/sumo-ds/active_directory/enabled", SUMODS_active_directory_enabled)
    con.kv.put("service/sumo-ds/connection_timeout", SUMODS_connection_timeout)
    con.kv.put("service/sumo-ds/top_alerts", SUMODS_top_alerts)
    
    port = unused_ports(IP, SQLDS_PORT_RANGE[0], SQLDS_PORT_RANGE[1], port_count)
    con.kv.put("service/sqlds/port", str(port))

    configurenomadjobs(CXO, uniq_service)

def forensicservice(uniq_service,port_count):
    port = unused_ports(IP, FORENSICSERVICE_PORT_RANGE[0], FORENSICSERVICE_PORT_RANGE[1], port_count)
    con.kv.put("service/forensicservice/port",str(port))
    configurenomadjobs(FORENSICSERVICE, uniq_service)

def jppfcollector(uniq_service,count):
    uniq_id = str(uuid4())
    # Create a custom component in db with uniq_id
    agent_name = uniq_service+"_AGENT"
    if ( MIGRATION.upper() == "TRUE" ):
        try:
            uniq_id=UID[server_name]['JPPFCOLLECTOR'][count]
	    count=count+1
	    uniq_service=UID[server_name]['JPPFCOLLECTOR'][count]
        except (KeyError , IndexError):
            uniq_id = str(uuid4())
            dbentry(agent_name,IP,uniq_id)
    else :
        uniq_id = str(uuid4())
        dbentry(agent_name,IP,uniq_id)

    con.kv.put("service/jppfkpicollector/uniqueid", uniq_id)
    con.kv.put("service/jppfkpicollector/ip", IP)
    configurenomadjobs(JPPFKPICOLLECTOR, uniq_service)

def collectionservice(uniq_service, port_count):
    port = unused_ports(IP, COLLECTIONSERVICE_PORT_RANGE[0], COLLECTIONSERVICE_PORT_RANGE[1], port_count)
    key_name = "service/" + uniq_service + "/port/http"
    con.kv.put(key_name, str(port))

    port_count = port_count + 1
    port = unused_ports(IP, COLLECTIONSERVICE_PORT_RANGE[0], COLLECTIONSERVICE_PORT_RANGE[1], port_count)
    key_name = "service/" + uniq_service + "/port/https"
    con.kv.put(key_name, str(port))

    file_col = open("col_haproxy.txt", "a")
    file_col.write("  server "+uniq_service+" "+IP+":"+str(port)+" check-ssl ssl verify none\n")
    file_col.close()

    configurenomadjobs(COLLECTIONSERVICE, uniq_service)
    configuretpl(COLLECTIONSERVICE_1, uniq_service)

def datacollector_jvm(uniq_service,count):
    uniq_id = str(uuid4())
    agent_name = uniq_service+"_AGENT"
    count=count*2 - 2
    if ( MIGRATION.upper() == "TRUE" ):
        try:
            uniq_id=UID[server_name]['DATACOLLECTOR'][count]
	    count=count+1
	    uniq_service=UID[server_name]['DATACOLLECTOR'][count]
        except (KeyError , IndexError):
            uniq_id = str(uuid4())
            dbentry(agent_name,IP,uniq_id)
    else :
        uniq_id = str(uuid4())
        dbentry(agent_name,IP,uniq_id)

    key = "service/datacollector/"+ uniq_service + "/uniqueid"
    con.kv.put(key, uniq_id)
    con.kv.put("service/datacollector/stricthostkeychecking","no")
    configurenomadjobs(DATACOLLECTOR, uniq_service)
    configuretpl(DATACOLLECTOR_1, uniq_service)

def datacollector_was(uniq_service,count):
    if ( not os.path.isfile(IBM_JRE_PATH)):
	print ("IBM JRE FILE doesn't exists. Can not create .nomad file for dc was")
	return 0	

    uniq_id = str(uuid4())
    agent_name = uniq_service+"_AGENT"
    count=count*2 - 2
    if ( MIGRATION.upper() == "TRUE" ):
        try:
            uniq_id=UID[server_name]['DATACOLLECTOR_WAS'][count]
	    count=count+1
	    uniq_service=UID[server_name]['DATACOLLECTOR_WAS'][count]
        except (KeyError , IndexError):
            uniq_id = str(uuid4())
            dbentry(agent_name,IP,uniq_id)
    else :
        uniq_id = str(uuid4())
        dbentry(agent_name,IP,uniq_id)

    key = "service/datacollector/"+ uniq_service + "/uniqueid"
    con.kv.put(key, uniq_id)
    con.kv.put("service/data-collector-core/ibm-gre-path" , IBM_JRE_PATH )
    con.kv.put("service/datacollector/stricthostkeychecking","no")
    configurenomadjobs(DATACOLLECTOR_WAS, uniq_service)
    configuretpl(DATACOLLECTOR_1, uniq_service)

def grpc(uniq_service, port_count):
    port = unused_ports(IP, GRPC_PORT_RANGE[0], GRPC_PORT_RANGE[1], port_count)
    con.kv.put("service/grpc-server/port", str(port))

    configurenomadjobs(GRPC, uniq_service)

def notificationservice(uniq_service, port_count):
    con.kv.put("service/notification-service/ip", IP)
    port = unused_ports(IP, NOTIFICATIONSERVICE_PORT_RANGE[0], NOTIFICATIONSERVICE_PORT_RANGE[1], port_count)
    con.kv.put("service/notification-service/http/port", str(port))

    port_count = port_count + 1
    port1 = unused_ports(IP, NOTIFICATIONSERVICE_PORT_RANGE[0], NOTIFICATIONSERVICE_PORT_RANGE[1], port_count)
    con.kv.put("service/notification-service/https/port", str(port1))

    configurenomadjobs(NOTIFICATIONSERVICE, uniq_service)

def etltfp(uniq_service,port_count):
    con.kv.put("service/tfp-etl/mysql_mapping_data_fetch_interval_seconds",str(FETCH_INTERVAL_SECONDS))
    con.kv.put("service/tfp-etl/alert_description_characters",ALERT_DESCRIPTION_CHARACTERS)
    con.kv.put("service/tfp-etl/kpi_data_lag_in_minutes",str(KPI_DATA_LAG_IN_MINUTES))
    con.kv.put("service/tfp-etl/txn_data_lag_in_minutes",str(TXN_DATA_LAG_IN_MINUTES))
    con.kv.put("service/tfp-etl/alerts_queue_name",ALERTS_QUEUE_NAME )
    con.kv.put("service/tfp-etl/txn_queue_name", TXN_QUEUE_NAME )
    con.kv.put("service/tfp-etl/kpi_queue_name" , KPI_QUEUE_NAME )

    con.kv.put("service/tfp-etl/"+ uniq_service +"/ip",IP)

    port = unused_ports(IP, ETLTFP_PORT_RANGE[0], ETLTFP_PORT_RANGE[1], port_count)
    key = "service/tfp-etl/"+ uniq_service +"/port"
    con.kv.put(key,str(port))
    configuretpl(ETLTFP_1, uniq_service)
    configurenomadjobs(ETLTFP, uniq_service)

def tfp_ds(uniq_service,port_count):
    con.kv.put("service/tfp/ip",IP)
    con.kv.put("service/tfp/inboundlayersizeob",INBOUND_LAYER_SIZE)
    con.kv.put("service/tfp/infocuslayersizeob",INFOCUS_LAYER_SIZE)
    con.kv.put("service/tfp/outboundlayersizeob",OUTBOUND_LAYER_SIZE)
    con.kv.put("service/tfp/inboundlayersizewob",INBOUND_LAYER_SIZE_NOB)
    con.kv.put("service/tfp/infocuslayersizewob",INFOCUS_LAYER_SIZE_NOB)
    con.kv.put("service/tfp/outboundlayersizewob",OUTBOUND_LAYER_SIZE_NOB)
    configurenomadjobs(TFPDS,uniq_service)

def reportingservice(uniq_service):
    configurenomadjobs(REPORTINGSERVICE, uniq_service)

def check_percona_status(port,counter):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
    counter=counter+1
    if s.connect_ex((DB_IP, int(DB_PORT))) == 0:
	 return 0
    else:
	 return counter 


def cassandra():
    global CASSANDRA_BROADCAST_ADDRESS_Node1,CASSANDRA_BROADCAST_ADDRESS_Node2,CASSANDRA_BROADCAST_ADDRESS_Node3
    try:
       CASSANDRA_BROADCAST_ADDRESS_Node1
    except NameError: 
        User_IN = raw_input("Provide IP as input for cassandra node1: ")
        if (User_IN != "") :
            CASSANDRA_BROADCAST_ADDRESS_Node1 = User_IN
        else :
            print ( "value not provided for cassandra node1. cassandra.nomad file will not get generated" )
	    return 0

    try:
       CASSANDRA_BROADCAST_ADDRESS_Node2
    except NameError:
        User_IN = raw_input("Provide IP as input for cassandra node2: ")
        if (User_IN != "") :
            CASSANDRA_BROADCAST_ADDRESS_Node2 = User_IN
        else :
            print ( "value not provided for cassandra node2. cassandra.nomad file will not get generated" )
            return 0

    try:
       CASSANDRA_BROADCAST_ADDRESS_Node3
    except NameError:
        User_IN = raw_input("Provide IP as input for cassandra node3: ")
        if (User_IN != "") :
            CASSANDRA_BROADCAST_ADDRESS_Node3 = User_IN
        else :
            print ( "value not provided for cassandra node3. cassandra.nomad file will not get generated" )
            return 0

    fileh = open(nomaddir + "/cassandra.nomad", "w")
    fileh.write(CASSANDRA.safe_substitute(NOMAD_JOB_PATH=NOMAD_JOB_PATH, DATA_CENTER=DATA_CENTER,REGION=REGION, port=CASSANDRA_PORT,CASSANDRA_BROADCAST_ADDRESS_Node1=CASSANDRA_BROADCAST_ADDRESS_Node1,CASSANDRA_BROADCAST_ADDRESS_Node2=CASSANDRA_BROADCAST_ADDRESS_Node2,CASSANDRA_BROADCAST_ADDRESS_Node3=CASSANDRA_BROADCAST_ADDRESS_Node3,RAN_PORT=RAN_PORT,LOCAL_NODE=LOCAL_NODE,getHost=getHost))
    fileh.close()

    con.kv.put("service/cassandra/node1/CASSANDRA_BROADCAST_ADDRESS", CASSANDRA_BROADCAST_ADDRESS_Node1)
    con.kv.put("service/cassandra/node2/CASSANDRA_BROADCAST_ADDRESS", CASSANDRA_BROADCAST_ADDRESS_Node2)
    con.kv.put("service/cassandra/node3/CASSANDRA_BROADCAST_ADDRESS", CASSANDRA_BROADCAST_ADDRESS_Node3)
    con.kv.put("service/cassandra/node1/port", CASSANDRA_PORT)
    con.kv.put("service/cassandra/node2/port", CASSANDRA_PORT)
    con.kv.put("service/cassandra/node3/port", CASSANDRA_PORT)

def couchbase():
    global COUCHBASE_IP
    try:
       COUCHBASE_IP
    except NameError:
        User_IN = raw_input("Provide IP as input for couchbase: ")
        if (User_IN != "") :
            COUCHBASE_IP = User_IN
        else :
            print ( "value not provided for COUCHBASE_IP . couchbase.nomad file will not get generated" )
            return 0

    con.kv.put("service/couchbase/ip",COUCHBASE_IP)
    con.kv.put("service/couchbase/port",str(COUCHBASE_HTTP_PORT))
    con.kv.put("service/couchbase/database/bucket", COUCHBASE_DATABASE_BUCKET)
    fileh = open(nomaddir + "/couchbase.nomad", "w")
    fileh.write(COUCHBASE.safe_substitute(NOMAD_JOB_PATH=NOMAD_JOB_PATH, IP=COUCHBASE_IP, DATA_CENTER=DATA_CENTER,REGION=REGION, http_port=COUCHBASE_HTTP_PORT, connect_port=COUCHBASE_CONNECT_PORT,RAN_PORT=RAN_PORT,LOCAL_NODE=LOCAL_NODE,getHost=getHost))
    fileh.close()

def batchjobmonitoring():
    global BATCHJOB_IP
    try:
       BATCHJOB_IP
    except NameError:
       BATCHJOB_IP = LOCAL_NODE
    try:
       BATCHJOB_PORT
    except NameError:
       BATCHJOB_PORT = '9192' 

    con.kv.put("service/batchjobmonitoring/ip",BATCHJOB_IP)
    con.kv.put("service/batchjobmonitoring/port",str(BATCHJOB_PORT))
    fileh = open(nomaddir + "/batchjobmonitoring.nomad", "w")
    fileh.write(BATCHJOBMONITORING.safe_substitute(NOMAD_JOB_PATH=NOMAD_JOB_PATH, IP=BATCHJOB_IP, DATA_CENTER=DATA_CENTER,REGION=REGION, RAN_PORT=RAN_PORT,getHost=getHost,BATCH_JOB_PORT=BATCHJOB_PORT))
    fileh.close()


def tomcat():
    global TOMCAT_IP
    try:
       TOMCAT_IP
    except NameError:
        User_IN = raw_input("Provide IP as input for tomcat: ")
        if (User_IN != "") :
            TOMCAT_IP = User_IN
        else :
            print ( "value not provided for TOMCAT_IP . tomcat.nomad file will not get generated" )
            return 0
		
    con.kv.put("service/tomcat/ip",TOMCAT_IP)
    con.kv.put("service/tomcat/port",str(TOMCAT_PORT))

    # Putting dummy values for sql-ds and sumo-dataservice as these are required to start tomcat via nomad. This will be required when starting tomcat without configuring
    # sql-ds and sumo-dataservice.
    index = None
    index, data = con.kv.get("service/tfp/ip", index=index)
    if data == None:
    	con.kv.put("service/tfp/ip","localhost")
    index, data = con.kv.get("service/cxo/ip", index=index)
    if data == None:
    	con.kv.put("service/cxo/ip", "localhost")
    index, data = con.kv.get("service/sqlds/ip", index=index)
    if data == None:
    	con.kv.put("service/sqlds/ip", "localhost")
    index, data = con.kv.get("service/sqlds/port", index=index)
    if data == None:
    	con.kv.put("service/sqlds/port", "localhost")
    fileh = open(nomaddir + "/tomcat.nomad", "w")
    fileh.write(TOMCAT.safe_substitute(BASEDIR=SER_INSTALL_PATH, IP=TOMCAT_IP, DATA_CENTER=DATA_CENTER,REGION=REGION, TOMCAT_PORT=TOMCAT_PORT,RAN_PORT=RAN_PORT,LOCAL_NODE=LOCAL_NODE,getHost=getHost))
    fileh.close()
    fileh = open(nomaddir + "/saboo.nomad", "w")
    fileh.write(SABOO.safe_substitute(NOMAD_JOB_PATH=NOMAD_JOB_PATH, IP=TOMCAT_IP, USER=NOMAD_USER, DATA_CENTER=DATA_CENTER,REGION=REGION))
    fileh.close()

def keycloak():
    global KEYCLOAK_IP
    try:
       KEYCLOAK_IP
    except NameError:
        User_IN = raw_input("Provide IP as input for KEYCLOAK:")
        if (User_IN != "") :
            KEYCLOAK_IP = User_IN
        else :
            print ( "value not provided for KEYCLOAK_IP . keycloak.nomad file will not get generated" )
            return 0

    con.kv.put("service/keycloak/ip" , KEYCLOAK_IP)
    con.kv.put("service/keycloak/port" , str(KEYCLOAK_PORT))
    con.kv.put("service/keycloak/user" , "keycloakadmin" )
    con.kv.put("service/keycloak/loglevel" , KEYCLOAK_LOGLEVEL )
    con.kv.put("service/keycloak/secret" , "0a0d1781-27a5-40a9-8ce0-8c58201cab5e" )
    con.kv.put("service/keycloak/clientid" , "appsone-sso" )
    fileh = open(nomaddir + "/keycloak.nomad", "w")
    if ( KEYCLOAK_SSL.upper() == "YES" ):
        con.kv.put("service/keycloak/truststore/file_path" , "/opt/jboss/cert/keycloak.jks" )
        con.kv.put("service/keycloak/truststore/password" , "changeit" )
        fileh.write(KEYCLOAK_AD.safe_substitute(NOMAD_JOB_PATH=NOMAD_JOB_PATH,IP=KEYCLOAK_IP, DATA_CENTER=DATA_CENTER,REGION=REGION, port=KEYCLOAK_PORT,RAN_PORT=RAN_PORT,LOCAL_NODE=LOCAL_NODE,getHost=getHost))
    else :
        fileh.write(KEYCLOAK.safe_substitute(NOMAD_JOB_PATH=NOMAD_JOB_PATH,IP=KEYCLOAK_IP, DATA_CENTER=DATA_CENTER,REGION=REGION, port=KEYCLOAK_PORT,RAN_PORT=RAN_PORT,LOCAL_NODE=LOCAL_NODE,getHost=getHost))
    fileh.close()

def haproxy():
    global HAPROXY_IP
    try:
       HAPROXY_IP
    except NameError:
        User_IN = raw_input("Provide IP as input for HAPROXY:")
        if (User_IN != "") :
            HAPROXY_IP = User_IN
        else :
            print ( "value not provided for HAPROXY_IP . haproxy.nomad file will not get generated" )
            return 0

    con.kv.put("service/haproxy/ip",str(HAPROXY_IP))
    con.kv.put("service/haproxy/hostname"," ")
    con.kv.put("service/haproxy/collectionservice/port",str(HAPROXY_KPI_PORT))
    con.kv.put("service/haproxy/transactioncollector/port",str(HAPROXY_TXN_PORT))
    con.kv.put("service/haproxy/etlltm/port",str(HAPROXY_ETL_LTM_PORT))
    con.kv.put("service/haproxy/etlstitching/port",str(HAPROXY_ETL_STITCHING_PORT))

    with open('txn_haproxy.txt') as fp:
        txn_ha = fp.read()

    with open('col_haproxy.txt') as fp:
        col_ha = fp.read()

    with open('etl_ltm_haproxy.txt') as fp:
        etl_ltm_ha = fp.read()

    with open('etl_stitching_haproxy.txt') as fp:
        etl_stitching_ha = fp.read()

    fileh = open("haproxy.cfg", "w")
    fileh.write(HAPROXY_CONF.safe_substitute(IP=HAPROXY_IP,NOMAD_JOB_PATH=NOMAD_JOB_PATH,txn_ha=txn_ha,col_ha=col_ha,etl_ltm_ha=etl_ltm_ha,etl_stitching_ha=etl_stitching_ha,Username=HAPROXY_STAT_USERNAME,Password=HAPROXY_STAT_PASSWORD)) 
    fileh.close()

    fileh = open(nomaddir + "/haproxy.nomad", "w")
    fileh.write(HAPROXY.safe_substitute(IP=HAPROXY_IP,NOMAD_JOB_PATH=NOMAD_JOB_PATH,HAPROXY_TXN_PORT=HAPROXY_TXN_PORT,HAPROXY_KPI_PORT=HAPROXY_KPI_PORT,STAT_PORT=HAPROXY_STAT_PORT,HAPROXY_ETL_LTM_PORT=HAPROXY_ETL_LTM_PORT,DATA_CENTER=DATA_CENTER,REGION=REGION,RAN_PORT=RAN_PORT,LOCAL_NODE=LOCAL_NODE,getHost=getHost,HAPROXY_ETL_STITCHING_PORT=HAPROXY_ETL_STITCHING_PORT)) 
    fileh.close()


def percona():
    global DB_IP
    try:
       DB_IP
    except NameError:
        User_IN = raw_input("Provide IP as input for PERCONA:")
        if (User_IN != "") :
            DB_IP = User_IN
        else :
            print ( "value not provided for PERCONA_IP . percona.nomad file will not get generated" )
	    print ("cannot proceed further. Aborting")
            sys.exit(102)
    val=getHost+":8501"
    con.kv.put("service/consul/url" , val )
    val=getHost+":8500"
    con.kv.put("service/consul/http/url" , val )
    con.kv.put("service/perconadb/ip",DB_IP)
    con.kv.put("service/perconadb/port",str(DB_PORT))
    con.kv.put("service/perconadb/username",DB_USER)
    #con.kv.put("service/perconadb/user",DB_USER)
    fileh = open(nomaddir + "/percona.nomad", "w")
    fileh.write(PERCONA.safe_substitute(NOMAD_JOB_PATH=NOMAD_JOB_PATH, DB_IP=DB_IP, DB_USER=DB_USER, DATA_CENTER=DATA_CENTER,REGION=REGION,DB_PORT=DB_PORT,RAN_PORT=RAN_PORT,LOCAL_NODE=LOCAL_NODE,getHost=getHost))
    fileh.close()
    bashcommand = 'nomad run ' + nomaddir + '/percona.nomad'
    os.system(bashcommand)
    sleep(15)
    ret = 1
    while True:
        ret = check_percona_status(DB_PORT,ret)
        print ( "Waiting for Percona to start")
	sleep(30)
        if ( ret == 0 ):
           break	
	elif ( ret == 5 ):
	   print(" Not able to start percona. ")
	   User_IN = raw_input("Percona has not started yet. Do you want to recheck percona status? Press Y/N : ")
	   if ( User_IN == "N" ) or ( User_IN == "n" ):
                print ("Aborting..")
                sys.exit(106)
	   else :
    		ret = 1
		while True:
		    ret = check_percona_status(DB_PORT,ret)
		    print ( "Waiting for Percona to start")
		    sleep(15)
		    if ( ret == 0 ):
		       break	
		    elif ( ret == 5 ):
		       print(" Not able to start percona. Aborting..")
	   	       sys.exit(106) 

def rabbitmq():
    global RABBITMQ_IP
    try:
       RABBITMQ_IP
    except NameError:
        User_IN = raw_input("Provide IP as input for RABBITMQ:")
        if (User_IN != "") :
            RABBITMQ_IP = User_IN
        else :
            print ( "value not provided for RABBITMQ_IP . rabbitmq.nomad file will not get generated" )
            return 0

    con.kv.put("service/rabbitmq/ip",RABBITMQ_IP)
    con.kv.put("service/rabbitmq/username",RABBITMQ_USER)
    con.kv.put("service/rabbitmq/password",RABBITMQ_PASSWORD)
    con.kv.put("service/rabbitmq/port",str(RABBITMQ_AMQP_PORT))
    fileh = open(nomaddir + "/rabbitmq.nomad", "w")
    fileh.write(RABBITMQ.safe_substitute(NOMAD_JOB_PATH=NOMAD_JOB_PATH,IP=RABBITMQ_IP,DATA_CENTER=DATA_CENTER,REGION=REGION,RAN_PORT=RAN_PORT,LOCAL_NODE=LOCAL_NODE,amqp_port=RABBITMQ_AMQP_PORT,http_port=RABBITMQ_HTTP_PORT,getHost=getHost))
    fileh.close()

def flink():
    global HADOOP_NAMENODE_IP,HADOOP_DATANODE_IP,ZOOKEEPER_IP,FLINK_IP_1,FLINK_IP_2
    try:
       FLINK_IP_1
    except NameError:
        User_IN = raw_input("Provide IP as input for FLINK node 1:")
        if (User_IN != "") :
            FLINK_IP_1 = User_IN
        else :
            print ( "value not provided for FLINK node 1 . flink.nomad,hadoop.nomad.zookeeper.nomad file will not get generated" )
            return 0
    try:
       FLINK_IP_2
    except NameError:
        User_IN = raw_input("Provide IP as input for FLINK node 2:")
        if (User_IN != "") :
            FLINK_IP_2 = User_IN
        else :
            print ( "value not provided for FLINK node 2 . flink.nomad,hadoop.nomad.zookeeper.nomad file will not get generated" )
            return 0
    try:
       HADOOP_NAMENODE_IP
    except NameError:
        User_IN = raw_input("Provide IP as input for HADOOP_NAMENODE_IP:")
        if (User_IN != "") :
            HADOOP_NAMENODE_IP = User_IN
        else :
            print ( "value not provided for HADOOP_NAMENODE_IP . flink.nomad,hadoop.nomad,zookeeper.nomad file will not get generated" )
            return 0
		
    try:
       HADOOP_DATANODE_IP
    except NameError:
        User_IN = raw_input("Provide IP as input for HADOOP_DATANODE_IP:")
        if (User_IN != "") :
            HADOOP_DATANODE_IP = User_IN
        else :
            print ( "value not provided for HADOOP_DATANODE_IP . flink.nomad,hadoop.nomad,zookeeper.nomad file will not get generated" )
            return 0

    try:
       ZOOKEEPER_IP
    except NameError:
        User_IN = raw_input("Provide IP as input for ZOOKEEPER_IP:")
        if (User_IN != "") :
            ZOOKEEPER_IP = User_IN
        else :
            print ( "value not provided for ZOOKEEPER_IP . flink.nomad,hadoop.nomad,zookeeper.nomad file will not get generated" )
            return 0		
    con.kv.put("service/hadoopnamenode/ip",HADOOP_NAMENODE_IP)
    con.kv.put("service/hadoopnamenode/port",str(HADOOP_NAMENODE_PORT))
    con.kv.put("service/hadoopdatanode/ip",HADOOP_DATANODE_IP)
    con.kv.put("service/hadoopdatanode/port",str(HADOOP_HTTP_PORT))
    con.kv.put("service/zookeeper/ip",ZOOKEEPER_IP)
    con.kv.put("service/zookeeper/port",str(ZOOKEEPER_PORT))
    con.kv.put("service/flink/jobmanager/node1/ip",FLINK_IP_1)
    con.kv.put("service/flink/jobmanager/node2/ip",FLINK_IP_2)
    con.kv.put("service/flink/jobmanager/ha_port",str(HIGH_AVAILABILITY_PORT))
    con.kv.put("service/pipeline/alert_queue_name" , ALERT_QUEUE_NAME) 
    con.kv.put("service/pipeline/txn_queue_name" , TXN_QUEUE_NAME ) 
    con.kv.put("service/pipeline/delayed_txn_queue_name", DELAYED_TXN_QUEUE_NAME) 
    con.kv.put("service/pipeline/kpi_queue_name", KPI_QUEUE_NAME)
    con.kv.put("service/pipeline/out_of_orderness", str(OUT_OF_ODERNESS) ) 
    con.kv.put("service/pipeline/max_delay_btw_txn_kpi_min", str(MAX_DELAY_BTW_TXN_KPI_MIN)) 
    con.kv.put("service/pipeline/cb_ttl_current_window", str(CB_TTL_CURRENT_WINDOW)) 
    con.kv.put("service/pipeline/checkpoint_interval" ,  str(CHECK_INTERVEL))
    con.kv.put("service/pipeline/analytics_training_data_days", str(ANALYTICS_TRAINING_DATA_DAYS))
    con.kv.put("service/pipeline/data_aggregation_interval_min" , str(DATA_AGGREGATION_INTERVAL_MIN)) 
    con.kv.put("service/pipeline/run_hourly_aggregation" , RUN_HOURLY_AGGREGATION_PROCESS_IN )
    con.kv.put("service/pipeline/arima_fitting_strategy" , ARIMA_FITTING_STRATEGY )
    con.kv.put("service/pipeline/medium_anomaly_weightage" , str(MEDIUM_ANOMALY_WEIGHTAGE) )
    con.kv.put("service/pipeline/high_anomaly_weightage" , str(HIGH_ANOMALY_WEIGHTAGE))

    con.kv.put("service/pipeline/analytics_severity_percentile" , str(ANALYTICS_SEVERITY_PERCENTILE))
    con.kv.put("service/pipeline/analytics_severity_percentile_threshold", str(ANALYTICS_SEVERITY_PERCENTILE_THRESHOLD))
    con.kv.put("service/pipeline/analytics_txn_high_threshold", str(ANALYTICS_TXN_HIGH_THRESHOLD))
    con.kv.put("service/pipeline/analytics_txn_medium_threshold" , str(ANALYTICS_TXN_MEDIUM_THRESHOLD))
    con.kv.put("service/pipeline/analytics_kpi_medium_threshold",str(ANALYTICS_KPI_MEDIUM_THRESHOLD))
    con.kv.put("service/pipeline/analytics_kpi_high_threshold",str(ANALYTICS_KPI_HIGH_THRESHOLD))
    con.kv.put("service/pipeline/analytics_stl_remove_outliner" , str(ANALYTICS_STL_REMOVE_OUTLINER))
    con.kv.put("service/pipeline/analytics_stl_daily_seasonal_width" , str(ANALYTICS_STL_DAILY_SEASONAL_WIDTH))
    con.kv.put("service/pipeline/analytics_stl_weekly_seasonal_width" , str(ANALYTICS_STL_WEEKLY_SEASONAL_WIDTH))

    con.kv.put("service/flink/jobmanager/node1/loglevel", FLINK_LOGLEVEL )
    con.kv.put("service/flink/jobmanager/node2/loglevel", FLINK_LOGLEVEL )
    con.kv.put("service/flink/taskmanager/node1/loglevel", FLINK_LOGLEVEL )
    con.kv.put("service/flink/taskmanager/node2/loglevel", FLINK_LOGLEVEL )

    fileh = open(nomaddir + "/zookeeper.nomad", "w")
    fileh.write(ZOOKEEPER.safe_substitute(NOMAD_JOB_PATH=NOMAD_JOB_PATH,IP=ZOOKEEPER_IP,DATA_CENTER=DATA_CENTER,REGION=REGION,RAN_PORT=RAN_PORT,LOCAL_NODE=LOCAL_NODE,client_port=ZOOKEEPER_PORT,follower_port=ZOOKEEPER_FOLLOWER_PORT,election_port=ZOOKEEPER_ELECTION_PORT,getHost=getHost))
    fileh.close()

    fileh = open(nomaddir + "/hadoop.nomad", "w")
    fileh.write(HADOOP.safe_substitute(NOMAD_JOB_PATH=NOMAD_JOB_PATH,IP=HADOOP_NAMENODE_IP,IP_2=HADOOP_DATANODE_IP,DATA_CENTER=DATA_CENTER,REGION=REGION,RAN_PORT=RAN_PORT,LOCAL_NODE=LOCAL_NODE,namenode_port=HADOOP_NAMENODE_PORT,http_port=HADOOP_HTTP_PORT,getHost=getHost))
    fileh.close()

    fileh = open(nomaddir + "/flink.nomad", "w")
    fileh.write(FLINK.safe_substitute(NOMAD_JOB_PATH=NOMAD_JOB_PATH,IP_1=FLINK_IP_1,IP_2=FLINK_IP_2,DATA_CENTER=DATA_CENTER,REGION=REGION,RAN_PORT=RAN_PORT,LOCAL_NODE=LOCAL_NODE,RPC_PORT=JOBMANAGER_RPC_PORT,BLOB_PORT=JOBMANAGER_BLOB_PORT,WEB_PORT=WEB_PORT,TM_RPC_PORT=TASKMANAGER_RPC_PORT,TM_DATAPORT=TASKMANAGER_DATA_PORT,getHost=getHost))
    fileh.close()

def capacityPlanning():
    global CAPACITY_PLANNING_IP
    try:
       CAPACITY_PLANNING_IP
    except NameError:
        User_IN = raw_input("Provide IP as input for CAPACITY_PLANNING_IP:")
        if (User_IN != "") :
            CAPACITY_PLANNING_IP = User_IN
        else :
            print ( "value not provided for CAPACITY_PLANNING_IP . CAPACITY PLANNING nomad file will not get generated" )
            return 0

    con.kv.put("service/capacity-planning/ip",CAPACITY_PLANNING_IP)
    con.kv.put("service/capacity-planning/port" , str(CAPACITY_PLANNING_PORT))
    con.kv.put("service/capacity-planning/clientname", str(CAPACITY_PLANNING_CLIENT_NAME))
    fileh = open(nomaddir + "/capacityPlanning.nomad","w")
    fileh.write(CAPACITY_PLAN.safe_substitute(NOMAD_JOB_PATH=NOMAD_JOB_PATH,DATA_CENTER=DATA_CENTER,REGION=REGION,RAN_PORT=RAN_PORT,LOCAL_NODE=LOCAL_NODE,IP=CAPACITY_PLANNING_IP,static_port=CAPACITY_PLANNING_PORT,getHost=getHost))
    fileh.close()

def servicedetails(IP, appsone_service):
    for jobs in xrange(len(appsone_service)):
        job_name = appsone_service.keys()[jobs];
        job_count = appsone_service.values()[jobs];
        # port_count is used to fetch the multiple unused port for single service
        for count in xrange(int(job_count)):
            port_count = int(count) * 2 + 1
            count = int(count) + 1
            uniq_service = IP + "_" + job_name + "_" + str(count)
            try:
                call = {"TXNCOLLECTOR": "txncollector(uniq_service , port_count)",
                        "ALERTINGSERVICE": "alertingservice(uniq_service , port_count)",
                        "COMPONENTAGENT": "componentagent(uniq_service)",
                        "ETL-LTM": "etlltm(uniq_service , port_count)",
                        "INCIDENTMGMTBROKER": "incidentmgmtbroker(uniq_service , port_count)",
                        "PATTERNRT": "patternrt(uniq_service)",
                        "JMXCOLLECTOR_JBOSS": "jmxcollector_jboss(uniq_service,count)",
                        "JMXCOLLECTOR": "jmxcollector(uniq_service,count)",
                        "JMXCOLLECTOR_WAS": "jmxcollector_was(uniq_service,count)",
                        "COLLATINGSERVICE": "collatingservice(uniq_service,port_count)",
                        "CXO": "cxo(uniq_service,port_count)",
                        "FORENSICSERVICE": "forensicservice(uniq_service,port_count)",
                        "JPPFKPICOLLECTOR": "jppfcollector(uniq_service,count)",
			"ETL-STITCHING" : "etlstitching(uniq_service, port_count)" ,
                        "COLLECTIONSERVICE": "collectionservice(uniq_service,port_count)",
                        "DATACOLLECTOR": "datacollector_jvm(uniq_service,count)",
                        "DATACOLLECTOR_WAS": "datacollector_was(uniq_service,count)",
                        "GRPC": "grpc(uniq_service,port_count)",
                        "NOTIFICATIONSERVICE": "notificationservice(uniq_service,port_count)",
                        "ETL-TFP": "etltfp(uniq_service,port_count)",
			"TFP-DS" : "tfp_ds(uniq_service,port_count)",
                        }
                exec(call[job_name])
            except(KeyError):
                print("Service name is incorrect: " + job_name );

def infraService():
    Infra_call = [ "haproxy()", "couchbase()", "cassandra()", "rabbitmq()", "flink()" , "capacityPlanning()" , "keycloak()", "tomcat()" ]
    for num in xrange(len(Infra_call)):
	exec(Infra_call[num])


def dbentry(agent_name,IP,uniq_id):
    try :
        cnx = mysql.connector.connect(host=DB_IP, user=ROOT_DB_USER, password=ROOT_DB_PASSWORD,  port=DB_PORT)
        cursor = cnx.cursor(buffered=True)
        Agent_query = "select Id from apmcommon.COMPMETRICSETDEF where Name = 'A1Agent';"
        cursor.execute(Agent_query)
        genAgentId = cursor.fetchone()[0]

        Cluster_query = "select Id from A1HEALTH.GENCOMPONENTDEF where Type='CLUSTER' and Name='A1AGENT_CLUSTER';"
        cursor.execute(Cluster_query)
        clusterId = cursor.fetchone()[0]

        query1 = "INSERT INTO A1HEALTH.GENCOMPONENTDEF (Name,Type,MonitorFlag,CollectionInterval,CompMetricSetId,RemoteClientName,AliasName) VALUES (%s,'CUSTOM',1,1,%s,'A1HEALTH',%s);"
        cursor.execute(query1, (agent_name,genAgentId,agent_name))

        cursor.execute("select Id from A1HEALTH.GENCOMPONENTDEF where Name IN ('%s')" % agent_name)
        maxId = cursor.fetchone()[0]

        query2 = "INSERT INTO A1HEALTH.COMPCLUSTERMAPPING (CompInstanceId, ClusterId) VALUES (%s,%s);"
        cursor.execute(query2,(maxId, clusterId))

        current_time = "select utc_timestamp();"
        cursor.execute(current_time)
        cur_time = cursor.fetchone()[0]

        query3 = "INSERT INTO apmcommon.AGENTCONFIGDEF (Id,Name,DisplayName,IPAddress,Mode,Type,UniqueIdString,CreatedTime,UpdatedTime,DataPushFrequency,CacheUpdateInterval,HealthUpdateInterval) VALUES (%s,%s,%s,%s,'MONITOR','COMPONENT',%s,%s,%s,60,60,300);"
        cursor.execute(query3, (maxId,agent_name,agent_name,IP,uniq_id,cur_time,cur_time))

        query4 = "INSERT INTO apmcommon.COMPONENTAGENTDEF (AgentId,Type,CreatedTime,UpdatedTime) VALUES (%s,'CollectionAgent',%s,%s);"
        cursor.execute(query4, (maxId,cur_time,cur_time))


    except Exception as e:
        print ("Not able to create agent identifier due to : " + str(e) )
	print ("Aborting..")
        sys.exit(107)

    cnx.commit()
    cursor.close()
    cnx.close()


def version():
    con.kv.put("version/data-collector-core",data_collector_core_vr)
    con.kv.put("version/transaction-collector",transaction_collector_vr)
    con.kv.put("version/alerting-service",alerting_service_vr)
    con.kv.put("version/component-agent-core",component_agent_core_vr)
    con.kv.put("version/log-txn-monitoring" , etl_ltm_vr)
    con.kv.put("version/incident-management-broker" , incident_mgmt_vr)
    con.kv.put("version/patternrt" , patternrt_vr)
    con.kv.put("version/tfp-etl" , etl_tfp_vr)
    con.kv.put("version/jmx-collector" , jmx_collector_vr)
    con.kv.put("version/collating-service", collating_service_vr)
    con.kv.put("version/jppf-kpi-collector", jppf_collector_vr)
    con.kv.put("version/collection-service", collection_service_vr )
    con.kv.put("version/grpc-service", grpc_vr )
    con.kv.put("version/notification-service", notification_service_vr)
    con.kv.put("version/reporting-service", reporting_service_vr)
    con.kv.put("version/stitching-etl", stitching_etl_vr)
    con.kv.put("version/tfp-ds", tfp_ds_vr)
    con.kv.put("version/sumo-dataservice",sumo_ds_vr)


def fetchKeyValue():
    logdir="log"
    try:
        os.stat(logdir)
    except:
        os.mkdir(logdir)  

    fileh = open("log/ListOfKeyValue.log", "a")
    showtime = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    fileh.write("-------------------------Details of all the key value stored in consul----------------------------\n")
    fileh.write(" Time : " + showtime + "\n")
    data=con.kv.get('service',recurse=True)
    var=data[1]
    for i in xrange(len(var)):
        fileh.write("\n Key : " + var[i]['Key'] )
	val = var[i]['Value']
	if ( val != None ):
        	fileh.write("\n Value : " + var[i]['Value'])
	fileh.write("\n")
    data=con.kv.get('version',recurse=True)
    var=data[1]
    for i in xrange(len(var)):
        fileh.write("\n Key : " + var[i]['Key'] )
	val = var[i]['Value']
	if ( val != None ):
        	fileh.write("\n Value : " + var[i]['Value'])
	fileh.write("\n")

    fileh.close()

def createDefaultMysql():
    print ("Creating fresh percona database")
    try :
	command=[createDefaultMysql_path+'/createDefaultMysql.sh',DB_IP,DB_PORT,ROOT_DB_USER,ROOT_DB_PASSWORD,createDefaultMysql_path]
	Process=Popen(command,stdout=PIPE,stderr=PIPE)
	Process.wait()
	for line in Process.stdout:
    		print line
	for line in Process.stderr:
    		print line
    except Exception as e:
	print("Not able to execute the sql queries due to" + str(e))
	sys.exit(108)

def main():
    global getHost
    getHost = HOST_ENTRY
    # configurate and start percona service
    print ("Configuring and starting percona inorder to add the agent identifiers in DB")
    percona()
    print ("Percona has started.")
    sleep(5)
    if ( MIGRATION.upper() == "FALSE" ):
    	createDefaultMysql()
    ## To collector txndetails for haproxy
    global file_txn
    file_txn = open("txn_haproxy.txt", "w")

    ## To collector collectionservice details for haproxy
    global file_col
    file_col = open("col_haproxy.txt", "w")

    ## To collector etl ltm details for haproxy
    global file_etl_ltm
    file_etl_ltm = open("etl_ltm_haproxy.txt", "w")

    ## To collector stitching etl details for haproxy
    global file_etl_stitching
    file_etl_stitching = open("etl_stitching_haproxy.txt", "w")

    ###Handling Percona and A1Services.
    global filehPort
    filehPort = open(".open_port.txt", "w")
    print ("Generating .nomad and .tpl files for all the appsone services")
    ServerDetails()
    filehPort.close()

    ### Infra Services
    print ("Generating .nomad file for all the docker services")
    infraService()

    version()
    ###
    fetchKeyValue()
    os.remove('txn_haproxy.txt')
    os.remove('col_haproxy.txt')
    os.remove('etl_ltm_haproxy.txt')
    os.remove('etl_stitching_haproxy.txt')
    print ("Generated all the .nomad file sucessfully.")

get_ips()
from service_ip import *

NOMAD_JOB_PATH = SER_INSTALL_PATH[1:]+"/Appsone_Service"
nomaddir=SER_INSTALL_PATH+"/nomadFile"
try:
    os.stat(nomaddir)
except:
    os.mkdir(nomaddir)

os.environ["NOMAD_CACERT"]="/etc/nomad/nomad-ca.pem"
os.environ["NOMAD_CLIENT_KEY"]="/etc/nomad/cli-key.pem"
os.environ["NOMAD_CLIENT_CERT"]="/etc/nomad/cli.pem"
os.environ["NOMAD_ADDR"]="https://localhost:"+ HTTP_PORT
main()
