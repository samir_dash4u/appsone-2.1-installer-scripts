#!/usr/bin/env python

from string import Template

TXNCOLLECTOR_1 = Template ("""
mysql.server.ip={{ key "service/perconadb/ip" }}
mysql.server.port={{ key "service/perconadb/port" }}
mysql.server.username={{ key "service/perconadb/username" }}
mysql.server.password={{ key "service/perconadb/password" }}
mysql.server.url=?useServerPrepStmts=true&cachePrepStmts=false&prepStmtCacheSize=200&prepStmtCacheSqlLimit=1000&zeroDateTimeBehavior=convertToNull&verifyServerCertificate=false&useSSL=true&requireSSL=true&enabledTLSProtocols=TLSv1.2
cache.update.interval={{ key "service/txncollector/cache_update_interval" }}

http.port={{ key "service/$JOB_NAME/port/http" }}
https.port={{ key "service/$JOB_NAME/port/https" }}
#
#unmatched.transaction.cache.spec=maximumSize=5000

#
# alerting-service hostname and port details for communicating with alerting-service
# default values; alerting.service.hostname=127.0.0.1 and  alerting.service.port=9194
#
alerting.service.hostname={{ key "service/alertingservice/ip" }}
alerting.service.port={{ key "service/alertingservice/https/port" }}

#
# Below property allows user to enable/disable MIXED mode of transaction collector
# default value is DISABLE(0). Values can be 1 for Enable/ 0 for disable.
#

#mixed.mode=

#
# Below property defines the schedule start,end time, batch size for MIXED mode
# default value for mixed mode start/end is current day 9:00:00 - 18:00:00
# update the mixed.start.time and end.time using format yyyy-mm-dd HH:mm:ss
# default value for batch size of discovered URL data updates is 1000
#
#mixed.start.time=
#mixed.end.time=
#mysql.batch.context.size=

""")

ETLLTM_1 = Template ("""
# Port on which TxnLog will push data
# By default the url for which data will be pushed :
# post on http://local_machine_ip:7782/transaction-data
# post on https://local_machine_ip:7784/transaction-data

# Transaction collector related Details
# Default url of transaction collector will constructed as :
# http://txn_collector_ip:txn_collector_port/ApmPtmTxnService
transaction.collector.ip = https://{{ key "service/haproxy/hostname" }}
transaction.collector.port = {{ key "service/haproxy/transactioncollector/port" }}

#http-client configuration for TC
#socket timeout unit in millisecond, default value is -1
#max connection default value is 20
#ior thread default value is present available processors
#connection timeout unit in millisecond, default value is -1
#connection request timeout unit in millisecond, default value is -1
#pending request validation frequency unit in minutes, default value is 5 (min)
etl.tc.socket.timeout={{ key "service/etl/${JOB_NAME}/tcsockettimeout" }}
etl.tc.max.connection={{ key "service/etl/${JOB_NAME}/tcmaxconn" }}
etl.tc.ior.threads={{ key "service/etl/${JOB_NAME}/iorthreads" }}
etl.tc.connection.timeout={{ key "service/etl/${JOB_NAME}/tcconntimeout" }}
etl.tc.connection.request.timeout={{ key "service/etl/${JOB_NAME}/tcconnreqtimeout" }}
etl.tc.pending.req.validation.freq={{ key "service/etl/${JOB_NAME}/pendingreqvalfreq" }}

#
# default http port is 7782
#
txnlog.http.push.port = {{ key "service/etl/${JOB_NAME}/http/port" }}
#
# default https port is 7784
#
txnlog.https.push.port = {{ key "service/etl/${JOB_NAME}/https/port" }}
#
# txnlog.protocol.run.mode property take three arguments
# HTTP - run it in http mode
# HTTPS - run it in https mode
# BOTH - run it in both mode
#
# default mode is http
#
#Min/Max Thread value for web server's thread pool
#The details of resful.threads.max property is as below
#resful.threads.max ->The maximum number of threads that can be created by the application.So,for example if numberOfCore is 4 then resful.threads.max = sum of (Selector(4)+Acceptors(1)+Request(1)) = 6.
#Selector -> This must be as numberOfCores of machine.
#If 4 core then it should be 4.
#If 8 core then it should be 8.
#If 8 core and above it should be 8.
#Acceptors -> It is calculated as max(1,min(4,numberOfCores/8)).
#For eg: For Quadcore max(1,min(4,4/8)).
#Request must be always 1
#
restful.threads.min = {{ key "service/etl/${JOB_NAME}/threads/min" }}
restful.threads.max = {{ key "service/etl/${JOB_NAME}/threads/max" }}
txnlog.protocol.run.mode = {{ key "service/etl/${JOB_NAME}/runmode" }}
#here we can give separator as regex for fi log.
txnlog.fi.regex=,
txnlog.fi.params=ROUTE_NAME=0,REQUESTUUID=2,TRANSTAT=3,FAILSTAT=4,StrTime=5,EndTime=6,REQUESTTYPE=7
txnlog.fi.prefix=FI
txnlog.fiTxn.time.diff={{ key "service/etl/${JOB_NAME}/fi/timediff" }}
txnlog.fi.identifier={{ key "service/etl/${JOB_NAME}/fi/agentid" }}
txnlog.fi.timezone={{ key "service/etl/${JOB_NAME}/fi/timezone" }}

#ltm config details
txnlog.ltm.total.instances=2
txnlog.ltm.filters = @GET@js( |?)@
txnlog.ltm.prefix = AccessMon

#If we have multiple instances running then for each instance we can give identifier and timezone like below
#Instance-1 config details
txnlog.ltm.identifier.instance1={{ key "service/etl/${JOB_NAME}/ltm/agentid" }}
txnlog.timezone.instance1={{ key "service/etl/${JOB_NAME}/ltm/timezone" }}
txnlog.ltm.regex.instance1=^\\S+ (\\S+) \\S+ \\S+ \\S+ \\S+ (\\S+) (\\S+) (\\S+) \\S+ \\S+ \\[(\\S+) \\S+\\] \"([^\"]+)\" (\\S+) (\\S+) .*
txnlog.ltm.params.instance1=CIP, SIp,SPort,RespSize,EndTime, URL, RespCode, RespTime
txnlog.ltm.eum.filter.isEnabled.instance1={{ key "service/etl/${JOB_NAME}/ltm/eumfilter" }}

#Instance-2 config details for ltm-eum
#txnlog.ltm.identifier.instances2=ase5434-234rc234-4234r452-qwerq
#txnlog.timezone.instances2= +05:30
#txnlog.ltm.eum.filter.isEnabled.instance2=false

################################### db-sink configuration ###################################
#If you want to enable db-sink the please give below configuration
#DB-SINK-to enable db-sink and TC-SINK to enable transaction sink, default value is BOTH
etl.enable.sink={{ key "service/etl/${JOB_NAME}/global/sinktype" }}
#Enable ETL meter to check received and post data, by default it would be disable (value is true or false)
etl.meter.enabled={{ key "service/etl/${JOB_NAME}/meter/enabled" }}
#Default value is 10 (min)
etl.meter.start.time={{ key "service/etl/${JOB_NAME}/meter/starttime" }}

# Mysql Database Details, uncomment and set each properties if different from default
# default Values; mysql.server.ip=127.0.0.1 mysql.server.port=3306  mysql.server.username=appsone mysql.server.password=l5z3Vu20myM=  (#App$0ne)
jdbc.server.username={{ key "service/perconadb/username" }}
jdbc.server.password={{ key "service/perconadb/password" }}
jdbc.server.url=jdbc:mysql://{{ key "service/perconadb/ip" }}:{{ key "service/perconadb/port" }}/apmcommon?verifyServerCertificate=false&useSSL=true&requireSSL=true&enabledTLSProtocols=TLSv1.2
jdbc.max.active.connection={{ key "service/etl/${JOB_NAME}/maxactiveconn" }}

#Please give proper ds version, default version is - 4.5
etl.ds.version={{ key "service/etl/${JOB_NAME}/ds/version" }}
#If 'etl.enable.sink' has BOTH value then we have to enable log type level sink(DB-SINK or TC-SINK) otherwise by default every logtype will call TC-SINK
etl.fi.sink={{ key "service/etl/${JOB_NAME}/fi/sinktype" }}
etl.feba.sink={{ key "service/etl/${JOB_NAME}/feba/sinktype" }}
etl.ltm.dc.sink={{ key "service/etl/${JOB_NAME}/dc/sinktype" }}
etl.ltm.eum.sink={{ key "service/etl/${JOB_NAME}/eum/sinktype" }}

#Im-memory collation frequency, default value is 60 sec
inmemory.scheduler.time.interval={{ key "service/etl/${JOB_NAME}/scheduler/inmemory" }}

#cache updater frequency, default value is 60 sec
cahce.scheduler.time.interval={{ key "service/etl/${JOB_NAME}/scheduler/cache" }}

#Attribute names to create transaction name. These Attributes name should be comma separated.
#Example- if etl.fi.txn.fields=route_name,requestuuid then transaction name would be like below
#Transaction Name = ROUTEA_201 (Values of these attributes)
#Where values for attributes are -
#route_name - ROUTEA
#requestuuid - 201
etl.fi.txn.fields={{ key "service/etl/${JOB_NAME}/fi/dbtxnfields" }}
etl.feba.txn.fields={{ key "service/etl/${JOB_NAME}/feba/dbtxnfields" }}
etl.ltm.dc.txn.fields={{ key "service/etl/${JOB_NAME}/dc/dbtxnfields" }}
etl.ltm.eum.txn.fields={{ key "service/etl/${JOB_NAME}/eum/dbtxnfields" }}
""")


DATACOLLECTOR_1 = Template ("""
# Set the below property value to unique id which is generated by dash board for the data collector agent.
datacollector.uniqueid={{ key "service/datacollector/${JOB_NAME}/uniqueid" }}

# Collection Service URL for configuration service
datacollector.configserviceurl=https://{{ key "service/haproxy/ip" }}:{{ key "service/haproxy/collectionservice/port" }}/CollectionServiceConfig

# Collection Service URL for data service
datacollector.dataserviceurl=https://{{ key "service/haproxy/ip" }}:{{ key "service/haproxy/collectionservice/port" }}/CollectionServiceData

# Below property value sets the collectors thread pool size and this value is based on number of cpu cores
# These threads are used to submit the individual component data collection
# default value of the minimum pool size is 250
# default value of the maximum pool size is 1000
#
datacollector.collectors.threadpool.minimum.size=250
datacollector.collectors.threadpool.maximum.size=1000

# CollectorsPoolCache holds collectors instances to avoid instantiation of individual collectors for every collection
# set the maximumSize attribute value to number of component instances mapped to respective data collector agent
# default value of the spec is maximumSize=250
#
datacollector.collectors.instancespool.cache.spec=maximumSize=250

""")


ETLTFP_1 = Template ("""
################################## tfp-etl configuration ###################################
 #
 #==============================================
 # APPSONE - Mysql Database Configuration
 #==============================================
 #
 mysql.database.hostname = {{ key "service/perconadb/ip" }}
 mysql.database.common.schema = apmcommon
 mysql.database.port = {{ key "service/perconadb/port" }}
 mysql.database.username = {{ key "service/perconadb/username" }}
 mysql.database.password = {{ key "service/perconadb/password" }}
 mysql.url.property = ?verifyServerCertificate=false&useSSL=true&requireSSL=true&enabledTLSProtocols=TLSv1.2
 mysql.mapping.data.fetch.interval.inSeconds = {{ key "service/tfp-etl/mysql_mapping_data_fetch_interval_seconds" }}
 
 ################################ General configuration ################################
 alert.description.characters = {{ key "service/tfp-etl/alert_description_characters" }}
 etl.port = {{ key "service/tfp-etl/${JOB_NAME}/port" }}
 
 # Below properties used to retrieve the component/transaction data for 2 minutes lag (assuming the 2 minutes time
 #taken for collation), Changing this value will cause these many minutes delay in UI.
 component.kpi.data.lag.in.minutes = {{ key "service/tfp-etl/kpi_data_lag_in_minutes" }}
 transaction.data.lag.in.minutes = {{ key "service/tfp-etl/txn_data_lag_in_minutes" }}
 
 
 # IpAddress of ETL need to be provided
 local.ip.address = {{ key "service/tfp-etl/${JOB_NAME}/ip" }}
 
 # Provide fail-over TFP IpAddresses in comma separated in case of multiple fail-overs.
 #remote.ip.address =
 ################################## tfp-etl configuration ###################################
 #
 #==============================================
 # RabbitMQ Configuration
 #==============================================
 #
 rabbitmq.host = {{ key "service/rabbitmq/ip" }}
 rabbitmq.port = {{ key "service/rabbitmq/port" }}
 #rabbitmq.username = guest
 #rabbitmq.password = DeMfI1pDi48=
 rabbitmq.alerts.queue.name = {{ key "service/tfp-etl/alerts_queue_name" }}
 rabbitmq.transaction.queue.name = {{ key "service/tfp-etl/txn_queue_name" }}
 rabbitmq.component.kpi.queue.name = {{ key "service/tfp-etl/kpi_queue_name" }}
 
 
 # Added a feature to send historical transaction data for given range in start time and end time and
 # also to enable this historical data feature should uncomment below properties.
 # NOTE: Its not recommended to give range more than 14 days, dates mentioned in should be always in GMT.
 #==============================================
 #historical.transaction.data=1
 #historical.transaction.data.startime=2018-01-29 13:45:00
 #historical.transaction.data.endtime=2018-01-29 15:53:00
 
 
 # Added a feature to send historical component data for given range in start time and end time and
 # also to enable this historical data feature should uncomment below properties.
 # NOTE: Its not recommended to give range more than 14 days, dates mentioned in should be always in GMT.
 #==============================================
 #historical.component.data=1
 #historical.component.data.startime=2017-11-09 18:30:00
 #historical.component.data.endtime=2017-11-10 18:29:00
 
 # Based on below property, ETL will be sending component KPI data to Queue. By default component data will be
 # sent to queue, if user want to stop sending component data then uncomment below property and update the value to 1.
 #disable.component.data = 1
""")

ETLSTITCHING_1 = Template ("""
# Port on which TxnLog will push data
# By default the url for which data will be pushed :
# post on http://local_machine_ip:8892/transaction-data
#post on https://local_machine_ip:8894/transaction-data
#
# Transaction collector related Details
# Default url of transaction collector will constructed as :
# http://txn_collector_ip:txn_collector_port/ApmPtmTxnService
transaction.collector.ip = https://{{ key "service/haproxy/hostname" }}
transaction.collector.port = {{ key "service/haproxy/transactioncollector/port" }}
# Time in seconds for event to be timed-out
event.delayed.time = {{ key "service/etl/${JOB_NAME}/evtdelayedtime" }}

#http-client configuration for TC
#socket timeout unit in millisecond, default value is -1
#max connection default value is 20
#ior thread default value is present available processors
#connection timeout unit in millisecond, default value is -1
#connection request timeout unit in millisecond, default value is -1
#pending request validation frequency unit in minutes, default value is 5 (min)
etl.tc.socket.timeout={{ key "service/etl/${JOB_NAME}/tcsockettimeout" }}
etl.tc.max.connection={{ key "service/etl/${JOB_NAME}/tcmaxconn" }}
etl.tc.ior.threads={{ key "service/etl/${JOB_NAME}/iorthreads" }}
etl.tc.connection.timeout={{ key "service/etl/${JOB_NAME}/tcconntimeout" }}
etl.tc.connection.request.timeout={{ key "service/etl/${JOB_NAME}/tcconnreqtimeout" }}
etl.tc.pending.req.validation.freq={{ key "service/etl/${JOB_NAME}/pendingreqvalfreq" }}

#
# default http port is 8892
#
etl.http.push.port = {{ key "service/etl/${JOB_NAME}/httpport" }}
#
# default https port is 8894
#
etl.https.push.port = {{ key "service/etl/${JOB_NAME}/httpsport" }}
#
# etl.protocol.run.mode property take three arguments
#  HTTP - run it in http mode
#  HTTPS - run it in https mode
#  BOTH - run it in both mode
#
#  default mode is http
#
etl.protocol.run.mode = {{ key "service/etl/${JOB_NAME}/runmode" }}

#
#
#Min/Max Thread value for web server's thread pool
#The details of resful.threads.max property is as below
#resful.threads.max ->The maximum number of threads that can be created by the application.So,for example if numberOfCore is 4 then resful.threads.max = sum of (Selector(4)+Acceptors(1)+Request(1)) = 6.
#Selector = This must be as numberOfCores of machine.
#If 4 core then it should be 4.
#If 8 core then it should be 8.
#If 8 core and above it should be 8.
#Acceptors = It is calculated as max(1,min(4,numberOfCores/8)).
#For eg: For Quadcore max(1,min(4,4/8)).
#Request must be always 1
#
restful.threads.min = {{ key "service/etl/${JOB_NAME}/threads/min" }}
restful.threads.max = {{ key "service/etl/${JOB_NAME}/threads/max" }}
#
#
################################## npci configuration for unprocessed data ###################################
#Below configuration are using to filter and process data on ETL side for npci log
npci.req.start=.*(UPIPayReqServiceImpl | logPayCreditDebitReq | line No :754).*
npci.req.data=id=\"([^\"]+)\".+?ts=\"([^\"]+)\" type=\"([^\"]+)\".+?Payer addr=\"([^\"]+)\".+?Payee addr=\"([^\"]+)\"
npci.req.params=ID,STARTTIME,Rtype,PAYER,PAYEE
npci.res.start=.*(ReqPayTransNPCIController | rcvResPayResponse | line No :186).*
npci.res.data=id=\"([^\"]+)\".+?ts=\"([^\"]+)\" type=\"([^\"]+)\".+?result=\"([^\"]+)\".+?Ref addr=\"([^\"]+)\".+?Ref addr=\"([^\"]+)\"
npci.res.params=ID,ENDTIME,Rtype,STATUS,PAYER,PAYEE
npci.filters=@GET@js( |?)@
npci.prefix=NPCI
npci.identifier={{ key "service/etl/${JOB_NAME}/npci/agentid" }}
npci.timezone={{ key "service/etl/${JOB_NAME}/npci/timezone" }}

#
################################## db-sink configuration ###################################
#If you want to enable db-sink the please give below configuration
#DB-SINK-to enable db-sink and TC-SINK to enable transaction sink, default value is BOTH
etl.enable.sink={{ key "service/etl/${JOB_NAME}/global/sinktype" }}


# Mysql Database Details, uncomment and set each properties if different from default
# default Values; mysql.server.ip=127.0.0.1 mysql.server.port=3306  mysql.server.username=appsone mysql.server.password=l5z3Vu20myM=  (#App$0ne)
jdbc.server.username={{ key "service/perconadb/username" }}
jdbc.server.password={{ key "service/perconadb/password" }}
jdbc.server.url=jdbc:mysql://{{ key "service/perconadb/ip" }}:{{ key "service/perconadb/port" }}/apmcommon?verifyServerCertificate=false&useSSL=true&requireSSL=true&enabledTLSProtocols=TLSv1.2
jdbc.max.active.connection={{ key "service/etl/${JOB_NAME}/maxactiveconn" }}


#Please give proper ds version, default version is - 4.5
etl.ds.version={{ key "service/etl/${JOB_NAME}/ds/version" }}

etl.c24.sink={{ key "service/etl/${JOB_NAME}/c24/sinktype" }}
etl.atm.bom.sink={{ key "service/etl/${JOB_NAME}/bom/sinktype" }}
#Im-memory collation frequency, default value is 60 sec
inmemory.scheduler.time.interval={{ key "service/etl/${JOB_NAME}/scheduler/inmemory" }}

#cache updater frequency, default value is 60 sec
cahce.scheduler.time.interval={{ key "service/etl/${JOB_NAME}/scheduler/cache" }}

#Attribute names to create transaction name. These Attributes name should be comma separated.
#Example- if etl.txn.fields=stiched,command,dccId then transaction name would be like below
#Transaction Name = STR_CWMR_ATM (Values of these attributes)
#Where values for attributes are -
#stiched - STR (for stiched and UNSTR for unstiched)
#command - CWMR
#dccId - ATM
#Default attributes are  - stiched,command,dccId for C24 and stiched,id for npci
c24.etl.txn.fields={{ key "service/etl/${JOB_NAME}/c24/dbtxnfields" }}
npci.etl.txn.fields={{ key "service/etl/${JOB_NAME}/npci/dbtxnfields" }}
atm.bom.etl.txn.fields={{ key "service/etl/${JOB_NAME}/bom/dbtxnfields" }}
""")

COLLECTIONSERVICE_1 = Template ("""
#
# collection-service configuration property file
#

#
# collection-service http and https port details
# Valid http.portRange (9100-9119)
# Valid https.port range (9120-9139)
# default http port is 9100 and https port is 9120.
#
http.port={{ key "service/${JOB_NAME}/port/http" }}
https.port={{ key "service/${JOB_NAME}/port/https" }}

#
# alerting-service hostname and port details for communicating with alerting-service
# default values; alerting.service.hostname=127.0.0.1 and  alerting.service.port=9194
#
alerting.service.hostname={{ key "service/alertingservice/ip" }}
alerting.service.port={{ key "service/alertingservice/https/port" }}
""")

