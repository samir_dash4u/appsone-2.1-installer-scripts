#!/usr/bin/env python

from string import Template

HAPROXY_CONF = Template ("""
global
    log         127.0.0.1 local2

    pidfile     /var/run/haproxy.pid
    maxconn     2048
    tune.ssl.default-dh-param 2048
#    ssl-default-server-options force-tlsv12
#    ssl-default-bind-options force-tlsv12
    nbproc 2
    daemon

defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m

listen stats # Define a listen section called "stats"
  bind :9999 ssl crt /etc/ssl/certs/haproxy.pem
  stats enable  # Enable stats page
  stats hide-version  # Hide HAProxy version
  stats realm Haproxy\ Statistics  # Title text for popup window
  stats uri /haproxy_stats  # Stats URI
  stats auth ${Username}:${Password}  # Authentication credentials

frontend etl_ltm
  bind *:7789 ssl crt /etc/ssl/certs/haproxy.pem
  reqadd X-Forwarded-Proto:\ https
  acl url_etl path_beg -i /v2/transaction-data
  reqrep ^([^\ :]*)\ /v2/(.*)     \\1\ /\\2

  use_backend  etl_ltmv2 if url_etl
  default_backend etl_ltmv2

frontend etl_stitching
  bind *:7790 ssl crt /etc/ssl/certs/haproxy.pem
  reqadd X-Forwarded-Proto:\ https
  acl url_etl path_beg -i /v2/transaction-data
  reqrep ^([^\ :]*)\ /v2/(.*)     \\1\ /\\2

  use_backend  etl_stitchingv2 if url_etl
  default_backend etl_stitchingv2

frontend main_txn
  bind *:443 ssl crt /etc/ssl/certs/haproxy.pem
  reqadd X-Forwarded-Proto:\ https
  acl url_txndata  path_beg  -i  /v2/ApmPtmTxnService
  acl url_txnConfigData  path_beg  -i  /v2/AppsOnePSConfigService
  acl url_txnHealthData  path_beg  -i  /v2/AppsOneAgentHealthService
  acl url_JimData  path_beg  -i   /v2/AppsOneJIMDataService
  acl url_JimEchoData  path_beg  -i  /v2/AppsOneEchoService
  reqrep ^([^\ :]*)\ /v2/(.*)    \\1\ /\\2

  use_backend  txncollectorsv2  if url_txndata
  use_backend  txncollectorsv2  if url_txnConfigData
  use_backend  txncollectorsv2  if url_txnHealthData
  use_backend  txncollectorsv2  if url_JimData
  use_backend  txncollectorsv2  if url_JimEchoData
  default_backend txncollectorsv2

frontend main_collection
  bind *:445 ssl crt /etc/ssl/certs/haproxy.pem
  reqadd X-Forwarded-Proto:\ https
  acl url_colldata    path_beg    -i  /v2/CollectionServiceData
  acl url_collConfigData  path_beg    -i  /v2/CollectionServiceConfig
  reqrep ^([^\ :]*)\ /v2/(.*)     \\1\ /\\2

  use_backend CollectionServicev2   if url_colldata
  use_backend CollectionServicev2  if url_collConfigData
  default_backend CollectionServicev2
 
backend txncollectorsv2
  balance roundrobin
  option http-keep-alive
  http-reuse always
  $txn_ha

backend CollectionServicev2
  balance roundrobin
  option http-keep-alive
  http-reuse always
  $col_ha 

backend etl_ltmv2
  mode http
  retries 3
  option httplog
  option http-keep-alive
  balance roundrobin
  http-reuse always
  $etl_ltm_ha

backend etl_stitchingv2
  mode http
  retries 3
  option httplog
  option http-keep-alive
  balance roundrobin
  http-reuse always
  $etl_stitching_ha

""")

HAPROXY = Template("""
job "haproxy" {
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]

        task "haproxy" {
                driver = "docker"

                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }

                artifact {
                        source = "https://${getHost}:${RAN_PORT}/haproxy.tar"
                }

                config {
                        load = "haproxy.tar"
                        image = "abh1sh3k/haproxy-ssl:appsone"
                        volumes = [
                                "/$NOMAD_JOB_PATH/conf/HAPROXY:/usr/local/etc/haproxy",
                                "/$NOMAD_JOB_PATH/data/haproxy/haproxy.pem:/etc/ssl/certs/haproxy.pem",
				"/etc/localtime:/etc/localtime"
                        ]

                        port_map {
                                txn_https_port = 443
                                kpi_https_port = 445
                                haproxy_stats = 9999
				etl_ltm_https_port = 7789
				etl_stitching_https_port = 7790
                        }
                }

                resources {
                        cpu = 500
                        memory = 2048
                        network {
                                mbits = "10"
                                port "txn_https_port" {
                                        static = "$HAPROXY_TXN_PORT"
                                }
                                port "kpi_https_port" {
                                        static = "$HAPROXY_KPI_PORT"
                                }
                                port "haproxy_stats" {
                                        static = "$STAT_PORT"
                                }
				port "etl_ltm_https_port" {
					static = "$HAPROXY_ETL_LTM_PORT"
				}
				port "etl_stitching_https_port" {
					static = "$HAPROXY_ETL_STITCHING_PORT"
				}
                        }
                }
                               
                service {
                        name = "haproxy"
                        tags = ["haproxy"]
                        port = "haproxy_stats"
                }
        }
} 
""")


CASSANDRA = Template ("""
job "cassandra" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]

	task "cassandra-node-1" {
		driver = "docker"
	
		constraint {
			attribute = "${attr.unique.network.ip-address}"
			value = "$CASSANDRA_BROADCAST_ADDRESS_Node1"
		}
		artifact {
                        source = "https://${getHost}:${RAN_PORT}/cassandra.tar"
                }

		config {
			load = "cassandra.tar"
			image = "cassandra:2.2.12"
			port_map {
				gossip = 7000 
				transport = 9042
			}	

			volumes = [
				"/$NOMAD_JOB_PATH/data/cassandra:/var/lib/cassandra",
				"/etc/localtime:/etc/localtime"
			]
		}
	
		resources {
			cpu = 500
			memory = 1524
			network {
				mbits = "10"
				port "gossip" {
					static = "7000"
				}
				port "transport" {
					static = "$port"
				}
			}		
		}

		service {
			name = "cassandra-node-1"
			tags = ["cassandra-node-1"]
			port = "gossip"
		}

		template {
			data = <<EOH
				CASSANDRA_BROADCAST_ADDRESS="{{key "service/cassandra/node1/CASSANDRA_BROADCAST_ADDRESS"}}"
				CASSANDRA_SEEDS="{{key "service/cassandra/node1/CASSANDRA_BROADCAST_ADDRESS"}},{{key "service/cassandra/node2/CASSANDRA_BROADCAST_ADDRESS"}},{{key "service/cassandra/node3/CASSANDRA_BROADCAST_ADDRESS"}}"
			EOH
		
			destination = "secrets/file.env"	
			env = true
		}
	}
	
	task "cassandra-node-2" {
		driver = "docker"
	
		constraint {
			attribute = "${attr.unique.network.ip-address}"
			value = "$CASSANDRA_BROADCAST_ADDRESS_Node2"
		}
		artifact {
                        source = "https://${getHost}:${RAN_PORT}/cassandra.tar"
                }


		config {
			load = "cassandra.tar"
			image = "cassandra:2.2.12"
			port_map {
				gossip = 7000 
				transport = 9042
			}	

			volumes = [
				"/$NOMAD_JOB_PATH/data/cassandra:/var/lib/cassandra",
				"/etc/localtime:/etc/localtime"
			]
		}
	
		resources {
			cpu = 500
			memory = 1524
			network {
				mbits = "10"
				port "gossip" {
					static = "7000"
				}
				port "transport" {
					static = "$port"
				}
			}		
		}

		service {
			name = "cassandra-node-2"
			tags = ["cassandra-node-2"]
			port = "gossip"
		}

		template {
			data = <<EOH
				CASSANDRA_BROADCAST_ADDRESS="{{key "service/cassandra/node2/CASSANDRA_BROADCAST_ADDRESS"}}"
				CASSANDRA_SEEDS="{{key "service/cassandra/node1/CASSANDRA_BROADCAST_ADDRESS"}},{{key "service/cassandra/node2/CASSANDRA_BROADCAST_ADDRESS"}},{{key "service/cassandra/node3/CASSANDRA_BROADCAST_ADDRESS"}}"
			EOH
		
			destination = "secrets/file.env"	
			env = true
		}
	}

	task "cassandra-node-3" {
		driver = "docker"
	
		constraint {
			attribute = "${attr.unique.network.ip-address}"
			value = "$CASSANDRA_BROADCAST_ADDRESS_Node3"
		}
		artifact {
                        source = "https://${getHost}:${RAN_PORT}/cassandra.tar"
                }


		config {
			load = "cassandra.tar"
			image = "cassandra:2.2.12"
			port_map {
				gossip = 7000 
				transport = 9042
			}	
			
			volumes = [
				"/$NOMAD_JOB_PATH/data/cassandra:/var/lib/cassandra",
				"/etc/localtime:/etc/localtime"
			]
		}
	
		resources {
			cpu = 500
			memory = 1524
			network {
				mbits = "10"
				port "gossip" {
					static = "7000"
				}
				port "transport" {
					static = "$port"
				}
			}		
		}

		service {
			name = "cassandra-node-3"
			tags = ["cassandra-node-3"]
			port = "gossip"
		}

		template {
			data = <<EOH
				CASSANDRA_BROADCAST_ADDRESS="{{key "service/cassandra/node3/CASSANDRA_BROADCAST_ADDRESS"}}"
				CASSANDRA_SEEDS="{{key "service/cassandra/node1/CASSANDRA_BROADCAST_ADDRESS"}},{{key "service/cassandra/node2/CASSANDRA_BROADCAST_ADDRESS"}},{{key "service/cassandra/node3/CASSANDRA_BROADCAST_ADDRESS"}}"
			EOH
		
			destination = "secrets/file.env"	
			env = true
		}
	}
}

""")

COUCHBASE = Template ("""
job "couchbase" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]

	task "couchbase" {
		driver = "docker"
	
		constraint {
			attribute = "${attr.unique.network.ip-address}"
			value = "$IP"
		}

		artifact {
                        source = "https://${getHost}:${RAN_PORT}/couchbase.tar"
                }

		config {
			load = "couchbase.tar"
			image = "couchbase:community-4.5.1"
			port_map {
				http = 8091
				connect = 11210
			}	

			volumes = [
				"/$NOMAD_JOB_PATH/data/couchbase:/opt/couchbase/var",
				"/etc/localtime:/etc/localtime"
			]
		}
	
		resources {
			cpu = 500
			memory = 1524
			network {
				mbits = "10"
				port "http" {
					static = "${http_port}"
				}
				port "connect" {
					static = "${connect_port}"
				}
			}		
		}

		service {
			name = "couchbase"
			tags = ["couchbase"]
			port = "http"
		}

	}
	
}
""")


PERCONA = Template ("""
job "percona" {
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]

	constraint {
		attribute = "${attr.unique.network.ip-address}"
		value = "${DB_IP}"
	}

        task "percona" {
        	driver = "docker"

		artifact {
                        source = "https://${getHost}:${RAN_PORT}/percona.tar"
                }

		template {
			data = <<EOH
				CONSUL_URL="{{ key "service/consul/url" }}"
			EOH

			destination = "local/file.env"
			env = true
		}

                config {
			load = "percona.tar"
			image = "abh1sh3k/percona:5.7"
                        port_map {
                        	db = 3306
                        }

                        volumes = [
                        	"/${NOMAD_JOB_PATH}/conf/PERCONA:/etc/mysql/conf.d",
                                "/${NOMAD_JOB_PATH}/data/percona:/var/lib/mysql",
				"/etc/localtime:/etc/localtime",
				"/etc/hosts:/etc/hosts"
                	]
                }

                resources {
                        cpu = 500
                        memory = 2048
                        network {
                                mbits = "10"
                                port "db" {
                                	static = "${DB_PORT}"
                        	}
                	}
       		}
		service {
			name = "percona"
			tags = ["percona"]
			port = "db"
		}
	}
}

""")


RABBITMQ = Template ("""
job "rabbitmq" {
	region = "${REGION}"
	datacenters = ["${DATA_CENTER}"]

	constraint {
		attribute = "${attr.unique.network.ip-address}"
		value = "$IP"
	}

	task "rabbitmq" {
		driver = "docker"

		artifact {
                        source = "https://${getHost}:${RAN_PORT}/rabbitmq.tar"
                }
	
		config {	
			load = "rabbitmq.tar"
			image = "rabbitmq:3.7.4-management-alpine"
			port_map {
				amqp = 5672
				http = 15672
			}	
			
		}
	
		resources {
			cpu = 500
			memory = 1024
			network {
				mbits = "10"
				port "amqp" {
					static = "${amqp_port}"
				}
				port "http" {
					static = "${http_port}"
				}
			}		
		}

		service {
			name = "rabbitmq-server"
			tags = ["mq", "rabbitmq"]
			port = "amqp"
		}
	}
	
}

""")

FLINK = Template ("""
job "flink" {
	region = "$REGION"
	datacenters = ["${DATA_CENTER}"]

	task "flink-jobmanager-1" {
                driver = "docker"

                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP_1"
                }

		artifact {
                        source = "https://${getHost}:${RAN_PORT}/flink.tar"
                }

		template {
                       data = <<EOH
				LOG_MODE = "{{key "service/flink/jobmanager/node1/loglevel"}}"
				JOB_MANAGER_RPC_ADDRESS = "{{key "service/flink/jobmanager/node1/ip"}}"
				HADOOP_NAMENODE_HOST = "{{key "service/hadoopnamenode/ip"}}"
				HADOOP_NAMENODE_PORT = "{{key "service/hadoopnamenode/port"}}"
				ZOOKEEPER_HOST = "{{key "service/zookeeper/ip"}}"	
				ZOOKEEPER_PORT = "{{key "service/zookeeper/port"}}"
				HIGH_AVAILABILITY_PORT = "{{key "service/flink/jobmanager/ha_port"}}"
			EOH
		
			destination = "secrets/file.env"	
			env = true
                }
		template {
			source = "/${NOMAD_JOB_PATH}/conf-template/tfp-pipeline/conf.properties.tpl"
			destination = "local/tfp-pipeline/config/conf.properties"
			change_mode = "restart"
		}
		template {
			source = "/${NOMAD_JOB_PATH}/conf-template/pipeline/conf.properties.tpl"
			destination = "local/pipeline/config/conf.properties"
			change_mode = "restart"
		}
	
                config {
			load = "flink.tar"
                        image = "abh1sh3k/flink:1.3.3-hadoop27-scala_2.10-alpine"

			volumes = [ 
				"/${NOMAD_JOB_PATH}/tfp-pipeline:/opt/appnomic/tfp-pipeline",
				"/${NOMAD_JOB_PATH}/pipeline:/opt/appnomic/appsone/pipeline",
				"local/tfp-pipeline/config/conf.properties:/opt/appnomic/tfp-pipeline/config/conf.properties",
				"local/pipeline/config/conf.properties:/opt/appnomic/appsone/pipeline/config/conf.properties",
				"/etc/localtime:/etc/localtime"
			]

			args = [
				"jobmanager",
			]

                        port_map {
				jobmanager_rpc_port = "6123"
				jobmanager_blob_port = "6124"
				web_port = "8081"
                        }
                }

                resources {
                        cpu = 500
                        memory = 1524
                        network {
                                mbits = "10"
                                port "jobmanager_rpc_port" {
                                        static = "$RPC_PORT"
                                }
                                port "jobmanager_blob_port" {
                                        static = "$BLOB_PORT"
                                }
				port "web_port" {
                                        static = "$WEB_PORT"
                                }
                        }
                }
				
		service {
                        name = "flink-jobmanager"
                        tags = ["flink-jobmanager"]
                        port = "jobmanager_rpc_port"
                }
	}

	task "flink-jobmanager-2" {
                driver = "docker"

                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP_2"
                }

		artifact {
                        source = "https://${getHost}:${RAN_PORT}/flink.tar"
                }

		template {
                       data = <<EOH
				LOG_MODE = "{{key "service/flink/jobmanager/node2/loglevel"}}"
				JOB_MANAGER_RPC_ADDRESS = "{{key "service/flink/jobmanager/node2/ip"}}"
				HADOOP_NAMENODE_HOST = "{{key "service/hadoopnamenode/ip"}}"
				HADOOP_NAMENODE_PORT = "{{key "service/hadoopnamenode/port"}}"
				ZOOKEEPER_HOST = "{{key "service/zookeeper/ip"}}"	
				ZOOKEEPER_PORT = "{{key "service/zookeeper/port"}}"
				HIGH_AVAILABILITY_PORT = "{{key "service/flink/jobmanager/ha_port"}}"
			EOH
		
			destination = "secrets/file.env"	
			env = true
                }
                template {
                        source = "/${NOMAD_JOB_PATH}/conf-template/tfp-pipeline/conf.properties.tpl"
                        destination = "local/tfp-pipeline/config/conf.properties"
                        change_mode = "restart"
                }
                template {
                        source = "/${NOMAD_JOB_PATH}/conf-template/pipeline/conf.properties.tpl"
                        destination = "local/pipeline/config/conf.properties"
                        change_mode = "restart"
                }

	
                config {
			load = "flink.tar"
                        image = "abh1sh3k/flink:1.3.3-hadoop27-scala_2.10-alpine"

			volumes = [ 
				"/${NOMAD_JOB_PATH}/tfp-pipeline:/opt/appnomic/tfp-pipeline",
				"/${NOMAD_JOB_PATH}/pipeline:/opt/appnomic/appsone/pipeline",
                                "local/tfp-pipeline/config/conf.properties:/opt/appnomic/tfp-pipeline/config/conf.properties",
                                "local/pipeline/config/conf.properties:/opt/appnomic/appsone/pipeline/config/conf.properties",
				"/etc/localtime:/etc/localtime"
			]

			args = [
				"jobmanager",
			]

                        port_map {
				jobmanager_rpc_port = "6123"
				jobmanager_blob_port = "6124"
				web_port = "8081"
                        }
                }

                resources {
                        cpu = 500
                        memory = 1524
                        network {
                                mbits = "10"
                                port "jobmanager_rpc_port" {
                                        static = "$RPC_PORT"
                                }
                                port "jobmanager_blob_port" {
                                        static = "$BLOB_PORT"
                                }
				port "web_port" {
                                        static = "$WEB_PORT"
                                }
                        }
                }
				
		service {
                        name = "flink-jobmanager"
                        tags = ["flink-jobmanager"]
                        port = "jobmanager_rpc_port"
                }
	}	

	task "flink-taskmanager-1" {
                driver = "docker"

                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP_1"
                }

		artifact {
                        source = "https://${getHost}:${RAN_PORT}/flink.tar"
                }

		template {
                       data = <<EOH
				LOG_MODE = "{{key "service/flink/taskmanager/node1/loglevel"}}"
				HADOOP_NAMENODE_HOST = "{{key "service/hadoopnamenode/ip"}}"
				HADOOP_NAMENODE_PORT = "{{key "service/hadoopnamenode/port"}}"
				ZOOKEEPER_HOST = "{{key "service/zookeeper/ip"}}"	
				ZOOKEEPER_PORT = "{{key "service/zookeeper/port"}}"
			EOH
		
			destination = "secrets/file.env"	
			env = true
                }
                template {
                        source = "/${NOMAD_JOB_PATH}/conf-template/tfp-pipeline/conf.properties.tpl"
                        destination = "local/tfp-pipeline/config/conf.properties"
                        change_mode = "restart"
                }
                template {
                        source = "/${NOMAD_JOB_PATH}/conf-template/pipeline/conf.properties.tpl"
                        destination = "local/pipeline/config/conf.properties"
                        change_mode = "restart"
                }


                config {
			load = "flink.tar"
                        image = "abh1sh3k/flink:1.3.3-hadoop27-scala_2.10-alpine"
			network_mode = "host"

                        volumes = [
                                "/${NOMAD_JOB_PATH}/tfp-pipeline:/opt/appnomic/tfp-pipeline",
                                "/${NOMAD_JOB_PATH}/pipeline:/opt/appnomic/appsone/pipeline",
                                "local/tfp-pipeline/config/conf.properties:/opt/appnomic/tfp-pipeline/config/conf.properties",
                                "local/pipeline/config/conf.properties:/opt/appnomic/appsone/pipeline/config/conf.properties",
				"/etc/localtime:/etc/localtime"
                        ]


			args = [
				"taskmanager",
			]

                        port_map {
				taskmanager_rpc_port = "$TM_RPC_PORT"
                                taskmanager_data_port = "$TM_DATAPORT"
				data_port = "56728"
                        }
                }

                resources {
                        cpu = 500
                        memory = 1524
                        network {
                                mbits = "10"
                                port "taskmanager_rpc_port" {
                                        static = "$TM_RPC_PORT"
                                }
				port "taskmanager_data_port" {
                                        static = "$TM_DATAPORT"
                                }
				port "data_port" {
					static = "56728"
				}

                        }
                }
				
		service {
                        name = "flink-taskmanager-1"
                        tags = ["flink-taskmanager-1"]
                        port = "taskmanager_rpc_port"
                }
	}

	task "flink-taskmanager-2" {
                driver = "docker"

                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP_2"
                }

		artifact {
                        source = "https://${getHost}:${RAN_PORT}/flink.tar"
                }

		template {
                       data = <<EOH
				LOG_MODE = "{{key "service/flink/taskmanager/node2/loglevel"}}"
				HADOOP_NAMENODE_HOST = "{{key "service/hadoopnamenode/ip"}}"
				HADOOP_NAMENODE_PORT = "{{key "service/hadoopnamenode/port"}}"
				ZOOKEEPER_HOST = "{{key "service/zookeeper/ip"}}"	
				ZOOKEEPER_PORT = "{{key "service/zookeeper/port"}}"
			EOH
		
			destination = "secrets/file.env"	
			env = true
                }
                template {
                        source = "/${NOMAD_JOB_PATH}/conf-template/tfp-pipeline/conf.properties.tpl"
                        destination = "local/tfp-pipeline/config/conf.properties"
                        change_mode = "restart"
                }
                template {
                        source = "/${NOMAD_JOB_PATH}/conf-template/pipeline/conf.properties.tpl"
                        destination = "local/pipeline/config/conf.properties"
                        change_mode = "restart"
                }


                config {
			load = "flink.tar"
                        image = "abh1sh3k/flink:1.3.3-hadoop27-scala_2.10-alpine"
			network_mode = "host"

                        volumes = [
                                "/${NOMAD_JOB_PATH}/tfp-pipeline:/opt/appnomic/tfp-pipeline",
                                "/${NOMAD_JOB_PATH}/pipeline:/opt/appnomic/appsone/pipeline",
                                "local/tfp-pipeline/config/conf.properties:/opt/appnomic/tfp-pipeline/config/conf.properties",
                                "local/pipeline/config/conf.properties:/opt/appnomic/appsone/pipeline/config/conf.properties",
				"/etc/localtime:/etc/localtime"
                        ]

			args = [
				"taskmanager",
			]

                        port_map {
				taskmanager_rpc_port = "6122"
                                taskmanager_data_port = "6121"
				data_port = "56728"
                        }
                }

                resources {
                        cpu = 500
                        memory = 1524
                        network {
                                mbits = "10"
                                port "taskmanager_rpc_port" {
                                        static = "$TM_RPC_PORT"
                                }
				port "taskmanager_data_port" {
                                        static = "$TM_DATAPORT"
                                }
				port "data_port" {
					static = "56728"
				}
                        }
                }
		service {
                        name = "flink-taskmanager-2"
                        tags = ["flink-taskmanager-2"]
                        port = "taskmanager_rpc_port"
                }
	}		
}

""")

ZOOKEEPER = Template ("""
job "zookeeper" {
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]

	constraint {
		attribute = "${attr.unique.network.ip-address}"
		value = "$IP"
	}

        task "zookeeper" {
        	driver = "docker"

		artifact {
                        source = "https://${getHost}:${RAN_PORT}/zookeeper.tar"
                }
		
                config {
			load = "zookeeper.tar"
			image = "zookeeper"

                        port_map {
                        	zookeeper_client_port = 2181
				zookeeper_follower_port = 2888
				zookeeper_election_port = 3888
                        }
                }

                resources {
                        cpu = 500
                        memory = 1024
                        network {
                                mbits = "10"
                                port "zookeeper_client_port" {
                                	static = "$client_port"
                        	}
                                port "zookeeper_follower_port" {
                                	static = "$follower_port"
                        	}
                                port "zookeeper_election_port" {
                                	static = "$election_port"
                        	}
                	}
       		}

		service {
                        name = "zookeeper"
                        tags = ["zookeeper"]
                        port = "zookeeper_client_port"
                }
	}
}

""")

HADOOP = Template ("""
job "hadoop" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]

	task "hadoop-namenode" {
                driver = "docker"

                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }
	
		artifact {
                        source = "https://${getHost}:${RAN_PORT}/hadoop.tar"
                }

                template {
                       data = <<EOH
                                HADOOP_NAMENODE_ADDRESS = "{{key "service/hadoopnamenode/ip"}}"
                       EOH

                        destination = "secrets/file.env"
                        env = true
                }
	
                config {
			load = "hadoop.tar"
                        image = "abh1sh3k/hadoop:latest"
			network_mode = "host"
			
			volumes = [ 
				"/${NOMAD_JOB_PATH}/data/hadoop:/hadoop",
				"/etc/localtime:/etc/localtime"
			]
			
			args = [
				"hdfs",
				"namenode",
			]
			
                        port_map {
				hadoop_http_port = "50070"
				hadoop_namenode_port = "9000"
				hadoop_data_port = "50010"
                        }
                }

                resources {
                        cpu = 500
                        memory = 1524
                        network {
                                mbits = "10"
                                port "hadoop_http_port" {
                                        static = "$http_port"
                                }
                                port "hadoop_namenode_port" {
                                        static = "$namenode_port"
                                }
                                port "hadoop_data_port" {
                                        static = "50010"
                                }
                        }
                }
				
		service {
                        name = "hadoop-namenode"
                        tags = ["hadoop-namenode"]
                        port = "hadoop_namenode_port"
                }
	}

	task "hadoop-datanode" {
                driver = "docker"

                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP_2"
                }

                template {
                       data = <<EOH
                                HADOOP_NAMENODE_ADDRESS = "{{key "service/hadoopnamenode/ip"}}"
                       EOH

                        destination = "secrets/file.env"
                        env = true
                }

		artifact {
                        source = "https://${getHost}:${RAN_PORT}/hadoop.tar"
                }

                config {
			load = "hadoop.tar"
                        image = "abh1sh3k/hadoop:latest"
			network_mode = "host"
			
			volumes = [ 
				"/${NOMAD_JOB_PATH}/data/hadoop:/hadoop",
				"/etc/localtime:/etc/localtime"
			]

			args = [
				"hdfs",
				"datanode",
			]
                        port_map {
                                hadoop_data_port = "50010"
                                hadoop_http_port = "50075"
                                hadoop_ipc_port = "50020"
                        }

                }

                resources {
                        cpu = 500
                        memory = 1524
                        network {
                                mbits = "10"
                                port "hadoop_data_port" {
                                        static = "50010"
                                }
                                port "hadoop_http_port" {
                                        static = "50075"
                                }
                                port "hadoop_ipc_port" {
                                        static = "50020"
                                }

                        }
                }
				
		service {
                        name = "hadoop-datanode-1"
                        tags = ["hadoop-datanode-1"]
                }
	}

}

""")

CAPACITY_PLAN = Template ("""
job "capacity-planning" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]


	task "capacity-planning" {
                driver = "docker"

                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }

		artifact {
                        source = "https://${getHost}:${RAN_PORT}/capacity-planning.tar"
                }
	
		template {
			data = <<EOH
				CONSUL_URL="{{ key "service/consul/http/url" }}"
			EOH
	
			destination = "secrets/file.env"	
			env = true
		}

		template {
                	source = "/${NOMAD_JOB_PATH}/conf-template/capacity-planning/config.properties.tpl"
                        destination = "local/capacity-planning/Config/config.properties"
			change_mode = "restart"
		}

		template {
			source = "/${NOMAD_JOB_PATH}/conf-template/capacity-planning/ssoCheck.js.tpl"
                        destination = "local/capacity-planning/CP/www/js/ssoCheck.js"
			change_mode = "restart"
		}

		template {
			source = "/${NOMAD_JOB_PATH}/conf-template/capacity-planning/conf.properties.tpl"
                        destination = "local/capacity-planning/DataService/conf/conf.properties"

			change_mode = "restart"
		}
		config {
			load = "capacity-planning.tar"
                        image = "abh1sh3k/shiny-server"
			volumes = [ 
				"/${NOMAD_JOB_PATH}/capacity-planning/CP:/srv/shiny-server/CP",
				"/${NOMAD_JOB_PATH}/capacity-planning/Config:/srv/shiny-server/Config",
				"/${NOMAD_JOB_PATH}/capacity-planning/DataService:/opt/DataService",
				"local/capacity-planning/Config/config.properties:/srv/shiny-server/Config/config.properties",
				"local/capacity-planning/CP/www/js/ssoCheck.js:/srv/shiny-server/CP/www/js/ssoCheck.js",
				"local/capacity-planning/DataService/conf/conf.properties:/opt/DataService/conf/conf.properties",
				"/etc/localtime:/etc/localtime",
				"/etc/hosts:/etc/hosts"
			]

                        port_map {
				"capacity_web_port"= "443"
				"capacity_data_service_port"= "4567"
                        }
                }

                resources {
                        cpu = 500
                        memory = 1024
                        network {
                                mbits = "10"
                                port "capacity_web_port" {
                                       static = "3838"
                                }
                                port "capacity_data_service_port" {
                                       static = "4567"
                                }
			}
                }

		service {
                        name = "capacity-planning"
                        tags = ["capacity-planning"]
                        port = "capacity_web_port"
                }
	}
}

""")

TOMCAT = Template ("""
job "tomcat" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]


	task "tomcat" {
                driver = "docker"

                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }

		artifact {
                        source = "https://${getHost}:${RAN_PORT}/tomcat.tar"
                }

		template {
			data = <<EOH
				TFP_IP="{{ key "service/tfp/ip" }}"
                                SUMODS_IP="{{ key "service/cxo/ip" }}"
                                TOMCAT_IP="{{ key "service/tomcat/ip" }}"
                                TOMCAT_PORT="{{ key "service/tomcat/port" }}"
				KEYCLOAK_ADDRESS="{{ key "service/keycloak/ip" }}"
				KEYCLOAK_PORT="{{ key "service/keycloak/port" }}"
				KEYCLOAK_CLIENT_ID="{{ key "service/keycloak/clientid" }}"
				KEYCLOAK_SECRET="{{ key "service/keycloak/secret" }}"
				MYSQL_ADDRESS="{{ key "service/perconadb/ip" }}"
				MYSQL_PORT="{{ key "service/perconadb/port" }}"
				MYSQL_USER="{{ key "service/perconadb/username" }}"
				MYSQL_PASSWORD="{{ key "service/perconadb/password" }}"
				COLLATING_ADDRESS="{{ key "service/collatingservice/ip" }}"
				ALERTING_ADDRESS="{{ key "service/alertingservice/ip" }}"
				SQL_DS_URL="{{ key "service/sqlds/ip" }}:{{ key "service/sqlds/port" }}"
				SERVICE_PATH="/usr/local/service"
				LICENSE_PATH="/usr/local/tomcat/webapps/"
				SQL_FOLDER_PATH="/usr/local/tomcat/webapps/apmt/WEB-INF/classes/conf/sqls/"
			EOH

			destination = "local/file.env"
			env = true
		}
	
                config {
			load = "tomcat.tar"
                        image = "tomcat-8.5.31:latest"
			network_mode = "host"
			volumes = [ 
				"${BASEDIR}/Appsone_Service/data/tomcat/webapps:/usr/local/tomcat/webapps",
				"${BASEDIR}/Appsone_Service/data/tomcat/server.xml:/usr/local/tomcat/conf/server.xml",
				"${BASEDIR}/Appsone_Service/data/tomcat/web.xml:/usr/local/tomcat/conf/web.xml",
				"${BASEDIR}/Appsone_Service/data/tomcat/nomad:/etc/nomad",
				"${BASEDIR}/Appsone_Service/data/tomcat/bin-nomad:/usr/local/service/bin",
				"${BASEDIR}/Appsone_Service/data/cert/cacerts:/etc/ssl/certs/java/cacerts",
				"${BASEDIR}/nomadFile:/usr/local/nomad_jobs",
				"/etc/localtime:/etc/localtime"
			]

                        port_map {
				"tomcat_https_port"= "9191"
                        }
                }

                resources {
                        cpu = 500
                        memory = 2048
                        network {
                                mbits = "10"
                                port "tomcat_https_port" {
                                        static = "$TOMCAT_PORT"
                                }
			}
                }
				
		service {
                        name = "tomcat"
                        tags = ["tomcat"]
                        port = "tomcat_https_port"
                }
	}
}

""")

KEYCLOAK = Template ("""
job "keycloak" {
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]

	constraint {
		attribute = "${attr.unique.network.ip-address}"
		value = "$IP"
	}

        task "keycloak" {
        	driver = "docker"

		artifact {
        		source = "https://${getHost}:${RAN_PORT}/keycloak-appsone.tar"
	        }

		template {
			data = <<EOH
				KEYCLOAK_IP="{{ key "service/keycloak/ip" }}"
				KEYCLOAK_USER="{{ key "service/keycloak/user" }}"
				KEYCLOAK_PASSWORD="{{key "service/keycloak/password"}}"
				KEYCLOAK_LOGLEVEL="{{ key "service/keycloak/loglevel" }}"
				CONSUL_URL="{{ key "service/consul/url" }}"
			EOH

			destination = "local/file.env"
			env = true
		}

                config {
			load = "keycloak-appsone.tar"
                        image = "keycloak-appsone:3.4.3.Final"
			volumes = [
				"/${NOMAD_JOB_PATH}/data/keycloak:/opt/jboss/keycloak/standalone/data:rw",
				"/etc/localtime:/etc/localtime",
				"/${NOMAD_JOB_PATH}/data/cert/server.jks:/opt/jboss/keycloak/standalone/configuration/server.jks",
				"/etc/hosts:/etc/hosts"
			]
                        port_map {
                        	"keycloak_https_port" = 8443
                        }
                }

                resources {
                        cpu = 500
                        memory = 1024 
                        network {
                                mbits = "10"
                                port "keycloak_https_port" {
                                	static = "$port"
                        	}
                	}
       		}
	}
}
""")

KEYCLOAK_AD = Template ("""
job "keycloak" {
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]

	constraint {
		attribute = "${attr.unique.network.ip-address}"
		value = "$IP"
	}

        task "keycloak" {
        	driver = "docker"

		artifact {
        		source = "https://${getHost}:${RAN_PORT}/keycloak-appsone.tar"
	        }

		template {
			data = <<EOH
				KEYCLOAK_USER="{{ key "service/keycloak/user" }}"
				KEYCLOAK_LOGLEVEL="{{ key "service/keycloak/loglevel" }}"
				KEYCLOAK_PASSWORD="{{key "service/keycloak/password"}}"
				CONSUL_URL="{{ key "service/consul/url" }}"
				KEYCLOAK_IP="{{ key "service/keycloak/ip" }}"
				TRUSTSTORE_FILE_PATH="{{ key "service/keycloak/truststore/file_path" }}"
				TRUSTSTORE_PASSWORD="{{ key "service/keycloak/truststore/password" }}"
			EOH

			destination = "local/file.env"
			env = true
		}

                config {
			load = "keycloak-appsone.tar"
                        image = "keycloak-appsone:3.4.3.Final"
			volumes = [
				"/${NOMAD_JOB_PATH}/data/keycloak:/opt/jboss/keycloak/standalone/data:rw",
				"/${NOMAD_JOB_PATH}/data/cert:/opt/jboss/cert:rw",
				"/${NOMAD_JOB_PATH}/data/cert/server.jks:/opt/jboss/keycloak/standalone/configuration/server.jks",
				"/etc/localtime:/etc/localtime",
				"/etc/hosts:/etc/hosts"
			]
                        port_map {
                        	"keycloak_https_port" = 8443
                        }
                }

                resources {
                        cpu = 500
                        memory = 1024 
                        network {
                                mbits = "10"
                                port "keycloak_https_port" {
                                	static = "$port"
                        	}
                	}
       		}
	}
}

""")


BATCHJOBMONITORING = Template ("""
job "batchjob" {
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]

	constraint {
		attribute = "${attr.unique.network.ip-address}"
		value = "$IP"
	}

        task "batchjob" {
        	driver = "docker"

		artifact {
        		source = "https://${getHost}:${RAN_PORT}/batchjob-monitoring.tar"
	        }

		template {
			data = <<EOH
				MYSQL_IP="{{ key "service/perconadb/ip" }}"
				MYSQL_PORT="{{ key "service/perconadb/port" }}"
				MYSQL_USER="{{ key "service/perconadb/username" }}"
				MYSQL_PASSWORD="{{ key "service/perconadb/password" }}"
			EOH

			destination = "local/file.env"
			env = true
		}

                config {
			load = "batchjob-monitoring.tar"
                        image = "batchjob/latest"
			volumes = [
				"/${NOMAD_JOB_PATH}/data/DataCollectionStatus:/var/www/cgi-bin/DataCollectionStatus",
				"/etc/localtime:/etc/localtime",
				"/etc/hosts:/etc/hosts"
			]
                        port_map {
                        	"batchjob_https_port" = 443
                        }
                }

                resources {
                        cpu = 500
                        memory = 1024 
                        network {
                                mbits = "10"
                                port "batchjob_https_port" {
                                	static = "$BATCH_JOB_PORT"
                        	}
                	}
       		}
	}
}
""")
