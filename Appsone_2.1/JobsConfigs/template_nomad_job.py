#!/usr/bin/env python

from string import Template

TXNCOLLECTOR = Template ("""
job "$JOB_NAME" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]

	task "$JOB_NAME" {
		constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }
		resources {
        		cpu    = 500
	        	memory = 2048
		}
		driver = "exec"
		user = "$USER"
			
		config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/transaction-collector && /usr/lib/jvm/zulu-8/bin/java -XX:ReservedCodeCacheSize=128m -XX:+UseConcMarkSweepGC -Xms1024m -Xmx4096m -XX:+HeapDumpOnOutOfMemoryError -jar transaction-collector-${tr_version}.jar"]
		}

		service {
			name = "transactioncollector"
			tags = ["transactioncollector"]
		}
	
		template {
			data = <<EOH
			tr_version = {{ key "version/transaction-collector" }}
			EOH
                        destination = "secrets/file.env"
                        env         = true
	
		}
		
		template {
			source = "/$NOMAD_JOB_PATH/conf-template/transaction-collector/${JOB_NAME}.config.tpl"
			destination = "$NOMAD_JOB_PATH/transaction-collector/conf/config.properties"

			change_mode = "restart"
		}
	}
}
""")

DATACOLLECTOR_WAS = Template ("""
job "$JOB_NAME" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]
		
	task "$JOB_NAME" {
		constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }
		resources {
        		cpu    = 500
	        	memory = 2048
		}
		driver = "exec"
		user = "$USER"
			
		config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/data-collector-core && ${ibm_jre_path} -Duser.timezone=`/bin/date +%Z` -Xms256m -Xmx2048m -XX:+HeapDumpOnOutOfMemoryError -classpath /$NOMAD_JOB_PATH/data-collector-core/data-collector-core-${dc_version}.jar com.appnomic.appsone.datacollector.core.DataCollectorMain"]

		}

		 service {
                               name = "data-collector-core"
                               tags = [ "data-collector-core" ]
                }
                template {
                        data = <<EOH
                        dc_version = {{ key "version/data-collector-core" }}
			ibm_jre_path = {{ key "service/data-collector-core/ibm-gre-path" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }

                template {
                        source = "/${NOMAD_JOB_PATH}/conf-template/data-collector-core/${JOB_NAME}.config.tpl"
                        destination = "$NOMAD_JOB_PATH/data-collector-core/conf/agent_config.properties"
                        change_mode = "restart"
                }
                template {
			source = "/${NOMAD_JOB_PATH}/conf-template/data-collector-core/collectors_config.properties.tpl"
			destination = "${NOMAD_JOB_PATH}/data-collector-core/conf/collectors_config.properties"
                        change_mode = "restart"
                }
		}
}
""")

DATACOLLECTOR = Template ("""
job "${JOB_NAME}" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]

	task "${JOB_NAME}" {
		constraint {
               	        attribute = "${attr.unique.network.ip-address}"
       		        value = "$IP"
       		 }
		resources {
        		cpu    = 500
	        	memory = 2048
		}
		driver = "exec"
		user = "$USER"
		
		config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/data-collector-core && /usr/lib/jvm/zulu-8/bin/java -Duser.timezone=`/bin/date +%Z` -Xms256m -Xmx2048m -XX:+HeapDumpOnOutOfMemoryError -classpath /$NOMAD_JOB_PATH/data-collector-core/data-collector-core-${dc_version}.jar com.appnomic.appsone.datacollector.core.DataCollectorMain"]

		}

		service {
			name = "data-collector-core"
			tags = ["data-collector-core"]
		}
                template {
                        data = <<EOH
                        dc_version = {{ key "version/data-collector-core" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }
			
		template {
			source = "/${NOMAD_JOB_PATH}/conf-template/data-collector-core/${JOB_NAME}.config.tpl"
			destination = "$NOMAD_JOB_PATH/data-collector-core/conf/agent_config.properties"
			change_mode = "restart"
		}
	}
}
""")


ALERTINGSERVICE = Template ("""
job "$JOB_NAME" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]
        constraint {
        	attribute = "${attr.unique.network.ip-address}"
                value = "$IP"
        }

	task "$JOB_NAME" {
		driver = "exec"
		user = "$USER"
		resources {
        		cpu    = 500
	        	memory = 2048
		}
			
		config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/alerting-service && /usr/lib/jvm/zulu-8/bin/java -Xms256m -Xmx4096m -XX:+HeapDumpOnOutOfMemoryError -jar alerting-service-${as_version}.jar -Dhazelcast.config=./conf/hazelcast.xml"]

		}

		service {
			name = "alertingservice"
			tags = ["alertingservice"]
		}
                template {
                        data = <<EOH
                        as_version = {{ key "version/alerting-service" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }
	
		template {
			source = "/$NOMAD_JOB_PATH/conf-template/alerting-service/conf.json.tpl"
			destination = "$NOMAD_JOB_PATH/alerting-service/conf/conf.json"
			change_mode = "restart"
		}
		
		template {
			source = "/$NOMAD_JOB_PATH/conf-template/alerting-service/dbconfig.properties.tpl"
			destination = "$NOMAD_JOB_PATH/alerting-service/conf/dbconfig.properties"
			change_mode = "restart"
		}

		template {
			source = "/$NOMAD_JOB_PATH/conf-template/alerting-service/hazelcast.xml.tpl"
			destination = "$NOMAD_JOB_PATH/alerting-service/conf/hazelcast.xml"
			change_mode = "restart"
		}
	}
}
""")

COMPONENTAGENT = Template ("""
job "$JOB_NAME" {
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]
        constraint {
        	attribute = "${attr.unique.network.ip-address}"
                value = "$IP"
        }

        task "$JOB_NAME" {
		resources {
        		cpu    = 500
	        	memory = 2048
		}
                driver = "exec"
                user = "$USER"

                config {
			command = "/bin/bash"
                        args = ["-c","cd /$NOMAD_JOB_PATH/component-agent-core && /usr/lib/jvm/zulu-8/bin/java -XX:+UseConcMarkSweepGC -jar component-agent-core-${ca_version}.jar >> /$NOMAD_JOB_PATH/component-agent-core/log/component-agent-core.log "]	
                }
		
		service {
			name = "component-agent"
			tags = ["component-agent"]
		}
                template {
                        data = <<EOH
                        ca_version = {{ key "version/component-agent-core" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true 
                }

		template {
			source = "/$NOMAD_JOB_PATH/conf-template/component-agent-core/basic.properties.tpl"
                        destination = "$NOMAD_JOB_PATH/component-agent-core/config/basic.properties"
			change_mode = "restart"
		}
       }
}

""")

ETLLTM = Template ("""
job "$JOB_NAME" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]
        constraint {
        	attribute = "${attr.unique.network.ip-address}"
                value = "$IP"
        }

	task "$JOB_NAME" {
		driver = "exec"
		user = "$USER"
		resources {
        		cpu    = 500
	        	memory = 2048
		}
		
		config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/log-txn-monitoring/bin && /usr/lib/jvm/zulu-8/bin/java -Dhttp.maxConnections=20 -Dhttp.keepAlive=true -jar /$NOMAD_JOB_PATH/log-txn-monitoring/lib/log-txn-monitoring-${ltm_version}.jar"]
		}
		
		service {
			name = "log-txn-monitoring"
			tags = ["log-txn-monitoring","ltm","etl-ltm"]
		}
                template {
                        data = <<EOH
                        ltm_version = {{ key "version/log-txn-monitoring" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }

		template {
			source = "/$NOMAD_JOB_PATH/conf-template/log-txn-monitoring/${JOB_NAME}.config.tpl"
			destination = "$NOMAD_JOB_PATH/log-txn-monitoring/conf/conf.properties"
		}
	}
}
""")

INCIDENTMGMTBROKER = Template ("""
job "$JOB_NAME" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]
        constraint {
        	attribute = "${attr.unique.network.ip-address}"
        	value = "$IP"
        }

	task "$JOB_NAME" {
		resources {
        		cpu    = 500
	        	memory = 2048
		}
		driver = "exec"
		user = "$USER"
		
		config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/incident-management-broker/bin && /usr/lib/jvm/zulu-8/bin/java -Dhttp.maxConnections=20 -Dhttp.keepAlive=true -jar /$NOMAD_JOB_PATH/incident-management-broker/lib/incident-management-broker-${im_version}.jar"]

		}
                service {
                        name = "incident-management-broker"
                        tags = ["incident-management-broker"]
                }
                template {
                        data = <<EOH
                        im_version = {{ key "version/incident-management-broker" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }

                template {
                        source = "/$NOMAD_JOB_PATH/conf-template/incident-management-broker/conf.properties.tpl"
                        destination = "$NOMAD_JOB_PATH/incident-management-broker/conf/conf.properties"
                        change_mode = "restart"
                }
	}
}
""")

PATTERNRT = Template ("""
job "$JOB_NAME" {
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]
        constraint {
        	attribute = "${attr.unique.network.ip-address}"
                value = "$IP"
        }

        task "$JOB_NAME" {
		resources {
        		cpu    = 500
	        	memory = 2048
		}
                driver = "exec"
                user = "$USER"

                config {
                	command = "/bin/bash"
                        args = ["-c","cd /$NOMAD_JOB_PATH/analytics/patternrt && /usr/lib/jvm/zulu-8/bin/java -Xmn100M -Xms500M -Xms500M -Xmx1000M -classpath '/$NOMAD_JOB_PATH/analytics/patternrt/analytics-${pt_version}.jar:/$NOMAD_JOB_PATH/analytics/patternrt' com.appnomic.appsone.abl.rt.process.NewRealTimeInitializer"]
                }

		service {
			name = "patternRT"
			tags = ["RT", "patternRT", "analytics"]
		}
                template {
                        data = <<EOH
                        pt_version = {{ key "version/patternrt" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }

		template {
			source = "/$NOMAD_JOB_PATH/conf-template/analytics/conf.properties.tpl"
			destination = "$NOMAD_JOB_PATH/analytics/patternrt/conf/conf.properties"
			change_mode = "restart"				
		}

		template {
			source = "/$NOMAD_JOB_PATH/conf-template/analytics/jdbc.properties.tpl"
			destination = "$NOMAD_JOB_PATH/analytics/patternrt/conf/jdbc.properties"
			change_mode = "restart"				
		}

		template {
			source = "/$NOMAD_JOB_PATH/conf-template/analytics/hazelcast.xml.tpl"
			destination = "$NOMAD_JOB_PATH/analytics/patternrt/conf/hazelcast.xml"
			change_mode = "restart"				
		}
	}
}
""")

ETLTFP = Template("""
job "${JOB_NAME}" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]

	task "${JOB_NAME}" {
		resources {
        		cpu    = 500
	        	memory = 2048
		}
          	constraint {
          	    attribute = "${attr.unique.network.ip-address}"
          	    value = "$IP"
          	}

		driver = "exec"
		user = "$USER"
		
		config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/tfp-etl/bin && /usr/lib/jvm/zulu-8/bin/java -Xms512m -Xmx1024m -cp '.:/$NOMAD_JOB_PATH/tfp-etl/lib/*' com.appnomic.etl.ETLMain"]
		}

		service {
			name = "tfp-etl"
			tags = ["tfp-etl"]
		}
                template {
                        data = <<EOH
                        tfp_version = {{ key "version/tfp-etl" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }

		template {
			source = "$NOMAD_JOB_PATH/conf-template/tfp-etl/${JOB_NAME}.config.tpl"
			destination = "$NOMAD_JOB_PATH/tfp-etl/config/conf.properties"
			change_mode = "restart"
		}
	}
}
""")

JMXCOLLECTOR_JBOSS = Template ("""
job "${JOB_NAME}" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]

	task "${JOB_NAME}" {
		constraint {
			attribute = "${attr.unique.network.ip-address}"
			value = "$IP"
		}
		resources {
        		cpu    = 500
	        	memory = 2048
		}
		driver = "exec"
		user = "$USER"
		
		config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/jmx-collector/bin && /usr/lib/jvm/zulu-8/bin/java  -classpath '/$NOMAD_JOB_PATH/jmx-collector/lib/*' com.appnomic.appsone.jmx.JMXCollectorMain" ]
		}
                template {
                        data = <<EOH
                        jmx_version = {{ key "version/jmx-collector" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }

		service {
			name = "jmx-collector-jboss"
			tags = ["jmx-collector-jboss"]
		}

		template {
			source = "/$NOMAD_JOB_PATH/conf-template/jmx-collector/conf_jboss.properties.tpl"
			destination = "$NOMAD_JOB_PATH/jmx-collector/config/conf.properties"
			change_mode = "restart"

		}
	}
}
""")

JMXCOLLECTOR = Template ("""
job "${JOB_NAME}" {
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]

        task "${JOB_NAME}" {
                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }
		resources {
        		cpu    = 500
	        	memory = 2048
		}
                driver = "exec"
                user = "$USER"
                
                config {
                        command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/jmx-collector/bin && /usr/lib/jvm/zulu-8/bin/java  -classpath '/$NOMAD_JOB_PATH/jmx-collector/lib/*' com.appnomic.appsone.jmx.JMXCollectorMain" ]
                }

                service {
                        name = "jmx-collector"
                        tags = ["jmx-collector"]
                }
                template {
                        data = <<EOH
                        jmx_version = {{ key "version/jmx-collector" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }

                template {
                        source = "/$NOMAD_JOB_PATH/conf-template/jmx-collector/conf_ds.properties.tpl"
                        destination = "$NOMAD_JOB_PATH/jmx-collector/config/conf.properties"
			change_mode = "restart"
                }
        }
}
""")

JMXCOLLECTOR_WAS = Template ("""
job "${JOB_NAME}" {
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]

        task "${JOB_NAME}" {
                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }
		resources {
        		cpu    = 500
	        	memory = 2048
		}
                driver = "exec"
                user = "$USER"
                
                config {
                        command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/jmx-collector/bin && ${ibm_jre_path} -classpath '/$NOMAD_JOB_PATH/jmx-collector/lib/*' com.appnomic.appsone.jmx.JMXCollectorMain" ]
                }

                service {
                        name = "jmx-collector-was"
                        tags = ["jmx-collector-was"]
                }
                template {
                        data = <<EOH
                        jmx_version = {{ key "version/jmx-collector" }}
			ibm_jre_path = {{ key "service/jmx-collector-was/ibm-gre-path" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }

                template {
                        source = "/$NOMAD_JOB_PATH/conf-template/jmx-collector/conf_was.properties.tpl"
                        destination = "$NOMAD_JOB_PATH/jmx-collector/config/conf.properties"
			change_mode = "restart"
                }
        }
}
""")


COLLATINGSERVICE = Template ("""
job "${JOB_NAME}" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]

	task "${JOB_NAME}" {
	        constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }
		resources {
        		cpu    = 500
	        	memory = 2048
		}
		driver = "exec"
		user = "$USER"
		
		config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/collating-service && /usr/lib/jvm/zulu-8/bin/java -Xms256m -Xmx2048m -XX:+HeapDumpOnOutOfMemoryError -jar collating-service-${cs_version}.jar conf/dbconfig -Dhazelcast.config=/$NOMAD_JOB_PATH/collating-service/conf/cache/hazelcast.xml"]
		}
		
		service {
			name = "collatingservice"
			tags = ["collatingservice"]
		}
                template {
                        data = <<EOH
                        cs_version = {{ key "version/collating-service" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
        
                }

		template {
			source = "/$NOMAD_JOB_PATH/conf-template/collating-service/dbconn.xml.tpl"
			destination = "$NOMAD_JOB_PATH/collating-service/conf/dbconfig/dbconn.xml"
			change_mode = "restart"
		}

		template {
			source = "/$NOMAD_JOB_PATH/conf-template/collating-service/hazelcast.xml.tpl"
			destination = "$NOMAD_JOB_PATH/collating-service/conf/cache/hazelcast.xml"
			change_mode = "restart"
		}
	}
}
	
""")

CXO = Template ("""
job "${JOB_NAME}" {
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]

	task "${JOB_NAME}_sqlds" {
                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }
		resources {
        		cpu    = 500
	        	memory = 2048
		}

		driver = "exec"
		user = "$USER"

		config {
			command = "/bin/bash"
			args = ["-c","cd /${NOMAD_JOB_PATH}/sql-ds/bin && /usr/lib/jvm/zulu-8/bin/java -Dconfig=../conf/config.edn -Dlogback.configurationFile=../conf/logback.xml -cp .:'../conf/':'../lib/*' sql_ds.core"]
		}

		service {
			name = "sql-ds"
			tags = ["sql-ds"]
		}

		template {
			source = "/${NOMAD_JOB_PATH}/conf-template/sql-ds/config.edn.tpl"
			destination = "${NOMAD_JOB_PATH}/sql-ds/conf/config.edn"
			change_mode = "restart"
		}
	}

	task "${JOB_NAME}_sumods" {
                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }
		resources {
        		cpu    = 500
	        	memory = 2048
		}

		driver = "exec"
		user = "$USER"
	
		config {
                        command = "/bin/bash"
			args = ["-c","cd /${NOMAD_JOB_PATH}/sumo-dataservice/bin && /usr/lib/jvm/zulu-8/bin/java -Dratpack.development=false -Dratpack.port=9193 -jar /${NOMAD_JOB_PATH}/sumo-dataservice/lib/sumo-dataservice-${sumo_version}.jar"]
                }       

                template {
                        data = <<EOH
                        sumo_version = {{ key "version/sumo-dataservice" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }

                service {
                        name = "sumo-ds"
                        tags = ["sumo-ds"]
                }       
                        
                template {
                        source = "/${NOMAD_JOB_PATH}/conf-template/sumo-dataservice/conf.properties.tpl"
                        destination = "${NOMAD_JOB_PATH}/sumo-dataservice/conf/conf.properties"
			change_mode = "restart"
                }
	}
}

""")

FORENSICSERVICE = Template ("""
job "${JOB_NAME}" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]

	task "${JOB_NAME}" {
                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }
		resources {
        		cpu    = 500
	        	memory = 2048
		}

		driver = "exec"
		user = "$USER"
				
		config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/forensic-service/bin && /usr/lib/jvm/zulu-8/bin/java -classpath '.:/$NOMAD_JOB_PATH/forensic-service/lib/*' com.appnomic.appsone.forensics.ForensicsMain"]
		}

		service {
			name = "forensicservice"
			tags = ["forensicservice"]
		}
	
		template {
			source = "/$NOMAD_JOB_PATH/conf-template/forensic-service/conf.properties.tpl"
			destination = "$NOMAD_JOB_PATH/forensic-service/config/conf.properties"
			change_mode = "restart"
		}
	}
}
""")

JPPFKPICOLLECTOR = Template ("""
job "${JOB_NAME}" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]

	task "${JOB_NAME}" {
                constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }
		resources {
        		cpu    = 500
	        	memory = 2048
		}

		driver = "exec"
		user = "$USER"
		
		config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/jppf-kpi-collector/bin && /usr/lib/jvm/zulu-8/bin/java -jar /$NOMAD_JOB_PATH/jppf-kpi-collector/lib/jppf-kpi-collector-${jk_version}.jar"]

		}

		service {
			name = "JPPFKpiCollector"
			tags = ["JPPFKpiCollector"]
		}
                template {
                        data = <<EOH
                        jk_version = {{ key "version/jppf-kpi-collector" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }

		template {
			source = "/$NOMAD_JOB_PATH/conf-template/jppf-kpi-collector/conf.properties.tpl"
			destination = "$NOMAD_JOB_PATH/jppf-kpi-collector/conf/conf.properties"
			change_mode = "restart"
		}
	}
}

""")

COLLECTIONSERVICE = Template ("""
job "${JOB_NAME}" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]

	task "${JOB_NAME}" {
	        constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }
		resources {
        		cpu    = 500
	        	memory = 2048
		}

		driver = "exec"
		user = "$USER"
		
		config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/collection-service && /usr/lib/jvm/zulu-8/bin/java -XX:+HeapDumpOnOutOfMemoryError -XX:+UseConcMarkSweepGC -Xms256m -Xmx4096m -XX:+HeapDumpOnOutOfMemoryError -jar collection-service-${cs_version}.jar"]	

		}
		
		service {
                        name = "collectionservice"
                        tags = ["collectionservice"]
                }
                template {
                        data = <<EOH
                        cs_version = {{ key "version/collection-service" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
        
                }

                template {
                	source = "/$NOMAD_JOB_PATH/conf-template/collection-service/${JOB_NAME}.config.tpl"
                        destination = "$NOMAD_JOB_PATH/collection-service/conf/config.properties"
                        change_mode = "restart"
                }

                template {
                         source = "/$NOMAD_JOB_PATH/conf-template/collection-service/dbconn.xml.tpl"
                         destination = "$NOMAD_JOB_PATH/collection-service/conf/dbconfig/dbconn.xml"
                         change_mode = "restart"
                }
	}
}

""")

GRPC = Template ("""
job "${JOB_NAME}" {
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]

        task "${JOB_NAME}" {
	        constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }
		resources {
        		cpu    = 500
	        	memory = 2048
		}

                driver = "exec"
                user = "$USER"

                config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/grpc-server && /usr/lib/jvm/zulu-8/bin/java -XX:+UseConcMarkSweepGC -Xms128m -Xmx256m -jar grpc-server-${grpc_version}.jar"]
                }
	
		service {
			name = "grpc"
			tags = ["grpc-server", "grpc"]
		}
                template {
                        data = <<EOH
                        grpc_version = {{ key "version/grpc-service" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }
	
		template {
			source = "/$NOMAD_JOB_PATH/conf-template/grpc/conf.properties.tpl"
			destination = "$NOMAD_JOB_PATH/grpc-server/config/conf.properties"
			change_mode = "restart"
		}
        }
}

""")

NOTIFICATIONSERVICE = Template ("""
job "${JOB_NAME}" { 
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]

        task "${JOB_NAME}" {
	       constraint {
	 	      attribute = "${attr.unique.network.ip-address}"
        	      value = "$IP"
                }
		resources {
        		cpu    = 500
	        	memory = 2048
		}

                driver = "exec"
                user = "$USER"

                config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/notification-service && /usr/lib/jvm/zulu-8/bin/java -XX:+UseConcMarkSweepGC -Xms128m -Xmx256m -jar notification-service-${ns_version}.jar"]
                }
			
		service {
			name = "notification-service"
			tags = ["notification-service"]
		}
                template {
                        data = <<EOH
                        ns_version = {{ key "version/notification-service" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }
	
		template {
			source = "/$NOMAD_JOB_PATH/conf-template/notification-service/conf.properties.tpl"
			destination = "$NOMAD_JOB_PATH/notification-service/config/conf.properties"
			change_mode = "restart"
		}
       }
}

""")

REPORTINGSERVICE = Template ("""
job "${JOB_NAME}" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]

	task "${JOB_NAME}" {
	        constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }
		resources {
        		cpu    = 500
	        	memory = 2048
		}

		driver = "exec"
		user = "$USER"
			
		config {
			command = "/bin/bash"
			args = ["-c","cd /$NOMAD_JOB_PATH/reporting-service && /usr/lib/jvm/zulu-8/bin/java -Xms256m -Xmx2048m -XX:+HeapDumpOnOutOfMemoryError -jar reporting-service-${rs_version}.jar ./conf/ScheduleReporting.xml"]
		}

		service {
			name = "reportingservice"
			tags = ["reportingservice"]
		}
                template {
                        data = <<EOH
                        rs_version = {{ key "version/reporting-service" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }

		template {
			source = "/$NOMAD_JOB_PATH/conf-template/reporting-service/ScheduleReporting.xml.tpl"
			destination = "$NOMAD_JOB_PATH/reporting-service/conf/ScheduleReporting.xml"
			change_mode = "restart"
		}

	}
}

""")

TFPDS = Template ("""
job "$JOB_NAME" {
	region = "$REGION"
	datacenters = ["$DATA_CENTER"]

	task "$JOB_NAME" {
		driver = "exec"
		user = "$USER"
		resources {
        		cpu    = 500
	        	memory = 2048
		}

		constraint {
			attribute = "${attr.unique.network.ip-address}"
			value = "$IP"
		}
				
		config {
			command = "/bin/bash"
			args = ["-c","cd /${NOMAD_JOB_PATH}/tfp-ds/bin && /usr/lib/jvm/zulu-8/bin/java -Xms512m -Xmx1024m -Dratpack.development=false -Dratpack.port=8995 -jar /${NOMAD_JOB_PATH}/tfp-ds/lib/tfp-ds-${tfp_version}.jar"]
		}

		service {
			name = "tfp-ds"
			tags = ["tfp-ds"]
		}
	
                template {
                        data = <<EOH
                        tfp_version = {{ key "version/tfp-ds" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
        
                }

		template {
			source = "/${NOMAD_JOB_PATH}/conf-template/tfp-ds/conf.properties.tpl"
			destination = "${NOMAD_JOB_PATH}/tfp-ds/conf/conf.properties"
			change_mode = "restart"
		}
		template {
			source = "/${NOMAD_JOB_PATH}/conf-template/tfp-ds/dashboard-details.json.tpl"
			destination = "$NOMAD_JOB_PATH/tfp-ds/conf/dashboard-details.json"
			change_mode = "restart"
		}
	}
}
""")

ETLSTITCHING = Template ("""
job "${JOB_NAME}" {
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]
        constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }

        task "${JOB_NAME}" {
		resources {
        		cpu    = 500
	        	memory = 2048
		}
                driver = "exec"
                user = "$USER"

                config {
                        command = "/bin/bash"
                        args = ["-c","cd /${NOMAD_JOB_PATH}/stitching-etl/bin && /usr/lib/jvm/zulu-8/bin/java -jar /${NOMAD_JOB_PATH}/stitching-etl/lib/stitching-etl-${stitching_vr}.jar"]
                }

                service {
                        name = "stitching-etl"
                        tags = ["stitching-etl"]
                }
                template {
                        data = <<EOH
                        stitching_vr = {{ key "version/stitching-etl" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
        
                }

                template {
                        source = "/${NOMAD_JOB_PATH}/conf-template/stitching-etl/${JOB_NAME}.config.tpl"
                        destination = "$NOMAD_JOB_PATH/stitching-etl/conf/conf.properties"
                }
        }
}
""")
SABOO = Template ("""
job "saboo" {
        region = "$REGION"
        datacenters = ["$DATA_CENTER"]
        constraint {
                        attribute = "${attr.unique.network.ip-address}"
                        value = "$IP"
                }

        task "saboo" {
		resources {
        		cpu    = 500
	        	memory = 2048
		}
                driver = "exec"
                user = "$USER"

                config {
                       command = "/bin/bash"
                       args = ["-c","cd /$NOMAD_JOB_PATH/analytics/saboo && /usr/lib/jvm/zulu-8/bin/java -Xmx1024M -Xms512M -classpath '.:/$NOMAD_JOB_PATH/analytics/saboo/analytics-${pt_version}.jar' com.appnomic.appsone.abl.main.RunHA"]
                 }

		service {
			name = "saboo"
			tags = ["RT", "saboo", "analytics"]
		}
                template {
                        data = <<EOH
                        pt_version = {{ key "version/patternrt" }}
                        EOH
                        destination = "secrets/file.env"
                        env         = true
                }

		template {
			source = "/$NOMAD_JOB_PATH/conf-template/analytics/conf.properties.tpl"
			destination = "$NOMAD_JOB_PATH/analytics/saboo/conf/conf.properties"
			change_mode = "restart"				
		}

		template {
			source = "/$NOMAD_JOB_PATH/conf-template/analytics/jdbc.properties.tpl"
			destination = "$NOMAD_JOB_PATH/Appsone_Service/analytics/saboo/conf/jdbc.properties"
			change_mode = "restart"				
		}

		template {
			source = "/$NOMAD_JOB_PATH/conf-template/analytics/hazelcast.xml.tpl"
			destination = "$NOMAD_JOB_PATH/Appsone_Service/analytics/saboo/conf/hazelcast.xml"
			change_mode = "restart"				
		}
       }
}

""")
