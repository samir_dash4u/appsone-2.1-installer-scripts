#!/bin/bash

BASEDIR=$(pwd)
mkdir -p "${BASEDIR}/logs"
LOG_FILE="${BASEDIR}/logs/${0}.log"

if [ $# -eq 2 ]; then
	INSTALLDIR="$1"
        REMOTE_NODES="$2"
        nomadPort=4646
elif [ $# -eq 3 ]; then
	INSTALLDIR="$1"
	REMOTE_NODES="$2"
	nomadPort=$3
else
	echo "
	Usage : $0 <Installation Dir, Nomad dir, Consul dir etc separated by space> <Remote ips separated by space> <nomad server port/optional>
	Eg: $0 '/opt/appnomic /opt/nomad /opt/consul' '192.168.2.153 192.168.2.212'
	Eg: $0 '/opt/appnomic /opt/nomad /opt/consul' '192.168.2.153 192.168.2.212' 4641"
	exit 0
fi

export NOMAD_CACERT="/etc/nomad/nomad-ca.pem"
export NOMAD_CLIENT_KEY="/etc/nomad/cli-key.pem"
export NOMAD_CLIENT_CERT="/etc/nomad/cli.pem"
export NOMAD_ADDR=https://localhost:$nomadPort

function logit()
{
        datestamp=$(date +"%d-%m-%Y:%H:%M:%S");
        echo "$datestamp : $* " >> $LOG_FILE
        return 0;
}

function stop_service() {
	echo "Stopping docker,nomad, consul, ran services on local node"
	logit "Stopping docker,nomad, consul, ran services on local node"
	for item in `echo "docker nomad consul ran"`
	do
		echo "Stopping $item" 
		logit "Stopping $item" 
		systemctl stop $item 2>/dev/null
	done
	for ip in `echo $REMOTE_NODES`
	do
		echo "Stopping docker,nomad, consul services on $ip"
		logit "Stopping docker,nomad, consul services on $ip"
		ssh -o ConnectTimeout=200 $ip 'for item in `echo "docker nomad consul ran"`; do echo "Stopping $item"; systemctl stop $item 2>/dev/null; done'
	done
	logit "Docker, nomad, consul etc stopped"
}

function uninstall_rpms() {
	echo "Uninstalling docker,nomad, consul, ran, java on local node"
	logit "Uninstalling docker,nomad, consul, ran, java on local node"
	for rpm in `rpm -qa | grep "zulu-8\|consul\|docker\|nomad\|ran\|MySQL-client\|mysql-connector-python" | sort`
	do
		echo "Removing $rpm"
		logit "Removing $rpm"
		rpm -ev $rpm  2>/dev/null
	done
	for ip in `echo $REMOTE_NODES`
	do
		echo "Uninstalling docker, nomad, consul, java on $ip"
		logit "Uninstalling docker, nomad, consul, java on $ip"
		ssh -o ConnectTimeout=200 $ip 'for rpm in `rpm -qa | grep "zulu-8\|consul\|docker\|nomad" | sort`; do echo "Removing $rpm"; rpm -ev $rpm 2>/dev/null; done'
	done
	logit "Docker, nomad, consul, java uninstalled"
}

function remove_leftovers() {
	echo "Removing nomad, docker, consul, ran config files on local node"
	logit "Removing nomad, docker, consul, ran config files on local node"
	rm -rf /etc/ran.d
	rm -rf /etc/nomad
	rm -rf /etc/consul
	rm -f /var/log/nomad*
	rm -f /var/log/consul*
	rm -rf /var/lib/docker
	for ip in `echo $REMOTE_NODES`
	do
		echo "Removing nomad, docker, consul config files on $ip"
		logit "Removing nomad, docker, consul config files on $ip"
		ssh -o ConnectTimeout=200 $ip "rm -rf /etc/nomad; rm -rf /etc/consul; rm -f /var/log/nomad.log; rm -f /var/log/consul.log; rm -rf /var/lib/docker"
	done
	logit "Nomad, docker, consul  config files removed "
}

function remove_appsone() {
	echo "Removing Installation Dir, Nomad dir, Consul dir.. [ ${INSTALLDIR} ]. Please take a backup of any scripts/modules copied manually to this folder."
	logit "Removing Installation Dir, Nomad dir, Consul dir.. [ ${INSTALLDIR} ]. Please take a backup of any scripts/modules copied manually to this folder."
	read -p "Continue.. [ y/n ] : " choicer
	choice=$(echo "$choicer" | tr [[:lower:]] [[:upper:]] )
	if [ "$choice" == "Y" -o "$choice" == "YES" ]; then
		echo "Removing on local node"
		logit "Removing on local node"
		rm -rf $INSTALLDIR
		for ip in `echo $REMOTE_NODES`
		do
			echo "Removing on $ip"
			logit "Removing on $ip"
			ssh -o ConnectTimeout=200 $ip "rm -rf $INSTALLDIR"
		done
	fi
	rm -f ../.installationstatus
	logit "Installation dir, Nomad dir, Consul dir removed"
}

function stop_nomad_service() {
	echo "Stopping all nomad jobs"
	logit "Stopping all nomad jobs"
	nomad status | tail -n +2 | grep "running" | awk '{print $1'} | while read line
	do
		nomad stop $line
	done
	logit "All nomad jobs stopped."
	sleep 5
}

stop_nomad_service
stop_service
uninstall_rpms
remove_leftovers
remove_appsone
echo "Prerequisites steps are not reverted. Please do them manually if required. Follow instructions from README"
logit "Prerequisites steps are not reverted. Please do them manually if required. Follow instructions from README"
exit 0
