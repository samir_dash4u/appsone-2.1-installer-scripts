sed -i 's/SELINUX=.*/SELINUX=disabled/' /etc/selinux/config
setenforce 0 > /dev/null 2>&1
echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_all
grep -q "net.ipv4.icmp_echo_ignore_all" /etc/sysctl.conf
if [ $? -eq 0 ]; then
	sed -i 's/net.ipv4.icmp_echo_ignore_all.*/net.ipv4.icmp_echo_ignore_all = 1/' /etc/sysctl.conf
else
	echo 'net.ipv4.icmp_echo_ignore_all = 1' >> /etc/sysctl.conf
fi
grep -q "fs.file-max" /etc/sysctl.conf
if [ $? -eq 0 ]; then
	sed -i 's/fs.file-max.*/fs.file-max = 500000/' /etc/sysctl.conf
else
	echo "fs.file-max=500000" >> /etc/sysctl.conf
fi 
sysctl -p > /dev/null 2>&1
# Check if firewall is running
systemctl status firewalld > /dev/null 2>&1
retcode=$?
if [ $retcode -ne 0 ]; then
	echo "Firewall is stopped. Starting Firewall"
	systemctl start firewalld > /dev/null 2>&1
	if [ $? -eq 0 ]; then
		echo "Firewall started"
	else
		echo "Firewall could not be started"
		exit 1
	fi
fi
exit 0
