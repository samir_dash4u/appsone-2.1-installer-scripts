#!/bin/bash


if [ $# -eq 2 ]; then
        REMOTE_NODES="$1"
	option_raw="$2"
elif [ $# -eq 3 ]; then
        REMOTE_NODES="$1"
	option_raw="$2"
	FileName="$3"
else
	echo "
	This script will enable/disable list of ports in firewall.
	Usage : $0 <remote ip lists separated by space> <enable/disable/reset> <file containing port list>
	$0 '192.168.2.153 192.168.2.212' enable filename 	# This will enable all the ports mentioned in file name
	$0 '192.168.2.153 192.168.2.212' disable filename 	# This will disaable all the ports mentioned in file name
	$0 '192.168.2.153 192.168.2.212' reset		# This will reset the firewall to original state.
	
	Note: The file should contain one port per line"
	exit 1
fi

function check_start_firewall()
{
	# Check firewall is on or not
	systemctl status firewalld > /dev/null 2>&1
	if [ $? -ne 0 ]; then
		systemctl start firewalld > /dev/null 2>&1
	fi
	for ip in `echo $REMOTE_NODES`
        do
		ssh -o ConnectTimeout=200 $ip 'systemctl status firewalld > /dev/null 2>&1; if [ $? -ne 0 ]; then systemctl start firewalld > /dev/null 2>&1; fi'
	done
}

function main()
{
	option=$(echo "$option_raw" | tr '[:lower:]' '[:upper:]')
	if [ "$option" == "ENABLE" ]; then
		if [ ! -f "$FileName" ]; then
			echo "File $FileName doesn't exist"
			exit 1
		fi
	
		while read line
		do
			firewall-cmd --add-port=$line/tcp --zone=public --permanent --service=A1_Service
		done < $FileName

		for line2 in `cat $FileName`
		do
			for ip in `echo $REMOTE_NODES`
        		do
				ssh -o ConnectTimeout=200 $ip "firewall-cmd --add-port=$line2/tcp --zone=public --permanent --service=A1_Service"
			done
		done
		firewall-cmd --reload
		for ip in `echo $REMOTE_NODES`
                do
			ssh -o ConnectTimeout=200 $ip "firewall-cmd --reload"
		done
	elif [ "$option" == "DISABLE" ]; then
		if [ ! -f "$FileName" ]; then
			echo "File $FileName doesn't exist"
			exit 1
		fi

		while read line
		do
			firewall-cmd --remove-port=$line/tcp --zone=public --permanent --service=A1_Service
		done < $FileName

		for line2 in `cat $FileName`
		do
			for ip in `echo $REMOTE_NODES`
        		do
				ssh -o ConnectTimeout=200 $ip "firewall-cmd --remove-port=$line2/tcp --zone=public --permanent --service=A1_Service"
			done
		done < $FileName
		firewall-cmd --reload
		for ip in `echo $REMOTE_NODES`
                do
			ssh -o ConnectTimeout=200 $ip "firewall-cmd --reload"
		done
	elif [ "$option" == "RESET" ]; then
		firewall-cmd --delete-service=A1_Service --permanent
		firewall-cmd --permanent --load-zone-defaults=public
		firewall-cmd --reload
		for ip in `echo $REMOTE_NODES`
                do
			ssh -o ConnectTimeout=200 $ip "firewall-cmd --delete-service=A1_Service --permanent; firewall-cmd --permanent --load-zone-defaults=public; firewall-cmd --reload"
		done
	else
		echo "Invalid Option"
	fi
}

check_start_firewall
main
