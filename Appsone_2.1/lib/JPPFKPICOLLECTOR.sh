#!/bin/bash
echo "Enter the required input to run JPPF KPI COLLECTOR"
read -p "Enter the component name : " comp_name
read -p "Enter the driver component name : " dcomp_name
read -p "Enter the node component instances : " ncomp_inst
read -p "Enter the driver_component_instances : " dcomp_inst

consul kv put "service/jppfkpicollector/component_name" comp_name
consul kv put "service/jppfkpicollector/driver_component_name" dcomp_name
consul kv put "service/jppfkpicollector/node_component_instances" ncomp_inst
consul kv put "service/jppfkpicollector/driver_component_instances" dcomp_inst

#Start the service
#nomad run JobsConfigs/*_JPPFKPICOLLECTOR_1.nomad

