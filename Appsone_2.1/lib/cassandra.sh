#!/bin/bash
chk_nodes()
{
	Nonodes=$(docker exec ${id} sh -c "nodetool status"  | grep UN | wc -l)
	if [ $Nonodes -ne 3 ]; then
		return 1
	else
		return 0
	fi
}
BASEDIR=$1
if [ -z "$BASEDIR" ]; then
	BASEDIR="/tmp"
fi
id=$(docker ps | grep cassandra | awk '{print $1}')
for i in {1..4};
do
	chk_nodes
	if [ $? -eq 0 ]; then
		break;
	else
		sleep 15
	fi
	if [ $i -eq 4 ]; then
		read -p "cassandra three node cluster has not formed. Do you want to check again. [Y/N]"  Ack
               	if [ "$Ack" = 'Y' ] || [ "$Ack" = 'y' ]; then
			sleep 30
			chk_nodes
			if [ $? -eq 0 ]; then
				break
			else
				echo "Cassandra three node cluster could not be formed. Please check manually and run the steps mentioned in README under section Cassandra database manual steps."
				exit 1
			fi
		else
			exit 1
		fi
	fi
done
	
docker exec ${id} ls /tmp/cassandra.cql > /dev/null 2>&1
res=$?
if [ $res -ne 0 ]; then
	docker cp ${BASEDIR}/cassandra.cql ${id}:/tmp/
	docker exec ${id} sh -c "cqlsh < /tmp/cassandra.cql"                                                
fi
