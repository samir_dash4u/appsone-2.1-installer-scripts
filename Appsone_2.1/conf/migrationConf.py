db_data_file="/home/appuser/percona.tar.gz"
### Below variable can take HAPROXY/HTTPD 
load_bal_type="HAPROXY"
conf_file="/tmp/oldhaproxy.cfg"

UID = {
	'server-03' : { 'DATACOLLECTOR' : ("1ce1-2erw-1233-1234" , "1ce1-2erw-1233-1232" ,"1ce1-2erw-1233-1233" , ) , 'DATACOLLECTOR_WAS' : ("1ce1-2erw-1233-1234" , ) } ,
	'server-02' : { 'DATACOLLECTOR' : ("1ce1-2erw-1233-1234" , ) , 'JMXCOLLECTOR_DS' : ("1234-12-12-12" ,) } ,
	'server-01' : { 'JMXCOLLECTOR' : ( "1ce1-2erw-1233-1234", ) }
}
