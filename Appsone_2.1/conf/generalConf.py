############# Nomad and consul ##############
DATA_CENTER = "dc1"
LOG_LEVEL = "INFO"
BOOT_STRAP_EXPECT = '3'
DATA_DIR_CONSUL = "/opt/consul"
DATA_DIR_NOMAD = "/opt/nomad"
REGION = 'IN'

# Specify local node and other installation nodes
NODES = {'server-01':'192.168.2.153','server-02':'192.168.2.212','server-03':'192.168.2.213'}

## Where the appsone service  will be installed on all nodes
SER_INSTALL_PATH = "/opt/appnomic" 

## For datacollector was
IBM_JRE_PATH = "/usr/lib/jvm/ibm-java-x86_64-jre-7.0-5.0/bin/java"

## Nomad user(non-root) to run all the appsone services
NOMAD_USER = 'appuser'
DB_USER = 'appsone'
MIGRATION = 'FALSE'

# This value will be added against ip address of local node in all node's /etc/hosts
# E.g. 192.168.2.213  rhel-213.appnomic
# Thi is required for all the certificates to work as they have common name *.appnomic
HOST_ENTRY = 'rhel-213.appnomic'

## Keycloak certificate and tomcat license file path
# This is required only if keycloak is set to run with ssl
KEYCLOAK_SSL = 'Yes'
KEYCLOAK_SSL_CERT_PATH = '/tmp/keycloak_sso_keystores.cer' 
TOMCAT_LICENSE_PATH = '/tmp/License.lic'

#Appsone services

## "COLLECTIONSERVICE" has to be present in one of the node.
SERVERS = { 'server-01' : 
		{ 'TXNCOLLECTOR' : 2 , 
		'JMXCOLLECTOR_WAS' : 1 ,
		'COMPONENTAGENT' : 1 ,
		'COLLATINGSERVICE' :1 ,
		'GRPC' : 1 ,
		'CXO' : 1,
		'ETL-STITCHING' : 1 ,
		'TFP-DS' :1  },
           'server-02' : 
		{ 'DATACOLLECTOR' : 1 ,
		'FORENSICSERVICE' :1 ,
		'JMXCOLLECTOR' : 1 ,
		'ALERTINGSERVICE' : 1 ,
		'COMPONENTAGENT' : 1 ,
		'ETL-LTM' : 1 ,
		'PATTERNRT' : 1 ,
		'INCIDENTMGMTBROKER' : 1 ,
		'ETL-TFP' : 1 } ,
             'server-03' : 
		{ 'TXNCOLLECTOR' : 2 , 'DATACOLLECTOR' : 1 , 'DATACOLLECTOR_WAS' : 1 , 'JMXCOLLECTOR_JBOSS' : 1 , 'COLLECTIONSERVICE' : 1 } 
	   }

#Docker services configuration
DOCKER_SERVICES = {
        'server-01' : [ 'CASSANDRA_NODE3' , 'ZOOKEEPER' , 'HADOOP_NAMENODE' , 'FLINK_NODE2' , 'COUCHBASE' ],
        'server-02' : [ 'CASSANDRA_NODE2','FLINK_NODE1' , 'CAPACITY_PLANNING', 'TOMCAT' ]  ,
        'server-03' : [ 'HAPROXY', 'RABBITMQ' , 'HADOOP_DATANODE' ,'KEYCLOAK' , 'PERCONA' , 'CASSANDRA_NODE1' ]
                }
