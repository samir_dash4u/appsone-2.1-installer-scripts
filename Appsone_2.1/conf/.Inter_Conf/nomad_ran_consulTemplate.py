#!/usr/bin/env python
from string import Template

NOMAD = Template ("""log_level = "$log_level"
data_dir = "$data_dir"
bind_addr = "0.0.0.0"
region = "$reg"
datacenter = "$dc"
leave_on_terminate = true

advertise {
  http = "$ip"
  rpc = "$ip"
  serf = "$ip"
}

ports {
  http = "$http_port"
  rpc = "$rpc_port"
  serf = "$serf_port"
}
server {
  enabled = true
  bootstrap_expect = $bs
  encrypt = "BoL4ZbSZ/o0zAUFpKoMyRA=="
}

tls {
  http = true
  rpc  = true
  ca_file   = "/etc/nomad/nomad-ca.pem"
  cert_file = "/etc/nomad/server.pem"
  key_file  = "/etc/nomad/server-key.pem"
  tls_min_version = "tls12"
  verify_server_hostname = false
  verify_https_client    = false
}


client {
  enabled = true
  servers = [$server_str]

  chroot_env {
        "/bin" = "/bin"
        "/lib64" = "/lib64"
	"/etc" = "/etc"
	"/home/$user" = "/home/$user"
        "/usr/lib/jvm" = "/usr/lib/jvm"
	"/usr/bin" = "/usr/bin"
        "$ser_install_path" = "$ser_install_path"
  }
}

consul {
  ssl = true
  ca_file   = "/etc/nomad/nomad-ca.pem"
  cert_file = "/etc/nomad/server.pem"
  key_file  = "/etc/nomad/server-key.pem"
  address   = "127.0.0.1:8501"
}

""")

TOMCAT_NOMAD = Template ("""log_level = "$log_level"
data_dir = "$data_dir"
bind_addr = "0.0.0.0"
region = "$reg"
datacenter = "$dc"
leave_on_terminate = true

server {
  encrypt = "BoL4ZbSZ/o0zAUFpKoMyRA=="
}

client {
  enabled = true
  servers = [$server_str]
  chroot_env {
        "/bin" = "/bin"
        "/lib64" = "/lib64"
        "/etc" = "/etc"
        "/usr/bin" = "/usr/bin"
  }
}

tls {
  http = true
  rpc  = true
  ca_file   = "/etc/nomad/nomad-ca.pem"
  cert_file = "/etc/nomad/server.pem"
  key_file  = "/etc/nomad/server-key.pem"
  tls_min_version = "tls12"
  verify_server_hostname = false
  verify_https_client    = false
}

""")

RAN = Template ("""rootpath=$rootpath
ranport=$ranport
""")

CONSUL = Template ("""{
  "datacenter": "$dc",
  "data_dir": "$data_dir",
  "log_level": "$log_level",
  "node_name": "$node_name",
  "server": true,
  "bootstrap_expect": $bs,
  "encrypt": "klFHydS3RAK8szzr7YyClQ==",
  "encrypt_verify_incoming" : true,
  "encrypt_verify_outgoing" : true,
  "key_file": "/etc/nomad/server-key.pem",
  "cert_file": "/etc/nomad/server.pem",
  "ca_file": "/etc/nomad/nomad-ca.pem",
  "verify_outgoing" : false,
  "verify_incoming" : false,
  "verify_server_hostname" : false,
  "verify_incoming_https" : false,
  "ports" : { "http" : 8500, "https": 8501 },
  "tls_min_version" : "tls12",
  "client_addr": "0.0.0.0",
  "advertise_addr": "$ip",
  "advertise_addr_wan": "$ip"
}
""")
