log_level = "INFO"
data_dir = "/opt/nomad"
bind_addr = "0.0.0.0"
region = "IN"
datacenter = "dc1"
leave_on_terminate = true

server {
  encrypt = "BoL4ZbSZ/o0zAUFpKoMyRA=="
}

client {
  enabled = true
  servers = ["192.168.2.213:4647","192.168.2.212:4647","192.168.2.153:4647"]
  chroot_env {
        "/bin" = "/bin"
        "/lib64" = "/lib64"
        "/etc" = "/etc"
        "/usr/bin" = "/usr/bin"
  }
}

tls {
  http = true
  rpc  = true
  ca_file   = "/etc/nomad/nomad-ca.pem"
  cert_file = "/etc/nomad/server.pem"
  key_file  = "/etc/nomad/server-key.pem"
  tls_min_version = "tls12"
  verify_server_hostname = false
  verify_https_client    = false
}

