#!/usr/bin/python
import sys
import io
import os

confdir=sys.argv[1]
sys.path.insert(0,confdir)

from generalConf import *
from extendedConf import *

def createString():
	server_str=""
	LI = len(IP_LIST) - 1
	for IP in IP_LIST:
		if IP_LIST.index(IP) == LI:
			server_str = server_str + IP
		else:
			server_str = server_str + IP + ' '		
	return server_str

# Read the lines from the template, substitute the values, and write to the new config file
def createWrapperConfig(server_str):
	wrapper = io.open('wrapper' + '.conf', 'w')
	for line in io.open('wrapper_template', 'r'):
		line = line.replace('$LN', LOCAL_NODE)
		line = line.replace('$NODES', server_str)
		line = line.replace('$SIP',SER_INSTALL_PATH)
		line = line.replace('$NOMAD_USER',NOMAD_USER)
		wrapper.write(line)
	wrapper.close()

IP_LIST = NODES.values()
server_str = createString()
for IP in IP_LIST:
	createWrapperConfig(server_str)
