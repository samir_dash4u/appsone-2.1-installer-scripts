#!/usr/bin/python
import sys
import io
import os
import fileinput

confdir=sys.argv[1]
sys.path.insert(0,confdir)

from generalConf import *
from extendedConf import *
from nomad_ran_consulTemplate import *


def createString(IP_LIST):
	server_str=""
	for IP in IP_LIST:
		if IP_LIST.index(IP) == 0:
			server_str = server_str + '"' + IP + ':' + RPC_PORT + '"'
		else:
			server_str = server_str + ',"' + IP + ':' + RPC_PORT + '"'
	return server_str

def createLastString():
	rem_nodes = NODES.values()
	rem_nodes.remove(LOCAL_NODE)
	str1='"' + 'start_join' + '"' + ':' + '['
	j = 0
	for i in rem_nodes:
		file_name = 'consul_' + i + '.json'
		if rem_nodes.index(i) == 0:
			str1 = str1 + '"' + LOCAL_NODE + '"'
			str0 = str1 + ']'
			fh = open(file_name, "r")
			buff = fh.readlines()
			fh.close()
			fh = open(file_name, "w")
			for line in buff:
				if line == "}\n":
					line = ',' + str0 + '\n' + line
				fh.write(line)
			
		
			old_nodes =[LOCAL_NODE]
	  	else:
			str1 = str1 + ',' + '"' + old_nodes[j] + '"'
			str2 = str1 + ']'
			fh = open(file_name, "r")
                        buff = fh.readlines()
                        fh.close()
                        fh = open(file_name, "w")
                        for line in buff:
                                if line == "}\n":
                                        line = ',' + str2 + '\n' + line
                                fh.write(line)
		j = j + 1
		old_nodes.append(i)
	
def createRanConfig():
	fileh = open('ran.conf', 'w')
	ROOTPATH = SER_INSTALL_PATH + '/InfraService'
    	fileh.write(RAN.safe_substitute(rootpath=ROOTPATH,ranport=RAN_PORT))
    	fileh.close()

def createNomadSConfig(ip,server_str):
	nomad_s = open('nomad_' + ip + '.hcl', 'w')
	nomad_s.write(NOMAD.safe_substitute(dc=DATA_CENTER,data_dir=DATA_DIR_NOMAD,log_level=LOG_LEVEL,bs=BOOT_STRAP_EXPECT,reg=REGION,ip=ip,http_port=HTTP_PORT,rpc_port=RPC_PORT,serf_port=SERF_PORT,ser_install_path=SER_INSTALL_PATH,server_str=server_str,user=NOMAD_USER))
	nomad_s.close()


def tomcatNomad(server_str):
	nomad_s = open('nomad_tomcat.hcl', 'w')
	nomad_s.write(TOMCAT_NOMAD.safe_substitute(dc=DATA_CENTER,data_dir=DATA_DIR_NOMAD,log_level=LOG_LEVEL,reg=REGION,ser_install_path=SER_INSTALL_PATH,server_str=server_str))
	nomad_s.close()

def createConsulConfig(node_n, node_i):
	consul_config = open('consul_' + node_i + '.json', 'w')
	consul_config.write(CONSUL.safe_substitute(dc=DATA_CENTER,data_dir=DATA_DIR_CONSUL,log_level=LOG_LEVEL,bs=BOOT_STRAP_EXPECT,node_name=node_n,ip=node_i))
	consul_config.close()

basedir = os.path.dirname(os.path.realpath(__file__))
def main():
	createRanConfig()
	IP_LIST = NODES.values()
	NO_OF_NODES = len(NODES)
	server_str = createString(IP_LIST)
	tomcatNomad(server_str)
	for IP in IP_LIST:
		createNomadSConfig(IP,server_str)
	for i in range(0,NO_OF_NODES):
    		node_n = NODES.keys()[i]
    		node_i = NODES.values()[i]
    		createConsulConfig(node_n,node_i)
	createLastString()

main()
