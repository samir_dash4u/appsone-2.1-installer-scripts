#!/bin/bash

logit()
{
        datestamp=$(date +"%d-%m-%Y:%H:%M:%S");
        echo "$datestamp : $* " >> $LOG_FILE
        return 0;
}

loadConfig()
{
	CONFIGFILE="${BASEDIR}/conf/.Inter_Conf/wrapper.conf"
	cd "${BASEDIR}/conf/.Inter_Conf/"
	if [ -f "genWrapperConfig.py" ] && [ -f "wrapper_template" ];then
		python genWrapperConfig.py "$CONFDIR" 2>/dev/null  1>/dev/null
		if [ ! -f "$CONFIGFILE" ]; then
			echo 'Could not create config file. Look at genWrapperConfig.py and retry.'
			logit 'Could not create config file. Look at genWrapperConfig.py and retry.'
			exit 1
		else
			rm -f genWrapperConfig.pyc 2>/dev/null  1>/dev/null
		fi
	else
		echo 'genWrapperConfig.py or/and wrapper_template.conf is not available. Aborting..'
		logit 'genWrapperConfig.py or/and wrapper_template.conf is not available. Aborting..'
		exit 1
	fi
	. "$CONFIGFILE" 
	if [ $? -ne 0 ]; then
	        echo "Could not load config file"
		logit "Could not load config file"
	        exit 1
	else
		logit "Configuration file loaded successfully"
	fi
	return 0
}

genConsulNomadRanConfigs()
{
	cd ${BASEDIR}/conf/.Inter_Conf/
	if [ -f "genNomadConsulRanConfigs.py" ] && [ -f "nomad_ran_consulTemplate.py" ]; then
		python genNomadConsulRanConfigs.py "$CONFDIR" 2>/dev/null  1>/dev/null
		rm -f UpgradeAppsOneDB.pyc 2>/dev/null
		rm -f genNomadConsulRanConfigs.pyc 2>/dev/null  1>/dev/null	
		file_count=`ls consul_*.json | wc -l`
		if [ "$NO_OF_NODES" != "$file_count" ]	;then
			echo 'Could not create consul_<ip>.json files properly. Look at genConsulConfig.py and retry.'
			logit 'Could not create consul_<ip>.json files properly. Look at genConsulConfig.py and retry.'
			exit 1
		fi
		file_count=`ls nomad_*.hcl | wc -l`
		file_count=$((file_count - 1))
		if [ "$NO_OF_NODES" != "$file_count" ]	;then
			echo 'Could not create nomad_<ip>.hcl files properly. Look at genConsulConfig.py and retry.'
			logit 'Could not create nomad_<ip>.hcl files properly. Look at genConsulConfig.py and retry.'
			exit 1
		fi
		file_count=`ls ran.conf | wc -l`
		if [ "$file_count" -ne 1 ]	;then
			echo 'Could not create ran.conf. Look at genConsulConfig.py and retry.'
			logit 'Could not create ran.conf. Look at genConsulConfig.py and retry.'
			exit 1
		fi
	else
		echo 'genNomadConsulRanConfigs.py or/and template file is not available. Aborting..'
		logit 'genNomadConsulRanConfigs.py or/and template file is not available. Aborting..'
		exit 1
	fi
	logit "Consul, ran and  nomad configurtaion files created.."
	return 0
}

copyConfigsStartServices()
{
	cd ${BASEDIR}/conf/.Inter_Conf/
	FOLDER="Master_Services"

	### for local node
	echo "Copying the generated service configuration files on local node"
	logit "Copying the generated service configuration files on local node"
        cp "consul_${LOCAL_NODE}.json" /etc/consul/ 2>/dev/null  1>/dev/null
        cp "nomad_${LOCAL_NODE}.hcl" /etc/nomad/ 2>/dev/null  1>/dev/null
        cp "ran.conf" /etc/ran.d/ 2>/dev/null  1>/dev/null
	cp nomad_tomcat.hcl ${SER_INSTALL_PATH}/Appsone_Service/data/tomcat/nomad/  2>/dev/null  1>/dev/null
	cp $BASEDIR/conf/cert/* /etc/nomad > /dev/null 2>&1
	cp $BASEDIR/conf/cert/* "${SER_INSTALL_PATH}/Appsone_Service/data/tomcat/nomad" > /dev/null 2>&1
        rm -f "consul_${LOCAL_NODE}.json" "nomad_${LOCAL_NODE}.hcl" "ran.conf" 2>/dev/null
	echo "Starting the services ran,consul,docker in : $LOCAL_NODE"
	logit "Starting the service ran,consul,docker in : $LOCAL_NODE"
        python ${SER_INSTALL_PATH}/Master_Services/startServices.py 'ran' 2>/dev/null
        if [ $? -eq 100 ]; then
                echo "Service could not start. Aborting...."
                logit "Service could not start. Aborting...."
                exit 1
	else
		echo "Services started susseccfully"
		logit "Services started susseccfully"
        fi

	for node in `echo $NODES`
	do
		if [ "$node" = "$LOCAL_NODE" ]; then
			continue;
		else
			echo "Copying the generated service configuration files on $node"
			logit "Copying the generated service configuration files on $node"
			scp "consul_$node.json" ${USER}@${node}:/etc/consul/ 2>/dev/null  1>/dev/null
			scp "nomad_$node.hcl" ${USER}@${node}:/etc/nomad/ 2>/dev/null  1>/dev/null
			scp nomad_tomcat.hcl ${USER}@${node}:${SER_INSTALL_PATH}/Appsone_Service/data/tomcat/nomad/  2>/dev/null  1>/dev/null
			scp $BASEDIR/conf/cert/* ${USER}@${node}:/etc/nomad > /dev/null 2>&1
			scp $BASEDIR/conf/cert/* ${USER}@${node}:"${SER_INSTALL_PATH}/Appsone_Service/data/tomcat/nomad" > /dev/null 2>&1
			
			rm -f "consul_$node.json" "nomad_$node.hcl" 2>/dev/null
			echo "Starting the service consul,docker in : $node "
			logit "Starting the service consul,docker in : $node "
			ssh -o ConnectTimeout=200 ${USER}@${node} "cd ${SER_INSTALL_PATH}/${FOLDER}; python startServices.py service 2>/dev/null"
                        if [ $? -eq 100 ]; then
		                echo "Service could not start. Aborting...."
        		        logit "Service could not start. Aborting...."
				exit 1
			else
				echo "Services started susseccfully"
				logit "Services started susseccfully"
                        fi
		fi
	done
	### Starting nomad in local node
	echo "Starting service nomad in : $LOCAL_NODE"
	logit "Starting service nomad in : $LOCAL_NODE"
	python ${SER_INSTALL_PATH}/Master_Services/startServices.py 'nomad' 2>/dev/null
        if [ $? -eq 100 ]; then
                echo "Service could not start. Aborting...."
                logit "Service could not start. Aborting...."
                exit 1
	else
		echo "Services started susseccfully"
		logit "Services started susseccfully"
        fi
	rm -rf ${SER_INSTALL_PATH}/Master_Services 2>/dev/null

	for node in `echo $NODES`
	do
		if [ "$node" = "$LOCAL_NODE" ]; then
			continue;
		else
			echo "Starting service nomad in : $node"
			logit "Starting service nomad in : $node"
			ssh -o ConnectTimeout=200 ${USER}@${node} "cd ${SER_INSTALL_PATH}/${FOLDER}; python startServices.py nomad 2>/dev/null ; cd .. ; rm -rf ${SER_INSTALL_PATH}/${FOLDER} 2>/dev/null"
                        if [ $? -eq 100 ]; then
		                echo "Service could not start. Aborting...."
        		        logit "Service could not start. Aborting...."
				exit 1
			else
				echo "Services started susseccfully"
				logit "Services started susseccfully"
                        fi
		fi
	done
}

install_RPM()
{
	FOLDER="Master_Services"
	RPM_TAR_NAME="Master_Services.tar"
	cd ${BASEDIR}/binary/
	## Local
	if [ -f "$RPM_TAR_NAME" ];then
		cp "$RPM_TAR_NAME" "$SER_INSTALL_PATH"
		cd "$SER_INSTALL_PATH"
		echo "Installing consul, docker, ran and other required service on node $LOCAL_NODE"
		logit "Installing consul, docker, ran and other required service on node $LOCAL_NODE"
		tar -zxf "$RPM_TAR_NAME" --warning=no-timestamp 2>/dev/null
	        if [ $? -eq 0 ]; then
			rm -f "$RPM_TAR_NAME" 2>/dev/null
	        	python $FOLDER/appsoneInstaller.py
			python $FOLDER/installRanMysql.py
			if [ $? -eq 0 ]; then
				echo "Services installed successfully"
				logit "Services installed successfully"
			else
				echo "Service installation failed. Exiting"
				logit "Service installation failed. Exiting"
				exit 1
			fi
			cp ${BASEDIR}/lib/appsoneUninstaller.sh "$SER_INSTALL_PATH"
			rm -rf /opt/localrepo
			rm -f $FOLDER/*.pyc 2>/dev/null	
	    	else
        		echo "Could not untar on local. Aborting.."
			logit "Could not untar on local. Aborting.."
	        	exit 1
        	fi
	else
      		echo "$RPM_TAR_NAME not found. Please check the config file. Aborting.."
		logit "$RPM_TAR_NAME not found. Please check the config file. Aborting.."
	    	exit 1
    	fi
	
	cd ${BASEDIR}/binary/
	## Remote: For Node 2,3..
	for node in `echo $NODES`
	do
		if [ "$node" = "$LOCAL_NODE" ]; then
			continue
		else
			echo "Installing consul, docker and other required service on node $node"
			logit "Installing consul, docker and other required service on node $node"
			ssh -o ConnectTimeout=200 ${USER}@${node} "mkdir -p ${SER_INSTALL_PATH}"
			scp "$RPM_TAR_NAME" ${USER}@${node}:"$SER_INSTALL_PATH" 2>/dev/null  1>/dev/null
			ssh -o ConnectTimeout=200 ${USER}@${node} "cd ${SER_INSTALL_PATH}; tar -zxf ${RPM_TAR_NAME} --warning=no-timestamp;  rm -f ${RPM_TAR_NAME} 2>/dev/null ; cd ${FOLDER}; python appsoneInstaller.py ; rm -rf /opt/localrepo ; rm -f *.pyc 2>/dev/null"
			if [ $? -eq 0 ]; then
				echo "Services installed successfully"
				logit "Services installed successfully"
			else
				echo "Service installation failed. Exiting"
				logit "Service installation failed. Exiting"
				exit 1
			fi
			scp ${BASEDIR}/lib/appsoneUninstaller.sh ${USER}@${node}:"$SER_INSTALL_PATH" 2>/dev/null  1>/dev/null
		fi
	done
	disable_tls
}

disable_tls()
{
	echo "Disabling TLSv1 and TLSv1.1"
	logit "Disabling TLSv1 and TLSv1.1"
	for node in `echo $NODES`
	do
		if [ "$node" = "$LOCAL_NODE" ]; then
			 sed -i 's/^jdk.tls.disabledAlgorithms=.*/jdk.tls.disabledAlgorithms=SSLv3, RC4, MD5withRSA, DH keySize < 1024, EC keySize < 224, DES40_CBC, RC4_40, TLSv1, TLSv1.1/;623d' /usr/lib/jvm/zulu-8/jre/lib/security/java.security
		else
			 ssh -o ConnectTimeout=200 ${USER}@${node} "sed -i 's/^jdk.tls.disabledAlgorithms=.*/jdk.tls.disabledAlgorithms=SSLv3, RC4, MD5withRSA, DH keySize < 1024, EC keySize < 224, DES40_CBC, RC4_40, TLSv1, TLSv1.1/;623d' /usr/lib/jvm/zulu-8/jre/lib/security/java.security"
		fi
	done
	logit "TLSv1 and TLSv1.1 disabled in /usr/lib/jvm/zulu-8/jre/lib/security/java.security"	
}

install_Services()
{
	SER_DIR=`echo $SER_TAR_NAME | cut -d'.' -f1`
	## Local
	mkdir -p "$SER_INSTALL_PATH"
	cd "${BASEDIR}/binary/"
	## Local : For Node 1
	if [ -f "$SER_TAR_NAME" ];then
		echo "Copying appsone services on local node $LOCAL_NODE"
		logit "Copying appsone services on local node $LOCAL_NODE"
		tar -zxf "$SER_TAR_NAME" -C "$SER_INSTALL_PATH" --warning=no-timestamp 2>/dev/null
        	if [ $? -ne 0 ]; then
	        	echo "Could not untar $SER_TAR_NAME. Aborting.."
			logit "Could not untar $SER_TAR_NAME. Aborting.."
		        exit 1
		else
			if [ "$KEYCLOAK_SSL" == "YES" ]; then
				cp $KEYCLOAK_SSL_CERT_FILE  "$SER_INSTALL_PATH/$SER_DIR/data/cert" > /dev/null 2>&1
			fi
			cp $TOMCAT_LICENSE_FILE "$SER_INSTALL_PATH/$SER_DIR/data/tomcat/webapps" > /dev/null 2>&1
			cd "$SER_INSTALL_PATH/$SER_DIR"; chown -R "$NOMAD_USER":"$NOMAD_USER" *
			cd "$SER_INSTALL_PATH/$SER_DIR/data/"; chown -R 1000:1000 keycloak
			cd "$SER_INSTALL_PATH/Appsone_Service/capacity-planning/"; chown -R 999:999 Config/ CP/
			echo "Appsone service copied on local node"
			logit "Appsone service copied on local node"
        	fi
	else
      		echo "$SER_TAR_NAME not found. Aborting Installaion.."
		logit "$SER_TAR_NAME not found. Aborting Installaion.."
	    	exit 1
    	fi
	
	## Remote
	cd ${BASEDIR}/binary/
	for node in `echo $NODES`
	do
		if [ "$node" = "$LOCAL_NODE" ]; then
			continue
		else
			echo "Copying appsone services on node $node"
			logit "Copying appsone services on node $node"
			ssh -o ConnectTimeout=200 ${USER}@${node} "mkdir -p ${SER_INSTALL_PATH}"
			scp "$SER_TAR_NAME" ${USER}@${node}:"$SER_INSTALL_PATH" 2>/dev/null  1>/dev/null
			ssh -o ConnectTimeout=200 ${USER}@${node} "cd ${SER_INSTALL_PATH}; tar -zxf ${SER_TAR_NAME} --warning=no-timestamp ; rm -f ${SER_TAR_NAME} 2>/dev/null; cd ${SER_INSTALL_PATH}/${SER_DIR}; chown -R ${NOMAD_USER}:${NOMAD_USER} *; cd ${SER_INSTALL_PATH}/${SER_DIR}/data/; chown -R 1000:1000 keycloak ; cd "${SER_INSTALL_PATH}/Appsone_Service/capacity-planning/"; chown -R 999:999 Config/ CP/"
			if [ "$KEYCLOAK_SSL" == "YES" ]; then
				scp $KEYCLOAK_SSL_CERT_FILE  ${USER}@${node}:"$SER_INSTALL_PATH/$SER_DIR/data/cert" > /dev/null 2>&1
			fi
			scp $TOMCAT_LICENSE_FILE ${USER}@${node}:"$SER_INSTALL_PATH/$SER_DIR/data/tomcat/webapps" > /dev/null 2>&1
			echo "Appsone service copied on $node"
			logit "Appsone service copied on $node"
		fi
	done
}

copy_Infra()
{
	cd ${BASEDIR}/binary/
	if [ -f "InfraService.tar" ]; then
		echo "Extracting docker services on $LOCAL_NODE"
		tar -zxf "InfraService.tar" -C "$SER_INSTALL_PATH" --warning=no-timestamp 2>/dev/null
		if [ $? -eq 0 ]; then
			echo "Docker services extracted successfully"
			logit "Docker services extracted successfully"
		else
			echo "Extraction failed. Exiting"
			logit "Extraction failed. Exiting"
			exit 1
		fi
	else
		echo "Infra Service Folder is missing. Aborting.."
		logit "Infra Service Folder is missing. Aborting.."
		exit 1
	fi
}

generate_keycloak_cer()
{
	cd "${SER_INSTALL_PATH}/nomadFile/"
	grep -q TRUSTSTORE keycloak.nomad
	if [ $? -eq 0 ]; then
		echo "Creating certificate for keycloak"
		logit "Creating certificate for keycloak"
		CERFILE=$(ls ${SER_INSTALL_PATH}/Appsone_Service/data/cert/ | grep "\.cer$")
		if [ "x$CERFILE" == "x" ]; then
			echo ".cer file for keycloak ssl not found. Keycloak jks file can not be created. Please create the same manually before starting keycloak"
			logit ".cer file for keycloak ssl not found. Keycloak jks file can not be created. Please create the same manually before starting keycloak"
			return 0
		else
			if [ ! -f $SER_INSTALL_PATH/Appsone_Service/data/cert/keycloak.jks ]; then
				keytool -genkey -keyalg RSA -keystore $SER_INSTALL_PATH/Appsone_Service/data/cert/keycloak.jks -dname "CN=*.appnomic, OU=Development, O=Appnomic, L=Bangalore, S=Karnataka, C=IN" -validity 3650 -storepass changeit -keypass changeit
				keytool -import -keystore $SER_INSTALL_PATH/Appsone_Service/data/cert/keycloak.jks -file $SER_INSTALL_PATH/Appsone_Service/data/cert/$CERFILE -storepass changeit -noprompt -alias localhost
			fi
		fi
	fi
}

copy_tpls()
{	
	SER_DIR="Appsone_Service/conf-template"
	cd $BASEDIR/JobsConfigs
	
	for node in `echo $NODES`
	do
		echo "Copying the service configuration template files on $node"
		if [ "$node" = "$LOCAL_NODE" ]; then
			cp *_TXNCOLLECTOR_*.config.tpl "${SER_INSTALL_PATH}/${SER_DIR}/transaction-collector" 2>/dev/null 1>/dev/null
			cp *_DATACOLLECTOR_*.config.tpl "${SER_INSTALL_PATH}/${SER_DIR}/data-collector-core"  2>/dev/null 1>/dev/null
			cp *_COLLECTIONSERVICE_*config.tpl "${SER_INSTALL_PATH}/${SER_DIR}/collection-service"  2>/dev/null 1>/dev/null
			cp *_ETL-LTM_*config.tpl "${SER_INSTALL_PATH}/${SER_DIR}/log-txn-monitoring"  2>/dev/null 1>/dev/null
			cp *_ETL-STITCHING_*config.tpl "${SER_INSTALL_PATH}/${SER_DIR}/stitching-etl"  2>/dev/null 1>/dev/null
			cp *_ETL-TFP_*config.tpl "${SER_INSTALL_PATH}/${SER_DIR}/tfp-etl"  2>/dev/null 1>/dev/null
			cp haproxy.cfg "${SER_INSTALL_PATH}/Appsone_Service/conf/HAPROXY/" 2>/dev/null 1>/dev/null
		else
			scp *_TXNCOLLECTOR_*.config.tpl ${USER}@${node}:"${SER_INSTALL_PATH}/${SER_DIR}/transaction-collector" 2>/dev/null  1>/dev/null
			scp *_DATACOLLECTOR_*.config.tpl ${USER}@${node}:"${SER_INSTALL_PATH}/${SER_DIR}/data-collector-core" 2>/dev/null  1>/dev/null
			scp *_COLLECTIONSERVICE_*config.tpl ${USER}@${node}:"${SER_INSTALL_PATH}/${SER_DIR}/collection-service"  2>/dev/null 1>/dev/null
			scp *_ETL-LTM_*config.tpl ${USER}@${node}:"${SER_INSTALL_PATH}/${SER_DIR}/log-txn-monitoring"  2>/dev/null 1>/dev/null
			scp *_ETL-TFP_*config.tpl ${USER}@${node}:"${SER_INSTALL_PATH}/${SER_DIR}/tfp-etl"  2>/dev/null 1>/dev/null
			scp *_ETL-STITCHING_*config.tpl ${USER}@${node}:"${SER_INSTALL_PATH}/${SER_DIR}/stitching-etl"  2>/dev/null 1>/dev/null
			scp haproxy.cfg ${USER}@${node}:"${SER_INSTALL_PATH}/Appsone_Service/conf/HAPROXY/" 2>/dev/null 1>/dev/null
			scp ${SER_INSTALL_PATH}/Appsone_Service/data/cert/keycloak.jks ${USER}@${node}:${SER_INSTALL_PATH}/Appsone_Service/data/cert/keycloak.jks 2>/dev/null 1>/dev/null
			sort -u .open_port.txt > .open_port_sort.txt
			scp .open_port_sort.txt ${USER}@${node}:/tmp/ 2>/dev/null 1>/dev/null
		fi
	done
	rm -f *_TXNCOLLECTOR_*.config.tpl *_DATACOLLECTOR_*.config.tpl *_COLLECTIONSERVICE_*config.tpl *_ETL-LTM_*config.tpl *ETL-STITCHING* *ETL-TFP* haproxy.cfg 2>/dev/null
}

callNomadTpl()
{
	cd $BASEDIR/JobsConfigs
	if [ -f "genJobsConfigs.py" ];then
		echo "Creating nomad jobs and tpl files and starting percona database.."
		logit "Creating nomad jobs and tpl files and starting percona database.."
		python -W"ignore" genJobsConfigs.py "$CONFDIR" "$ROOT_DB_USER" "$ROOT_DB_PASS" "$PYTHONDEPSDIR" "${BASEDIR}/lib/fresh-install"
		exit_status=$?
		case $exit_status in
		101)
			echo "Could not load configuration file for configuring DB and creating nomad files. Aborting Installation...."
			logit "Could not load configuration file for configuring DB and creating nomad files. Aborting Installation...."
			exit 1
			;;
		102)
			echo "value not provided for PERCONA_IP . percona.nomad file will not get generated. Aborting Installation...."
			logit "value not provided for PERCONA_IP . percona.nomad file will not get generated. Aborting Installation...."
			exit 1
			;;
		103)
			echo "Not able to change the permission for percona data directory. Aborting Installation...."
			logit "Not able to change the permission for percona data directory. Aborting Installation...."
			exit 1
			;;
		104)
			echo "Unable to copy/untar db data directory.Aborting Installation.... "
			logit "Unable to copy/untar db data directory.Aborting Installation.... "
			exit 1
			;;
		105)
			echo "DB data file not found or unknown file format. Aborting Installation...."
			logit "DB data file not found or unknown file format. Aborting Installation...."
			exit 1
			;;
		106)
			echo "Not able to start percona. Aborting Installation...."
			logit "Not able to start percona. Aborting Installation...."
			exit 1
			;;
		108)
			echo "Not able to create DB. Aborting Installation...."
			logit "Not able to create DB. Aborting Installation...."
			exit 1
			;;
		107)
			echo "Not able to connect to database.Aborting Installation.... "
			logit "Not able to connect to database.Aborting Installation.... "
			exit 1
		esac

		echo "Removing residual files (.pyc python compiled files)"
		logit "Removing residual files (.pyc python compiled files)"
		rm -f *.pyc service_ip.py 2> /dev/null
		rm -f ${CONFDIR}/*.pyc 2> /dev/null
	else
		echo "JobsConfigs.py/genJobsConfigs.py is not available. Make sure these files are available at $BASEDIR and retry. Aborting.."
		logit "JobsConfigs.py/genJobsConfigs.py is not available. Make sure these files are available at $BASEDIR and retry. Aborting.."
		exit 1
	fi
	
}

check_cluster_members()
{
	tool=$1
	Ack=$2
	counter=5
	nomadPort=$(grep ^HTTP_PORT ${BASEDIR}/conf/extendedConf.py | cut -d = -f2 | sed -e "s/'//g;s/ //g")
	export NOMAD_CACERT="/etc/nomad/nomad-ca.pem"
	export NOMAD_CLIENT_KEY="/etc/nomad/cli-key.pem"
	export NOMAD_CLIENT_CERT="/etc/nomad/cli.pem"
	export NOMAD_ADDR="https://localhost:$nomadPort"
	for i in `seq 1 $counter`;do
		if [ "$tool" = "consul" ]; then
			tool_count=`consul members | wc -l`
		else
			tool_count=`nomad server members | wc -l`
		fi
		expected_count=`expr $NO_OF_NODES + 1`
		if [ $tool_count -ne $expected_count ]; then
			sleep 12
		else
			if [ "$tool" = "nomad" ]; then
				nomad server members | grep "true" 2>/dev/null 1>/dev/null
				logit "Create leader node for nomad successfully."
				if [ $? -eq 0 ]; then
					break
				else
					echo "Could not create leader node for nomad."
					logit "Could not create leader node for nomad."
					return 10
				fi
			fi
			break
		fi
		if [ $i -eq 5 ] && [ -z "$Ack" ]; then
			echo "cluster could not be created for ${tool}."
			logit "cluster could not be created for ${tool}."
			return 10
		elif [ $i -eq 5 ] && [ ! -z "$Ack" ]; then
			echo "Still, the cluster could not be created for ${tool}. Aborting.."
			logit "Still, the cluster could not be created for ${tool}. Aborting.."
			exit 1
		fi
	done 
	return 0
}

start_service_reboot()
{
	##For local node:
	echo "Enabling docker, nomad, consul and ran to start automatically after system reboot on local node"
	logit "Enabling docker, nomad, consul and ran to start automatically after system reboot on local node"
	systemctl enable docker.service
	systemctl enable nomad.service
	systemctl enable consul.service
	systemctl enable ran
	
	for node in `echo $NODES`
	do
		if [ "$node" = "$LOCAL_NODE" ]; then
			continue
		else
			echo "Enabling docker, nomad and consul to start automatically after system reboot on local node"
			logit "Enabling docker, nomad and consul to start automatically after system reboot on local node"
			ssh -o ConnectTimeout=200 ${USER}@${node} "systemctl enable docker.service; systemctl enable nomad.service; systemctl enable consul.service "
			
		fi
	done

}

put_enc_pass()
{
	DB_PASS=$1
	KC_PASS=$2
	cd $BASEDIR/lib
	./crypt set -backend consul service/perconadb/password_rsa <(echo "$DB_PASS")
	./crypt set -backend consul service/keycloak/password <(echo "$KC_PASS")
	enc_dp_pwd=`java EncryptionLogic "$DB_PASS" Encrypt`
	echo ""
	consul kv put "service/perconadb/password" $enc_dp_pwd
	cd $BASEDIR
}

db_pri_pwd()
{
	echo "Providing access to $DB_USER"
	logit "Providing access to $DB_USER"
	DB_IP=`grep -w "value" $SER_INSTALL_PATH/nomadFile/percona.nomad | awk -F= '{print $2}' | sed "s/ //g" | sed "s/\"//g"`
	MYSQL_PWD="$ROOT_DB_PASS"
	export MYSQL_PWD
	for node in `echo $NODES`
	do
		mysql -h ${DB_IP} -u "$ROOT_DB_USER" -P ${DB_PORT} -e "set global validate_password_policy='LOW';"
		mysql -h ${DB_IP} -u "$ROOT_DB_USER" -P ${DB_PORT} -e "GRANT create view, create temporary tables,create routine,alter routine,event, alter,create, delete, drop, execute, index, insert, references, select, update,trigger ON *.* TO $DB_USER@$node IDENTIFIED BY '$DB_PASS' WITH MAX_CONNECTIONS_PER_HOUR 9999; REVOKE GRANT OPTION ON *.* FROM $DB_USER@$node; REVOKE PROCESS ON *.* FROM $DB_USER@$node; REVOKE SHUTDOWN ON *.* FROM $DB_USER@$node; REVOKE SUPER ON *.* FROM $DB_USER@$node; REVOKE FILE ON *.* FROM $DB_USER@$node"
		mysql -h ${DB_IP} -u "$ROOT_DB_USER" -P ${DB_PORT} -e "GRANT create view, create temporary tables,create routine,alter routine,event, alter,create, delete, drop, execute, index, insert, references, select, update,trigger ON *.* TO $DB_USER@'172.17.%.%' IDENTIFIED BY '$DB_PASS' WITH MAX_CONNECTIONS_PER_HOUR 9999; REVOKE GRANT OPTION ON *.* FROM $DB_USER@'172.17.%.%'; REVOKE PROCESS ON *.* FROM $DB_USER@'172.17.%.%'; REVOKE SHUTDOWN ON *.* FROM $DB_USER@'172.17.%.%'; REVOKE SUPER ON *.* FROM $DB_USER@'172.17.%.%'; REVOKE FILE ON *.* FROM $DB_USER@'172.17.%.%'"
	done
	read -p "Current mysql password for root user [ $ROOT_DB_USER ] is $ROOT_DB_PASS. Do you want to reset ? Press Y/N " Ack
	if [ "$Ack" = 'Y' ] || [ "$Ack" = 'y' ]; then
		stty -echo
		read -p "Enter the new password for mysql root user :" mypwd
		stty echo
		mysql -h ${DB_IP} -u "$ROOT_DB_USER" -P ${DB_PORT} -e "set password=password('$mypwd');"
		logit "Mysql user $ROOT_DB_USER password is reset to $mypwd. Please delete this line from log files after verifying mysql login"
	fi
	echo "Removing mysql history file"
	logit "Removing mysql history file"
	rm ~/.mysql_history
	ln -s /dev/null ~/.mysql_history
}

fetch_db_details()
{
	if [ $# -eq 1 ]; then
		read -p "Enter MySql root equivalent user name: "  ROOT_DB_USER
		stty -echo
		read -p "Enter MySql password [ for $ROOT_DB_USER ]: "  ROOT_DB_PASS
		stty echo
	else
		ROOT_DB_USER='dbadmin'
		ROOT_DB_PASS='root@123'
	fi
	echo ""
	DB_USER=`grep -w "DB_USER" ${BASEDIR}/conf/generalConf.py | awk -F= '{print $2}' | sed "s/'//g;s/ //g"`
	DB_PORT=`grep -w "DB_PORT" ${BASEDIR}/conf/extendedConf.py | awk -F= '{print $2}' | sed "s/'//g;s/ //g"`
  	echo "Please enter the passwords for MySql User"
	stty -echo
	read -p "Enter MySql password [ for $DB_USER ]: "  DB_PASS
	stty echo
}

copy_sampleNomadJobFile()
{
	cd ${SER_INSTALL_PATH}/nomadFile/
	myhost=$(hostname)
	sed -i "s/server.in.nomad/$myhost/" *
	echo "Copying sample nomad files for future service creation"
	logit "Copying sample nomad files for future service creation"
	var1="/opt/appnomic"
	cd ${BASEDIR}/binary/sampleNomadJobFile
 	sed  -i "s|$var1|$SER_INSTALL_PATH|g" *
	cd ..
	cp -R sampleNomadJobFile "${SER_INSTALL_PATH}/nomadFile/"
	
	for node in `echo $NODES`
	do
		if [ "$node" = "$LOCAL_NODE" ]; then
			continue
		else
			ssh -o ConnectTimeout=200 ${USER}@${node} "mkdir -p ${SER_INSTALL_PATH}/nomadFile"
			scp -r "${SER_INSTALL_PATH}/nomadFile/" ${USER}@${node}:"${SER_INSTALL_PATH}/" 2>/dev/null  1>/dev/null
		fi
	done
}

check_user()
{
	if [ "$EUID" -ne 0 ]; then
		echo "Please run the script as root. Aborting.."
		logit "Please run the script as root. Aborting.."
		exit
	else
		USER='root'
	fi
}

configure_serv_ui()
{
	echo "Configuring services to start from bin script"
	logit "Configuring services to start from bin script"
	cd "${SER_INSTALL_PATH}/nomadFile/"
	nomad_port=$(grep ^HTTP_PORT  $BASEDIR/conf/extendedConf.py | grep -v "RANGE\|^#" | cut -d = -f2 | sed -e "s/'//g;s/ //g" )
	tomcat_port=$(grep ^TOMCAT_PORT $BASEDIR/conf/extendedConf.py | grep -v "RANGE\|^#" | cut -d = -f2 | sed -e "s/'//g;s/ //g" )
	sed -i "s/9191/$tomcat_port/g" "${SER_INSTALL_PATH}/Appsone_Service/data/tomcat/server.xml"
	echo "NOMAD_JOB_DIR=\"${SER_INSTALL_PATH}/nomadFile\"" >> "${SER_INSTALL_PATH}/Appsone_Service/bin/conf/Config.sh"
	echo "NOMAD_PORT=$nomad_port" >> "${SER_INSTALL_PATH}/Appsone_Service/bin/conf/Config.sh"
	echo "NOMAD_HOST=\"localhost\"" >> "${SER_INSTALL_PATH}/Appsone_Service/bin/conf/Config.sh"

	for file in `ls| grep "\.nomad"`
	do 
		nmd=$(grep "^job" $file | awk '{print $2}' | sed -e 's/"//g')
		nmd2=$(grep "^job" $file | awk '{print $2}' | sed -e 's/"//g;s/\./_/g;s/-/_/g')
		echo $nmd | grep -q "percona\|tomcat\|keycloak\|saboo"
		if [ $? -eq 0 ]; then
			echo $nmd | grep -q "saboo"
			if [ $? -ne 0 ]; then
				echo "A1_${nmd2}=$nmd,$file" >> "${SER_INSTALL_PATH}/Appsone_Service/bin/conf/Config.sh"
			fi
			continue
		fi
		echo "A1_${nmd2}=$nmd,$file" >> "${SER_INSTALL_PATH}/Appsone_Service/data/tomcat/bin-nomad/conf/Config.sh"
		echo "A1_${nmd2}=$nmd,$file" >> "${SER_INSTALL_PATH}/Appsone_Service/bin/conf/Config.sh"
	done
	for node in `echo $NODES`
	do
		if [ "$node" = "$LOCAL_NODE" ]; then
			continue
		else
			scp  "${SER_INSTALL_PATH}/Appsone_Service/data/tomcat/bin-nomad/conf/Config.sh" ${USER}@${node}:"${SER_INSTALL_PATH}/Appsone_Service/data/tomcat/bin-nomad/conf/Config.sh" 2>/dev/null  1>/dev/null
			scp  "${SER_INSTALL_PATH}/Appsone_Service/data/tomcat/server.xml" ${USER}@${node}:"${SER_INSTALL_PATH}/Appsone_Service/data/tomcat/server.xml" 2>/dev/null  1>/dev/null
			scp  "${SER_INSTALL_PATH}/Appsone_Service/bin/conf/Config.sh" ${USER}@${node}:"${SER_INSTALL_PATH}/Appsone_Service/bin/conf/Config.sh" 2>/dev/null  1>/dev/null
		fi
	done
}

enable_not_port()
{
	str=""
	for node in `echo $NODES`
        do
                if [ "$node" = "$LOCAL_NODE" ]; then
			continue
                else
			str="$str $node"
                fi
        done
	${BASEDIR}/lib/EnableDisablePortFirewall.sh "$str" "enable" ${BASEDIR}/JobsConfigs/.open_port_sort.txt > /dev/null 2>&1
}

httpd_format() 
{
	echo "Migrating existing httpd configuration from $conf_file to newly installed haproxy"
	cd ${BASEDIR}/JobsConfigs
	LineNum=$(grep -n "^<Proxy balancer.*" "$conf_file" | cut -d : -f1 | tr '\n' ' ')
	Var=($(grep "^<Proxy balancer.*" "$conf_file"  | awk -F '//' '{print $2}' | sed -e 's/>//g' | tr '\n' ' '))
	counter=0
	counter1=0
	for id in ${LineNum[@]}
	do
		counter=0
		variable=${Var[$counter1]}
		sed -n "$id,$ p" "$conf_file" | while read line
		do
			echo $line | grep -q "Proxy"
			if [ $? -eq 0 -a $counter -eq 1 ]; then
				break;
			fi
			echo $line | grep -v "#" | grep -q "BalancerMember"
			if [ $? -eq 0 ]; then
				extract=$(echo "$line" | awk -F '//' '{print $2}')
				name=$(echo "$extract" | sed -e 's/\:/_/g')
				#echo "Server $name $extract" ssl verify none
			
				echo "$variable" | grep -i 'txnCollector' 1>/dev/null 2>/dev/null
				if [ $? -eq 0 ]; then
					echo "server $name $extract" ssl verify none >> txnCollector.txt
				fi
				echo "$variable" | grep -i 'collectionService' 1>/dev/null 2>/dev/null
				if [ $? -eq 0 ]; then
					echo "server $name $extract" ssl verify none >> collectionService.txt
				fi
				echo "$variable" | grep -i 'etl' 1>/dev/null 2>/dev/null
				if [ $? -eq 0 ]; then
					echo "server $name $extract" ssl verify none >> etl.txt
				fi
				
			fi
			counter=1
		done
		((counter1++))
	done

	if [ -f txnCollector.txt ]; then
		sed -i "s/<var1>/txnCollector/" httpdToHA.conf
		to_add_next=`grep -nw 'backend txnCollector' httpdToHA.conf | cut -d':' -f1`
		to_add_next=`expr $to_add_next + 2`
		while read line
		do
			sed -i "$to_add_next a $line" httpdToHA.conf 
		done < txnCollector.txt
	else
		sed -i '/frontend txn_old/,/default_backend <var1>/d' httpdToHA.conf
		LINE_1=`grep -n 'backend <var2>' httpdToHA.conf | cut -d':' -f1`
		sed -i "$LINE_1 d" httpdToHA.conf ; sed -i "$LINE_1 d" httpdToHA.conf
	fi
	
	if [ -f collectionService.txt ]; then
		sed -i "s/<var2>/collectionService/" httpdToHA.conf
		to_add_next=`grep -nw 'backend collectionService' httpdToHA.conf | cut -d':' -f1`
		to_add_next=`expr $to_add_next + 2`
		while read line
		do
			sed -i "$to_add_next a $line" httpdToHA.conf 
		done < collectionService.txt
	else
		sed -i '/frontend collection_old/,/default_backend <var2>/d' httpdToHA.conf
		LINE_1=`grep -n 'backend <var2>' httpdToHA.conf | cut -d':' -f1`
		sed -i "$LINE_1 d" httpdToHA.conf ; sed -i "$LINE_1 d" httpdToHA.conf
	fi
	
	if [ -f etl.txt ]; then
		sed -i "s/<var3>/collectionService/" httpdToHA.conf
		to_add_next=`grep -nw 'backend etl' httpdToHA.conf | cut -d':' -f1`
		to_add_next=`expr $to_add_next + 2`
		while read line
		do
			sed -i "$to_add_next a $line" httpdToHA.conf 
		done < etl.txt
	else
		sed -i '/frontend etl_old/,/default_backend <var3>/d' httpdToHA.conf
		LINE_1=`grep -n 'backend <var3>' httpdToHA.conf | cut -d':' -f1`
		sed -i "$LINE_1 d" httpdToHA.conf ; sed -i "$LINE_1 d" httpdToHA.conf
	fi

	rm -f etl.txt collectionService.txt txnCollector.txt 2>/dev/null
}

haproxy_mig()
{
	echo "Migrating existing haproxy configuration from $conf_file to newly installed haproxy"
	load_bal_type=`cat ${CONFDIR}/migrationConf.py | grep load_bal_type | cut -d'=' -f2 | tr -d '"' | tr -d ' '`
	conf_file=`cat ${CONFDIR}/migrationConf.py | grep conf_file | cut -d'=' -f2 | tr -d '"' | tr -d ' '`
	if [ "$load_bal_type" = "HAPROXY" ] && [ ! -z "$conf_file" ]; then
		cp $conf_file /tmp/.haproxy.cfg
		grep -n bind /tmp/.haproxy.cfg | cut -d : -f1 | while read line
                do
                        pline=$((line - 1))
                        port=$(sed -n "${line}p" /tmp/.haproxy.cfg | cut -d : -f2 | awk '{print $1}')
                        sed -i "${line}d" /tmp/.haproxy.cfg
                        sed -i "${pline}a bind *:$port ssl crt /etc/ssl/certs/haproxy.pem"  /tmp/.haproxy.cfg
                done
		cat /tmp/.haproxy.cfg >> ${BASEDIR}/JobsConfigs/haproxy.cfg
		rm -f /tmp/.haproxy.cfg
	elif [ "$load_bal_type" = "HTTPD" ] && [ ! -z "${BASEDIR}/JobsConfigs/httpdToHA.conf" ]; then
		httpd_format
		cat ${BASEDIR}/JobsConfigs/httpdToHA.conf >> ${BASEDIR}/JobsConfigs/haproxy.cfg
	else	
		echo "Please verify the details in migrationConf.py and the availabilty of config files"
	fi
}

checkTomcatKeycloak() {
	KEYCLOAK_SSL_CERT_FILE=$(grep -w KEYCLOAK_SSL_CERT_PATH $CONFDIR/generalConf.py | cut -d = -f2 | sed -e "s/\"//g;s/'//g;s/ //g")
	TOMCAT_LICENSE_FILE=$(grep -w TOMCAT_LICENSE_PATH $CONFDIR/generalConf.py | cut -d = -f2 | sed -e "s/\"//g;s/'//g;s/ //g")
	KEYCLOAK_SSL=$(grep -w KEYCLOAK_SSL $CONFDIR/generalConf.py | cut -d = -f2 | sed -e "s/\"//g;s/'//g;s/ //g" | tr '[:lower:]' '[:upper:]')
	if [ "$KEYCLOAK_SSL" == "YES" ]; then
		if [ ! -f "$KEYCLOAK_SSL_CERT_FILE" ]; then
			echo "Keycloak certificate file(.cer) file missing. Please copy the certificate file and rerun the installer"
			logit "Keycloak certificate file(.cer) file missing. Please copy the certificate file and rerun the installer"
			return 1
		fi
	fi
	if [ ! -f "$TOMCAT_LICENSE_FILE" ]; then
		echo "Tomcat license file(.lic) file missing. Please copy the certificate file and rerun the installer"
		logit "Tomcat license file(.lic) file missing. Please copy the certificate file and rerun the installer"
		return 1
	fi
	
	return 0
}

main()
{
	check_user
	unalias cp rm mv 2>/dev/null
	BASEDIR=`pwd`
	CONFDIR="${BASEDIR}/conf"
	PYTHONDEPSDIR="${BASEDIR}/conf/pythondeps"
	MIGRATION_STATUS=$(grep MIGRATION $CONFDIR/generalConf.py | cut -d = -f2 | sed -e "s/\"//g;s/'//g;s/ //g" | tr '[:lower:]' '[:upper:]')
	mkdir -p "${BASEDIR}/logs"
	LOG_FILE="${BASEDIR}/logs/${0}.log"
	
	# Check tomcat license file
	checkTomcatKeycloak
	if [ $? -ne 0 ]; then
		exit 1
	fi
	## Create the config file. Include the config file
	echo "Loading the configuration file"
	logit "Loading the configuration file"
	loadConfig
	if [ "$MIGRATION_STATUS" == "TRUE" ]; then
		logit "Migration is $MIGRATION_STATUS. Installer will perform migration steps"
		db_data_file=$(grep db_data_file $CONFDIR/migrationConf.py | cut -d = -f2 | sed -e 's/"//g')
		conf_file=$(grep conf_file $CONFDIR/migrationConf.py | cut -d = -f2 | sed -e 's/"//g')
		if [ ! -f "$db_data_file" ]; then
			echo "Please provide older [4.5/4.6 converted to percona] db backup file path in db_data_file variable in $CONFDIR/migrationConf.py and rerun installer"
			logit "Please provide older [4.5/4.6 converted to percona] db backup file path in db_data_file variable in $CONFDIR/migrationConf.py, rerun installer"
			exit 0
		fi
		if [ ! -f "$conf_file" ]; then
			echo "Please configure older load balancer config file in conf_file variable in $CONFDIR/migrationConf.py and rerun installer"
			logit "Please configure older load balancer config file in conf_file variable in $CONFDIR/migrationConf.py and rerun installer"
			exit 0
		fi
	else
		logit "Migration is $MIGRATION_STATUS. Installer will perform installation steps"
	fi
	NO_OF_NODES=`echo $NODES |awk -F' ' '{print NF}'`
	if [ -f ${BASEDIR}/.installationstatus ]; then
		logit "Installation process was aborted in the middle. Continuing from last executed step."
		echo "Installation process was aborted in the middle. Continuing from last executed step."
	else
		echo "Starting installation"
		touch ${BASEDIR}/.installationstatus
		> ${BASEDIR}/.installationstatus
	fi

	grep -q "install_Services" ${BASEDIR}/.installationstatus
	if [ $? -eq 0 ]; then
		logit "All the appsone services are installed on all nodes. Proceeding to next step"
	else
		echo "Installing appsone services in all the nodes"
		logit "Installing appsone services in all the nodes"
		install_Services
		echo "install_Services" >> ${BASEDIR}/.installationstatus
	fi

	grep -q "install_RPM" ${BASEDIR}/.installationstatus
	if [ $? -eq 0 ]; then
		logit "All required services (consul,docker,nomad) are installed in all the nodes. Proceeding to next step"
	else
		echo "Installing required services in all the nodes"
		logit "Installing required services in all the nodes"
		install_RPM
		echo "install_RPM" >> ${BASEDIR}/.installationstatus
	fi

	grep -q "copy_Infra" ${BASEDIR}/.installationstatus
	if [ $? -eq 0 ]; then
		logit "Infta services are available in install path. Proceeding to next step"
	else
		echo "Copying the infra service in $SER_INSTALL_PATH"
		logit "Copying the infra service in $SER_INSTALL_PATH"
		copy_Infra
		echo "copy_Infra" >> ${BASEDIR}/.installationstatus
	fi

	##Copying the percona data directory
	grep -q "CopyPerconaDir" ${BASEDIR}/.installationstatus
	if [ $? -ne 0 ]; then
		if [ "$MIGRATION_STATUS" == "TRUE" ]; then
			echo "Copying backup mysql directory in the local node"
			logit "Copying backup mysql directory in the local node"
			rm -rf ${SER_INSTALL_PATH}/Appsone_Service/data/percona 2>/dev/null
			tar -zxf "$db_data_file" -C ${SER_INSTALL_PATH}/Appsone_Service/data/ 2>/dev/null
			if [ $? -ne 0 ]; then
				tar -xf "$db_data_file" -C ${SER_INSTALL_PATH}/Appsone_Service/data/ 2>/dev/null
				if [ $? -ne 0 ]; then
					echo "Not able to extract DB data directoy.Please check and run the installer again"
					logit "Not able to extract DB data directoy.Please check and run the installer again"
					exit 1
				fi
			fi
			tarName=$(echo $db_data_file | awk -F/ '{print $NF}')
			fileName=$(echo $tarName | sed -e "s/.tar.*//")
			if [ "$fileName" != "percona" ]; then
				mv ${SER_INSTALL_PATH}/Appsone_Service/data/${fileName} ${SER_INSTALL_PATH}/Appsone_Service/data/percona
				if [ $? -ne 0 ]; then
					echo "The extracted folder name and the tar name doesn't match"
					logit "The extracted folder name and the tar name doesn't match"
					exit 1
				fi
			else
				echo "The extracted folder name is percona"
				logit "The extracted folder name is percona"
			fi
		chown -R ${NOMAD_USER}:${NOMAD_USER} ${SER_INSTALL_PATH}/Appsone_Service/data/percona/ 
		echo "CopyPerconaDir" >> ${BASEDIR}/.installationstatus
		fi
	fi

	##Create the .json files for consul and .hcl files for nomad.
	grep -q "genConsulNomadRanConfigs" ${BASEDIR}/.installationstatus
	if [ $? -eq 0 ]; then
		logit "Consul and nomad config files are generated. Proceeding to next step"
	else
		echo "Generating consul,ran and nomad config files for clustering"
		logit "Generating consul,ran and nomad config files for clustering"
		genConsulNomadRanConfigs
		echo "genConsulNomadRanConfigs" >> ${BASEDIR}/.installationstatus
	fi
	
	# Once created, copy the config files to pre-defined paths on all nodes and start the services on all nodes in the required order.
	grep -q "copyConfigsStartServices" ${BASEDIR}/.installationstatus
	if [ $? -eq 0 ]; then
		logit "Starting (consul,ran,docker,nomad). Proceeding to next step"
	else
		echo "Starting the services (consul,ran,docker,nomad) in all the nodes"
		logit "Starting the services (consul,ran,docker,nomad) in all the nodes"
		copyConfigsStartServices
		echo "copyConfigsStartServices" >> ${BASEDIR}/.installationstatus
	fi

	## Check if cluster members were created for consul properly.
	grep -q "check_cluster_members consul" ${BASEDIR}/.installationstatus
	if [ $? -eq 0 ]; then
		logit "Consul cluster is already formed. Proceeding to next step"
	else
		echo "Checking if the consul cluster is formed or not"
		logit "Checking if the consul cluster is formed or not"
		check_cluster_members 'consul'
		if [ $? -eq 10 ]; then
			read -p "consul cluster could not be created. Do you want to retry ?	Press Y/N and hit Enter"  Ack
			if [ "$Ack" = 'Y' ] || [ "$Ack" = 'y' ]; then
				check_cluster_members 'consul' 'Ack'
			else
				echo "Aborting.."
				exit 1
			fi
		fi
		echo "check_cluster_members consul" >> ${BASEDIR}/.installationstatus
	fi

	##  Check if cluster members were created for nomad properly.
	grep -q "check_cluster_members nomad" ${BASEDIR}/.installationstatus
	if [ $? -eq 0 ]; then
		logit "Nomad cluster is already formed. Proceeding to next step"
	else
		echo "Checking if the nomad cluster is formed or not"
		logit "Checking if the nomad cluster is formed or not"
		check_cluster_members 'nomad'
		if [ $? -eq 10 ]; then
			read -p "nomad cluster could not be created. Do you want to retry ?	Press Y/N and hit Enter"  Ack
			if [ "$Ack" = 'Y' ] || [ "$Ack" = 'y' ]; then
				check_cluster_members 'nomad' 'Ack'			
			else
				echo "Aborting.."
				exit 1
			fi
		fi
		echo "check_cluster_members nomad" >> ${BASEDIR}/.installationstatus
	fi
	
	if [ "$MIGRATION_STATUS" == "TRUE" ]; then
		logit "Fetch existing mysql user and configure the same"
		fetch_db_details "Mig"
	else
		logit "Creating Mysql user and provide permission"	
		fetch_db_details
	fi

	KC_PASS="admin@123"
	logit "Putting encrypted database and keycloak password in consul"
	put_enc_pass "$DB_PASS" "$KC_PASS"
	
	## If server goes down and comes back up, the services are auto started.
	grep -q "start_service_reboot" ${BASEDIR}/.installationstatus
	if [ $? -eq 0 ]; then
		logit "Service autostart is enabled. Proceeding to next step"
	else
		echo "Enabling service auto restart after machine reboot."
		logit "Enabling service auto restart after machine reboot."
		start_service_reboot
		echo "start_service_reboot" >> ${BASEDIR}/.installationstatus
	fi
	
	grep -q "callNomadTpl" ${BASEDIR}/.installationstatus
	if [ $? -eq 0 ]; then
		logit "All nomad files are generated and percona database is running. Proceeding to next step"
	else
		echo "Generating all the nomad files starting percona database"
		logit "Generating all the nomad files starting percona database"
		callNomadTpl
		echo "callNomadTpl" >> ${BASEDIR}/.installationstatus
	fi
		
	echo "Setting mysql privileges"	
	logit "Setting mysql privileges"	
	if [ "$MIGRATION_STATUS" == "TRUE" ]; then
		logit "Migrating older database and loadbalancer configuration"
		haproxy_mig
	else
		logit "Mysql password reset section.."	
		echo ""
	fi
	db_pri_pwd
	
	grep -q "copy_tpls" ${BASEDIR}/.installationstatus
	if [ $? -eq 0 ]; then
		logit "Config templates are already present. Proceeding to next step"
	else 	
		logit "Copying config templates on all nodes"
		echo "Copying config templates on all nodes"
		generate_keycloak_cer
		copy_tpls
		enable_not_port
		echo "copy_tpls" >> ${BASEDIR}/.installationstatus
	fi

	grep -q "copy_sampleNomadJobFile" ${BASEDIR}/.installationstatus
	if [ $? -eq 0 ]; then
		logit "Sample nomad job files are already present. Proceeding to next step"
	else
		echo "Copying sample nomad job files  on all nodes"
		logit "Copying sample nomad job files  on all nodes"
		copy_sampleNomadJobFile
		echo "copy_sampleNomadJobFile" >> ${BASEDIR}/.installationstatus
	fi

	grep -q "configure_serv_ui" ${BASEDIR}/.installationstatus
	if [ $? -eq 0 ]; then
		logit "All services are configured . Proceeding to next step"
	else
		echo "Configuring all services except percona/tomcat for service management from Appsone UI"
		logit "Configuring all services except percona/tomcat for service management from Appsone UI"
		configure_serv_ui
		echo "configure_serv_ui" >> ${BASEDIR}/.installationstatus
	fi
}

BASEDIR=`pwd`
nomad_port=$(grep ^HTTP_PORT  $BASEDIR/conf/extendedConf.py | grep -v "RANGE\|^#" | cut -d = -f2 | sed -e "s/'//g;s/ //g" )
export NOMAD_CACERT="/etc/nomad/nomad-ca.pem"
export NOMAD_CLIENT_KEY="/etc/nomad/cli-key.pem"
export NOMAD_CLIENT_CERT="/etc/nomad/cli.pem"
export NOMAD_ADDR="https://localhost:$nomad_port"

main
echo "Installation completed Successfully.."
logit "Installation completed Successfully.."
rm -f ${BASEDIR}/.installationstatus
exit 0
## End of script
