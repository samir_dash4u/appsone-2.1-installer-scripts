#!/bin/bash

usage()
{
	echo "To view the list of services that can be started
	$0 -l
To run all the infra services
	$0 -i ALL
To run individual the infra service:
	$0 -i <service_name>
To run all the A1 services:
	$0 -a ALL
To run individual the A1 service:
	$0 -a <service_name>
TOMCAT and Capacity Planning should be started after KEYCLOAK is up. COLLATINGSERVICE must be started before TOMCAT"
	exit 0
}

logit()
{
        datestamp=$(date +"%d-%m-%Y:%H:%M:%S");
        echo "$datestamp : $* " >> $LOG_FILE
        return 0;
}

cassandra_uploadfile()
{
	LOCAL_NODE=`grep "LOCAL_NODE" "${BASEDIR}/conf/extendedConf.py" | awk -F"=" '{print $2}' | sed "s/'//g;s/ //g;s/\"//g"`
	id=`nomad status cassandra | grep "cassandra-node-1" | grep "running" | awk '{print $1}'`
	if [ ! -z "$id" ]; then
		IP_node1=`nomad status ${id} | grep gossip | awk -F: '{print $2}' | sed 's/ //g'`
		if [ "$IP_node1" = "$LOCAL_NODE" ]; then
			bash ${BASEDIR}/lib/cassandra.sh "$BASEDIR/lib"
		else
			scp $BASEDIR/lib/cassandra.cql root@${IP_node1}:/tmp >/dev/null  2>/dev/null
			ssh root@${IP_node1} "bash " < "${BASEDIR}/lib/cassandra.sh"
		fi
	fi
}

check_service_called()
{
	service_=$1
	service_1=$2
	allc=$3
	nomad status | grep "$service_" | grep 'running' 1>/dev/null 2>/dev/null
	if [ $? -ne 0 ]; then
		echo "$service_ is a dependecy for $service_1. starting the $service_."
		nomad run "${SER_INSTALL_PATH}/nomadFile/${service_}.nomad"
		sleep 120
		check_service_up "$service_" "$allc"
		if [ $? -ne 0 ]; then
			echo "Check status of $service_ manually and start $service_1 after $service_ starts successfully."
			return 2
		fi
	fi
	
	return 0
}


check_service_up()
{
	service_=$1
	allocations=$2
	final="$3"
	counter=5
	
	if [ $allocations = 1 ]; then
		while [ ${counter} -gt 0 ]; do
			nomad status "$service_" | sed -n '/Allocations/{n;n;p}' | grep -v 'stop.*complete' | grep -v 'lost' |grep 'running' 1>/dev/null 2>/dev/null
			if [ $? -eq 0 ]; then
            		echo "$service_ started successfully."
					logit "$service_ started successfully."
				return 0
			else            
		        counter=$((counter - 1))
				if [ ${counter} -eq 1 ] && [ -z "$final" ]; then
					echo "$service_ is not up yet. Still waiting for running-status."
					logit "$service_ is not up yet. Still waiting for running-status. User is prompted for re-check."
					read -p "Do you want to wait for the status?	Press Y to wait or N to move on and hit Enter:	"  Ack
					if [ "$Ack" = 'Y' ] || [ "$Ack" = 'y' ]; then
						return 10
					else
						return 1
					fi
				fi
				sleep 30
                		continue
            		fi
        	done
	
	elif [ $allocations = 2 ]; then
		while [ ${counter} -gt 0 ];do
			item_run=`nomad status $service_ | sed -n '/Allocations/{n; n;p; n;p}' | grep -v 'stop.*complete' | grep -v 'lost' | grep -c 'running'`
			item_pend=`nomad status $service_ | sed -n '/Allocations/{n; n;p; n;p}' |grep -v 'stop.*complete' | grep -v 'lost' | grep -c 'pending'`
			ser_val=$((item_run + item_pend))
			if [ $item_run -eq 2 ]; then
				echo "$service_ started successfully."
				logit "$service_ started successfully."
				return 0
			else
				counter=$((counter - 1))
				echo "Waiting till all instance of $service_ are started."
				if [ ${counter} -eq 1 ] && [ -z "$final" ]; then
					echo "$service_ is not up yet. Still waiting for running-status."
					logit "$service_ is not up yet. Still waiting for running-status. User is prompted for re-check."
					read -p "Do you want to wait for the status?	Press Y to wait or N to move on and hit Enter:	"  Ack
					if [ "$Ack" = 'Y' ] || [ "$Ack" = 'y' ]; then
						return 10
					else
						echo "$service_ is not up yet. Keep checking the status manually."
						logit "$service_ is not up yet. Keep checking the status manually."
						return 1
					fi
				fi
				sleep 60
                		continue
			
			fi
		done
	
	elif [ $allocations = 3 ]; then
		while [ ${counter} -gt 0 ];do
			item_run=`nomad status $service_ | sed -n '/Allocations/{n; n;p; n;p; n;p}' |grep -v 'stop.*complete' | grep -v 'lost' | grep -c 'running'`
			item_pend=`nomad status $service_ | sed -n '/Allocations/{n; n;p; n;p; n;p}' |grep -v 'stop.*complete' | grep -v 'lost' | grep -c 'pending'`
			ser_val=$((item_run + item_pend))
			if [ $item_run -eq 3 ]; then
				echo "$service_ started successfully."
				logit "$service_ started successfully."
				return 0
			else
				echo "Waiting till all instance of $service_ are started."
				counter=$((counter - 1))
				if [ ${counter} -eq 1 ] && [ -z "$final" ]; then
					echo "$service_ is not up yet. Still waiting for running-status."
					logit "$service_ is not up yet. Still waiting for running-status. User is prompted for re-check."
					read -p "Do you want to wait for the status?	Press Y to wait or N to move on and hit Enter:	"  Ack
					if [ "$Ack" = 'Y' ] || [ "$Ack" = 'y' ]; then
						return 10
					else
						echo "$service_ is not up yet. Keep checking the status manually."
						logit "$service_ is not up yet. Keep checking the status manually."
						return 1
					fi
				fi
				sleep 60
		                continue	
			fi
		done
	
	elif [ $allocations = 4 ]; then
		while [ ${counter} -gt 0 ];do
			item_run=`nomad status $service_ | sed -n '/Allocations/{n; n;p; n;p; n;p; n;p}' |grep -v 'stop.*complete' | grep -v 'lost' | grep -c 'running'`
			item_pend=`nomad status $service_ | sed -n '/Allocations/{n; n;p; n;p; n;p; n;p}' |grep -v 'stop.*complete' | grep -v 'lost' | grep -c 'pending'`
			ser_val=$((item_run + item_pend))
			if [ $item_run -eq 4 ]; then
				echo "$service_ started successfully."
				logit "$service_ started successfully."
				return 0
			else	
				echo "Waiting till all instance of $service_ are started."
				counter=$((counter - 1))
				if [ ${counter} -eq 1 ] && [ -z "$final" ]; then
					echo "$service_ is not up yet. Still waiting for running-status."
					logit "$service_ is not up yet. Still waiting for running-status. User is prompted for re-check."
					read -p "Do you want to wait for the status?	Press Y to wait or N to move on and hit Enter:	"  Ack
					if [ "$Ack" = 'Y' ] || [ "$Ack" = 'y' ]; then
						return 10
					else
						echo "$service_ is not up yet. Keep checking the status manually."
						logit "$service_ is not up yet. Keep checking the status manually."
						return 1
					fi
				fi
				sleep 180
	        		continue
			fi
		done
	fi
	return 0
}

recheck_services_up()
{
	
	service_=$1
	allocations=$2
	return_val=$3
	if [ ${return_val} -eq 10 ]; then
		check_service_up ${service_} ${allocations} 'FINAL'
		if [ $? -eq 10 ]; then
			echo "$service_ could not be started. Please check manually"
			logit "$service_ could not be started. Please check manually"
		elif [ "$service_" = "cassandra" ];then
			cassandra_uploadfile
		fi
	fi

}


upload_jar()
{	
	FLINK_IP_1=`grep -w value ${SER_INSTALL_PATH}/nomadFile/flink.nomad | cut -d'=' -f2 | tr -d '"'| tr -d ' '| head -1`
	FLINK_IP_2=`grep -w value ${SER_INSTALL_PATH}/nomadFile/flink.nomad | cut -d'=' -f2 | tr -d '"'| tr -d ' '| head -2|tail -1`
	FLINK_PORT=`grep 'WEB_PORT' "${BASEDIR}/conf/extendedConf.py" |awk -F'=' '{print $2}'|tr -d "'"|tr -d ' '`
	
	curl -s -o flink_ui.test http://${FLINK_IP_1}:${FLINK_PORT}
	FLINK_IP=${FLINK_IP_1}
	if [ ! -f flink_ui.test ]; then
		curl -s -o flink_ui.test http://${FLINK_IP_2}:${FLINK_PORT}
		FLINK_IP=${FLINK_IP_2}
	fi
	if [ ! -f flink_ui.test ]; then
		echo "Both the Flink URLs are not available. Upload the jars manually."
		logit "Both the Flink URLs are not available. Upload the jars manually."
		return 1
	fi
	rm -f flink_ui.test 2>/dev/null
	##Check if any jar already available:
	curl -s -o flink_ui.json http://${FLINK_IP}:${FLINK_PORT}/jars/ 
	id_num=`cat ${SER_INSTALL_PATH}/nomadFile/flink_ui.json | jq '.files[] .id'|wc -l`
	
	##Upload jars	
	tfp_pip_ver=`cat ${BASEDIR}/conf/version.py | grep tfp_pipeline | awk -F= '{print $2}' | tr -d '"'`
	curl -X POST -H "Expect:" -F "jarfile=@${SER_INSTALL_PATH}/Appsone_Service/tfp-pipeline/tfp-pipeline-${tfp_pip_ver}.jar" http://${FLINK_IP}:${FLINK_PORT}/jars/upload
	sleep 5
	curl -s -o ${SER_INSTALL_PATH}/nomadFile/flink_ui.json http://${FLINK_IP}:${FLINK_PORT}/jars/
	tfp_id=`cat ${SER_INSTALL_PATH}/nomadFile/flink_ui.json | jq ".files[${id_num}] .id"|tr -d '"'`
	curl -X POST "http://${FLINK_IP}:${FLINK_PORT}/jars/${tfp_id}/run?allowNonRestoredState=false&entry-class=DTFPProcessor&parallelism=&program-args=&savepointPath="
        curl -X POST "http://${FLINK_IP}:${FLINK_PORT}/jars/${tfp_id}/run?allowNonRestoredState=false&entry-class=AnomalyScanner&parallelism=&program-args=&savepointPath="

	id_num=$((id_num+1))
	pip_ver=`cat ${BASEDIR}/conf/version.py | grep -w pipeline | awk -F= '{print $2}' | tr -d '"'`
	curl -X POST -H "Expect:" -F "jarfile=@${SER_INSTALL_PATH}/Appsone_Service/pipeline/pipeline-${pip_ver}.jar" http://${FLINK_IP}:${FLINK_PORT}/jars/upload
	sleep 5
	curl -s -o ${SER_INSTALL_PATH}/nomadFile/flink_ui.json http://${FLINK_IP}:${FLINK_PORT}/jars/
	noarch_id=`cat ${SER_INSTALL_PATH}/nomadFile/flink_ui.json | jq ".files[${id_num}] .id"|tr -d '"'`
	curl -X POST "http://${FLINK_IP}:${FLINK_PORT}/jars/${noarch_id}/run?allowNonRestoredState=false&entry-class=KPIProcessor&parallelism=&program-args=&savepointPath="

	rm -f flink_ui.json 2>/dev/null
}

start_service_infra()
{
	if [ $# -eq 1 ] ;then
		infra=`echo "$1" | tr [a-z] [A-Z]`
		INFRA_SERVICES=( "$infra" )
	fi
	if [ ${#INFRA_SERVICES[*]} -gt 0 ]; then
		for item in "${INFRA_SERVICES[@]}"; do
			case "$item" in
				HAPROXY)
					echo "Starting haproxy service"
					logit "Starting haproxy service"
					nomad status | grep "haproxy" | grep 'running' 1>/dev/null 2>/dev/null
					if [ $? -ne 0 ]; then
						nomad run haproxy.nomad
						sleep 60
						check_service_up haproxy 1
						return_val=$?
						if [ $return_val -eq 10 ]; then
							recheck_services_up haproxy 1 $return_val
						fi
					else
						echo "Haproxy is already running"
					fi
					;;
				
				RABBITMQ)
					echo "Starting rabbitmq service"
					logit "Starting rabbitmq service"
					nomad status | grep "rabbitmq" | grep 'running' 1>/dev/null 2>/dev/null
					if [ $? -ne 0 ]; then
						nomad run rabbitmq.nomad
						sleep 60
						check_service_up rabbitmq 1 
						return_val=$?
						if [ $return_val -eq 10 ]; then
							recheck_services_up rabbitmq 1 $return_val
						fi
					else
						echo "Rabbitmq is already running"
					fi
					;;
				
				COUCHBASE)
					echo "Starting couchbase service"
					logit "Starting couchbase service"
					nomad status | grep "couchbase" | grep 'running' 1>/dev/null 2>/dev/null
					if [ $? -ne 0 ]; then
						nomad run couchbase.nomad
						sleep 60
						check_service_up couchbase 1
						return_val=$?
						if [ $return_val -eq 10 ]; then
							recheck_services_up couchbase 1 $return_val
						fi
					else
						echo "Couchbase is already running"
					fi
					;;
				CASSANDRA)
					echo "Starting cassandra service"
					logit "Starting cassandra service"
					nomad status | grep "cassandra" | grep 'running' 1>/dev/null 2>/dev/null
					if [ $? -ne 0 ]; then
						nomad run cassandra.nomad
						sleep 60
						check_service_up cassandra 3
						return_val=$?
						if [ $return_val -eq 10 ]; then
							recheck_services_up cassandra 3 $return_val
						else
							cassandra_uploadfile
						fi
					else
						echo "Cassandra is already running"
					fi					
					;;
				FLINK)
					fflag1=0
					fflag2=0
					fflag3=0
					check_service_called 'rabbitmq' 'flink' 1
					if [ $? -eq 0 ]; then
						fflag1=0
					elif [ $? -eq 2 ]; then
						echo "rabbitmq (dependency for flink ) could not be started. Not starting flink"
						fflag1=1
					fi
					
					check_service_called 'cassandra' 'flink' 3
					if [ $? -eq 0 ]; then
						fflag2=0
					elif [ $? -eq 2 ]; then
						fflag2=1
						echo "cassandra (dependency for flink) could not be started. Not starting flink"
					fi
					
					check_service_called 'couchbase' 'flink' 1
					if [ $? -eq 0 ]; then
						fflag3=0
					elif [ $? -eq 2 ]; then
						fflag3=1
						echo "couchbase (dependency for flink) could not be started. Not starting flink"
					fi
					
					echo "Starting zookeeper service"
					logit "Starting zookeeper service"
					nomad status | grep "zookeeper" | grep 'running' 1>/dev/null 2>/dev/null
					if [ $? -ne 0 ]; then
						nomad run zookeeper.nomad
						sleep 60
						check_service_up zookeeper 1
						return_val=$?
						if [ $return_val -eq 10 ]; then
							recheck_services_up zookeeper 1 $return_val
						fi
					else
						echo "Zookeeper is already running"
					fi
					nomad status | grep "hadoop" | grep 'running' 1>/dev/null 2>/dev/null
					if [ $? -ne 0 ]; then
						echo "Starting hadoop service"
						logit "Starting hadoop service"
						nomad run hadoop.nomad
						sleep 200
						check_service_up hadoop 2
						return_val=$?
						if [ $return_val -eq 10 ]; then
							recheck_services_up hadoop 2 $return_val
						fi
					else
						echo "Hadoop is already running"
					fi
					if [ $fflag1 -eq 0 -a $fflag2 -eq 0 -a $fflag3 -eq 0 ]; then
						nomad status | grep "flink" | grep 'running' 1>/dev/null 2>/dev/null
						if [ $? -ne 0 ]; then
							echo "Starting flink service"
							logit "Starting flink service"
							nomad run flink.nomad
							sleep 200
							check_service_up flink 4
							return_val=$?
							if [ $return_val -eq 10 ]; then
								recheck_services_up flink 4 $return_val
								if [ $? -eq 0 ]; then
									upload_jar
								fi
							elif [ $return_val -eq 0 ]; then
								upload_jar
							fi
						else
							echo "Flink is already running"
						fi
					fi
					cd "${SER_INSTALL_PATH}/nomadFile/"
					;;
				
				CAPACITYPLANNING)
					check_service_called 'keycloak' 'capacityPlanning' 1
					if [ $? -eq 0 ]; then
						fflag=0
					elif [ $? -eq 2 ]; then
						fflag=0
						echo "keycloak (dependency for tomcat) could not be started. Not starting Capacity planning"
					fi
				
					if [ $fflag -eq 0 ]; then	
						nomad status | grep "capacity-planning" | grep 'running' 1>/dev/null 2>/dev/null
						if [ $? -ne 0 ]; then
							echo "Starting capacity planning service"
							nomad run capacityPlanning.nomad
							sleep 90
							check_service_up capacity-planning 1
							return_val=$?
							if [ $return_val -eq 10 ]; then
								recheck_services_up capacity-planning 1 $return_val
							fi
						fi
					else
						echo "Capacity is already running"
					fi
					;;
					
				TOMCAT)
					check_service_called 'keycloak' 'TOMCAT' 1
					if [ $? -eq 0 ]; then
						fflag=0
					elif [ $? -eq 2 ]; then
						fflag=1
						echo "keycloak (dependency for tomcat) could not be started. Not starting tomcat"
					fi
					
					nomad status | grep "COLLATINGSERVICE" 1>/dev/null 2>/dev/null
					if [ $? -ne 0 ]; then
						echo "COLLATINGSERVICE is required to be up. So starting the job."
						nomad run ${SER_INSTALL_PATH}/nomadFile/*_COLLATINGSERVICE*.nomad
						sleep 120
					fi
					
					if [ $fflag -eq 0 ]; then
						nomad status | grep "tomcat" | grep 'running' 1>/dev/null 2>/dev/null
						if [ $? -ne 0 ]; then
							echo "Starting tomcat service"
							nomad run tomcat.nomad
							sleep 90
							check_service_up tomcat 1
							return_val=$?
							if [ $return_val -eq 10 ]; then
								recheck_services_up tomcat 1 $return_val
							fi
						else
							echo "Tomcat is already running"
						fi
					fi
					;;
				KEYCLOAK)
					nomad status | grep "keycloak" | grep 'running' 1>/dev/null 2>/dev/null
					if [ $? -ne 0 ]; then
						echo "Starting Keycloak"
						logit "Starting Keycloak"
						cd "${SER_INSTALL_PATH}/nomadFile"
						nomad run keycloak.nomad
						sleep 60
						check_service_up keycloak 1
						return_val=$?
						if [ $return_val -eq 10 ]; then
							recheck_services_up keycloak 1 $return_val
						fi
					else
						echo "Keycloak is already running"
					fi
					;;
				
				*)
					if [ $# -eq 1 ];then
						echo "Services name provided in incorrect. example 'HAPROXY', 'RABBITMQ' ,'COUCHBASE', 'CASSANDRA','FLINK' , 'CAPACITY_PLANNING' , 'TOMCAT' , 'KEYCLOCK' "
						logit "Services name provided in incorrect. example 'HAPROXY', 'RABBITMQ' ,'COUCHBASE', 'CASSANDRA','FLINK' , 'CAPACITY_PLANNING' , 'TOMCAT' , 'KEYCLOCK' "
					else
						echo "Skipping $item as this is not supported. Please start it manually or contact administrator"
					fi
	
			esac
		done
	else
		if [ $# -eq 1 ];then
			echo "Services name provided in incorrect. example 'HAPROXY', 'RABBITMQ' ,'COUCHBASE', 'CASSANDRA','FLINK' , 'CAPACITYPLANNING' , 'TOMCAT' , 'KEYCLOCK' "
			logit "Services name provided in incorrect. example 'HAPROXY', 'RABBITMQ' ,'COUCHBASE', 'CASSANDRA','FLINK' , 'CAPACITYPLANNING' , 'TOMCAT' , 'KEYCLOCK' "
		fi
	fi
	
}

start_A1_Services()
{	
	serv="$1"
	hname=$(echo "$serv" | cut -d "_" -f1)
	nomad status | grep "${serv}" | grep "running" > /dev/null 2>&1
	if [ $? -ne 0 ]; then
		echo "Starting appsone service $1 on $hname"
		logit "Starting appsone service $1 on $hname"
		nomad run ${serv}.nomad
		sleep 15
		check_service_up "${serv}" '1'
		return_val=$?
		if [ $return_val -eq 10 ]; then
			recheck_services_up "${ser_}" "1" "$return_val"
		fi
	else
		echo "Service $1 is already running on $hname"
	fi
}

main()
{	
	BASEDIR=`pwd`
	CONFIGFILE="${BASEDIR}/conf/.Inter_Conf/wrapper.conf"
	. "$CONFIGFILE" 
	if [ $? -ne 0 ]; then
 	       	echo "Could not load config file"
		logit "Could not load config file"
 	       	exit 1
	fi
	nomad_port=$(grep ^HTTP_PORT  $BASEDIR/conf/extendedConf.py | grep -v "RANGE\|^#" | cut -d = -f2 | sed -e "s/'//g;s/ //g" )
        export NOMAD_CACERT="/etc/nomad/nomad-ca.pem"
        export NOMAD_CLIENT_KEY="/etc/nomad/cli-key.pem"
        export NOMAD_CLIENT_CERT="/etc/nomad/cli.pem"
        export NOMAD_ADDR=https://localhost:$nomad_port

	mkdir -p "${BASEDIR}/logs"
	LOG_FILE="${BASEDIR}/logs/$0_minimal.log"
	## Getting all INFRA services and putting them in an array.
	eval `grep "^INFRA_SERVICES" ${BASEDIR}/conf/JobsConfigs.py 2>/dev/null | tr -d ' '| tr '[' '(' | tr ']' ')'|tr ',' ' '` 
	wrong_iserv=0
	wrong_aserv=0
	cd "${SER_INSTALL_PATH}/nomadFile/"
	INFRA_SERVICES=($(ls $SER_INSTALL_PATH/nomadFile | grep -v "_\|hadoop\|zookeeper\|sample\|saboo\|capacity\|percona" | cut -d '.' -f1 | tr '[:lower:]' '[:upper:]'))
	INFRA_SERVICES_MANUAL=("CAPACITYPLANNING")
	A1_SERV=($(ls $SER_INSTALL_PATH/nomadFile | grep "_" | grep -v "DATACOLLECTOR_WAS\|JPPF\|PATTERNRT\|NOTIFICATIONSERVICE\|COMPONENTAGENT\|FORENSIC\|JMXCOLLECTOR\|INCIDENT" | sed -e "s/\.nomad//" | tr '\n' ' '))
	A1_SERV_MANUAL=($(ls $SER_INSTALL_PATH/nomadFile | grep "_" | grep "DATACOLLECTOR_WAS\|JPPF\|PATTERNRT\|NOTIFICATIONSERVICE\|COMPONENTAGENT\|FORENSIC\|JMXCOLLECTOR\|INCIDENT" | sed -e "s/\.nomad//" | tr '\n' ' '))
	while getopts "iavhl" opt; do
	case $opt in
        i)
		if [ "$2" = "ALL" ]; then
                	echo "Starting all the infra services one by one."
			logit "Starting all the infra services one by one."
			start_service_infra
		elif [ ! -z "$2" ]; then
			echo ${INFRA_SERVICES[@]} | grep -qw "$2"
			if [ $? -eq 0 ]; then
				start_service_infra "$2"
			else
				echo "Wrong service name. Supported service names are "
				logit "Wrong service name. Supported service names are "
				for item in "${INFRA_SERVICES[@]}"
				do
					echo "	$item"
				done
			fi
		else
			echo "No service name provided. Supported service names are"
			logit "No service name provided. Supported service names are"
			for item in "${INFRA_SERVICES[@]}"
			do
				echo "	$item"
			done
		fi
            	;;
        a)
		if [ "$2" = "ALL" ] ; then
                	echo "Starting all the A1 services one by one."
			logit "Starting all the A1 services one by one."
			for serv in "${A1_SERV[@]}"; do
				start_A1_Services "$serv"
				if [ $? -ne 0 ]; then
					logit "$serv status could not be verifed.. "
					echo "$serv : status could not be verifed. please check manually about the status.."
				fi
			done    
		elif [ ! -z "$2" ]; then
			flag=1
			for item in "${A1_SERV[@]}"
			do
				echo "$item" | grep -q "^${2}$"
				if [ $? -eq 0 ]; then
					flag=0
					break
				fi
			done 
			if [ $flag -eq 0 ]; then
				start_A1_Services "$2"
			else
				echo "Wrong service name. Supported service names are "
				logit "Wrong service name. Supported service names are "
				for item in "${A1_SERV[@]}"
				do
					echo "	$item"
				done
			fi
		else
			echo "No service name provided. Supported service names are"
			logit "No service name provided. Supported service names are"
			for item in "${A1_SERV[@]}"
			do
				echo "	$item"
			done
		fi
            	;;
        
        h)
		usage
		;;
	l)
		echo "Appsone services those can be started without any configuration changes"
		for item in "${A1_SERV[@]}"
		do
			echo "	$item"
		done
		echo "	Note : These service will be started via $0"
		echo "Appsone services those require manual configuration changes before starting"
		for item in "${A1_SERV_MANUAL[@]}"
		do
			echo "	$item"
		done
		echo "	Note : These services has to be started manually from A1HEALTH service management after configuration changes"
		echo "Infra services those can be started without any configuration changes"
		for item in "${INFRA_SERVICES[@]}"
		do
			echo "	$item"
		done
		#echo "	Note : These service will be started via $0"
		#echo "Infra services those require manual configuration changes before starting"
		#for item in "${INFRA_SERVICES_MANUAL[@]}"
		#do
		#	echo "	$item"
		#done
		#echo "	Note : These services has to be started manually after configuration changes."
		#echo "	using nomad run <Install Dir>/nomadFiles/capacity-planning.nomad"
		;;
        \?)
		echo "Invalid option "
		usage
		;;
	esac
	done
	
}

if [ $# -eq 1 ] || [ $# -eq 2 ] || [ $# -eq 3 ]; then
	main "$@"
else
	echo "The script was not invoked correctly. Please go through README or Please run the script with -h to for usage help."
	exit 1
fi
